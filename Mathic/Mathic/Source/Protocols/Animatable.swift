//
//  Animatable.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/3/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

public protocol TitledAnimation {
    static var animationTitle: String { get }
}

public protocol Animatable: AnyObject {
    var frameRate: Double { get }
    
    func start()
    func stop()
}

public protocol Informable: AnyObject {
    var didRenderFrame: (() -> Void)? { get set }
}

public typealias AnimatableView = UIView & Animatable & TitledAnimation
