//
//  ParamDisplayable.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public struct ParameterSet {
    public let name: String
    public let value: String
}

public protocol ParameterDisplayable {
    var params: [ParameterSet] { get }
}

extension ParameterDisplayable {
    public var count: Int {
        return params.count
    }
}
