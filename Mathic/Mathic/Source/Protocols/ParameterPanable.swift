//
//  TwoAxisParameters.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/1/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

public protocol ParameterPinchable {
    var pinchName: String? { get }
    
    var pinchMinimum: CGFloat { get }
    var displayablePinchMinimum: String { get }

    var pinchValue: CGFloat { get set }
    var displayablePinchValue: String { get }

    var pinchMaximum: CGFloat { get }
    var displayablePinchMaximum: String { get }
}

extension ParameterPinchable {
    public var displayablePinchMinimum: String { "\(pinchMinimum.twoSigFigs)" }
    public var displayablePinchValue: String { "\(pinchValue.twoSigFigs)" }
    public var displayablePinchMaximum: String { "\(pinchMaximum.twoSigFigs)" }
}

public protocol ParameterRotatable {
    var rotateName: String? { get }
    
    var rotateMinimum: CGFloat { get }
    var displayableRotateMinimum: String { get }
    
    var rotateValue: CGFloat { get set }
    var displayableRotateValue: String { get }
    
    var rotateMaximum: CGFloat { get }
    var displayableRotateMaximum: String { get }
}

extension ParameterRotatable {
    public var displayableRotateMinimum: String { "\(rotateMinimum.twoSigFigs)" }
    public var displayableRotateValue: String { "\(rotateValue.twoSigFigs)" }
    public var displayableRotateMaximum: String { "\(rotateMaximum.twoSigFigs)" }
}

public protocol ParameterPanable {
    var nameX: String? { get }
    var nameY: String? { get }
    var nameZ: String? { get }
    
    var minimumX: CGFloat { get }
    var maximumX: CGFloat { get }
    
    var minimumY: CGFloat { get }
    var maximumY: CGFloat { get }
    
    var minimumZ: CGFloat { get }
    var maximumZ: CGFloat { get }

    var valueX: CGFloat { get set }
    var valueY: CGFloat { get set }
    var valueZ: CGFloat { get set }
    
    // Values to display to the user. Default implementations below
    var displayableMinimumX: String { get }
    var displayableMaximumX: String { get }

    var displayableMinimumY: String { get }
    var displayableMaximumY: String { get }

    var displayableMinimumZ: String { get }
    var displayableMaximumZ: String { get }
    
    var displayableValueX: String { get }
    var displayableValueY: String { get }
    var displayableValueZ: String { get }
}

extension ParameterPanable {
    public var displayableMinimumX: String { "\(minimumX.twoSigFigs)" }
    public var displayableMaximumX: String { "\(maximumX.twoSigFigs)" }

    public var displayableMinimumY: String { "\(minimumY.twoSigFigs)" }
    public var displayableMaximumY: String { "\(maximumY.twoSigFigs)" }

    public var displayableMinimumZ: String { "\(minimumZ.twoSigFigs)" }
    public var displayableMaximumZ: String { "\(maximumZ.twoSigFigs)" }
    
    public var displayableValueX: String { "\(valueX.twoSigFigs)" }
    public var displayableValueY: String { "\(valueY.twoSigFigs)" }
    public var displayableValueZ: String { "\(valueZ.twoSigFigs)" }
}

public extension ParameterPanable {
    var xSpan: CGFloat { return maximumX - minimumX }
    var ySpan: CGFloat { return maximumY - minimumY }
    var zSpan: CGFloat { return maximumZ - minimumZ }
}

public extension ParameterPanable {
    var parameterCount: Int {
        var count = 0
        if nameX != nil { count += 1 }
        if nameY != nil { count += 1 }
        if nameZ != nil { count += 1 }
        return count
    }
}

public protocol ParameterInstructable {
    var instructions: String? { get }
}

extension CGFloat {
    private func string(sigFigs: Int) -> String {
        let format = "%.0\(sigFigs)f"
        return String(format: format, self)
    }

    public var oneSigFig: String {
        return string(sigFigs: 1)
    }

    public var twoSigFigs: String {
        return string(sigFigs: 2)
    }
}
