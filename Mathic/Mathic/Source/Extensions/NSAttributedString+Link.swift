//
//  NSAttributedString+Link.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

extension NSAttributedString {
    static func link(font: UIFont = UIFont.preferredFont(forTextStyle: .body),
                     text: String,
                     textColor: UIColor,
                     url: URL,
                     urlText: String,
                     urlColor: UIColor,
                     urlMultiplier: CGFloat = 1.0,
                     alignment: NSTextAlignment = .center) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        
        let attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: textColor,
            .font: font,
            .paragraphStyle: paragraphStyle
        ]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        
        // Insert URL color and underline
        let pointSize = font.pointSize * urlMultiplier
        let urlFont = UIFont(name: font.fontName, size: pointSize) ?? font
        let range = (text as NSString).range(of: urlText)
        if range.location != NSNotFound {
            let attrites: [NSAttributedString.Key: Any] = [
                .font: urlFont,
                .underlineStyle: true,
                .underlineColor: urlColor,
                .foregroundColor: urlColor
            ]
            attributedString.addAttributes(attrites, range: range)
        }
        
        // Return as immutable
        return NSAttributedString(attributedString: attributedString)
    }
}
