//
//  CGPoint+Helpers.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/2/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

extension CGPoint {
    static func interpolate(percent: CGFloat, point1: CGPoint, point2: CGPoint) -> CGPoint {
        let x: CGFloat = CGFloat.interpolate(percent: percent, minimum: point1.x, maximum: point2.x)
        let y: CGFloat = CGFloat.interpolate(percent: percent, minimum: point1.y, maximum: point2.y)
        return CGPoint(x: x, y: y)
    }
}

extension CGPoint {
    func offset(dx: CGFloat = 0, dy: CGFloat = 0) -> CGPoint {
        return CGPoint(x: x + dx, y: y + dy)
    }
    
    func add(point: CGPoint) -> CGPoint {
        CGPoint(x: x + point.x, y: y + point.y)
    }
    
    static func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
}
