//
//  UIColor+Helpers.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

internal extension UIColor {
    static func pastelColor(hue: CGFloat) -> UIColor {
        return UIColor(hue: hue, saturation: 0.5, brightness: 1.0, alpha: 1.0)
    }

    static func darkPastelColor(hue: CGFloat) -> UIColor {
        return UIColor(hue: hue, saturation: 1.0, brightness: 0.5, alpha: 1.0)
    }

    static func hueColor(hue: CGFloat) -> UIColor {
        return UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
    }

    static func interpolate(color1: UIColor, color2: UIColor) -> UIColor {
        var r1: CGFloat = 0
        var g1: CGFloat = 0
        var b1: CGFloat = 0
        var a1: CGFloat = 0
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        
        var r2: CGFloat = 0
        var g2: CGFloat = 0
        var b2: CGFloat = 0
        var a2: CGFloat = 0
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        let red = (r1 + r2) / 2.0
        let green = (g1 + g2) / 2.0
        let blue = (b1 + b2) / 2.0
        let alpha = (a1 + a2) / 2.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func saturate(saturation newSaturation: CGFloat) -> UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return UIColor(hue: hue, saturation: newSaturation, brightness: brightness, alpha: alpha)
    }
    
    func brightness(brightness newBrightness: CGFloat) -> UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return UIColor(hue: hue, saturation: saturation, brightness: newBrightness, alpha: alpha)
    }
}
