//
//  CubicBezier.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/22/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import Foundation

//
//    // https://stackoverflow.com/questions/4089443/find-the-tangent-of-a-point-on-a-cubic-bezier-curve
//    struct CubicBezier {
//        typealias Coeficients = (c: CGFloat, s: CGFloat, m: CGFloat, l: CGFloat)
//
//        let xCoeficients: Coeficients
//        let yCoeficients: Coeficients
//
//        init(point0: CGPoint,
//             point1: CGPoint,
//             point2: CGPoint,
//             point3: CGPoint) {
//            xCoeficients = CubicBezier.coeficients(point0: point0.x,
//                                                          point1: point1.x,
//                                                          point2: point2.x,
//                                                          point3: point3.x)
//
//            yCoeficients = CubicBezier.coeficients(point0: point0.y,
//                                                          point1: point1.y,
//                                                          point2: point2.y,
//                                                          point3: point3.y)
//        }
//
//        func x(at t: CGFloat) -> CGFloat {
//            return CubicBezier.curve(with: xCoeficients, at: t)
//        }
//
//        func y(at t: CGFloat) -> CGFloat {
//            return CubicBezier.curve(with: yCoeficients, at: t)
//        }
//
//        func dx(at t: CGFloat) -> CGFloat {
//            return CubicBezier.tangentToCurve(with: xCoeficients, at: t)
//        }
//
//        func dy(at t: CGFloat) -> CGFloat {
//            return CubicBezier.tangentToCurve(with: yCoeficients, at: t)
//        }
//
//        func point(at t: CGFloat) -> CGPoint {
//            return CGPoint(x: x(at: t), y: y(at: t))
//        }
//
//        func tangent(at t: CGFloat) -> CGVector {
//            return CGVector(dx: dx(at: t), dy: dy(at: t))
//        }
//    }
//
//
//    extension CubicBezier {
//        static func coeficients(point0: CGFloat,
//                                point1: CGFloat,
//                                point2: CGFloat,
//                                point3: CGFloat) -> Coeficients {
//            let _3c0 = point0 + point0 + point0
//            let _3c1 = point1 + point1 + point1
//            let _3c2 = point2 + point2 + point2
//            let _6c1 = _3c1 + _3c1
//
//            let c = point3 - _3c2 + _3c1 - point0
//            let s = _3c2 - _6c1 + _3c0
//            let m = _3c1 - _3c0
//            let l = point0
//
//            return (c, s, m, l)
//        }
//
//        static func curve(with coeficients: Coeficients, at t: CGFloat) -> CGFloat {
//            let (c, s, m, l) = coeficients
//            return ((c * t + s) * t + m) * t + l
//        }
//
//        static func tangentToCurve(with coeficients: Coeficients, at t: CGFloat) -> CGFloat {
//            let (c, s, m, _) = coeficients
//            return ((c + c + c) * t + s + s) * t + m
//        }
//    }
//
//
//            func drawCubicBezier() {
//                UIColor.label.setStroke()
//                let path = UIBezierPath()
//                path.lineWidth = 4
//
//                let bezier = CubicBezier(point0: CGPoint(x: 0, y: 0),
//                                         point1: CGPoint(x: 0, y: 1),
//                                         point2: CGPoint(x: 1, y: 0),
//                                         point3: CGPoint(x: 1, y: 1))
//                for i in stride(from: 0, to: Int(bounds.width), by: 20) {
//                    let t = CGFloat(i) / bounds.width
//
//                    let bezierPoint = bezier.point(at: t)
//
//                    let p = CGPoint(x: t, y: bezierPoint.y)
//
//                    let point = expand(point: p)
//                    if i == 0 {
//                        path.move(to: point)
//                    } else {
//                        path.addLine(to: point)
//                    }
//                }
//                UIColor.label.setStroke()
//                path.stroke()
//            }
//      //        drawCubicBezier()
