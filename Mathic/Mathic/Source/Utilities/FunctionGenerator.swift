//
//  FunctionGenerator.swift
//
//  Created by Zakk Hoyt on 8/28/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//
//  A few swift function to generate sine, square, triangle, and sawtooth waves —
//

import Foundation
import Darwin

let kRMS = CGFloat(0.707)

@inline(__always) func sine(_ phase: CGFloat) -> CGFloat {
    return sin(phase)
}

@inline(__always) func square(_ phase: CGFloat) -> CGFloat {
    var amplitude = CGFloat(0.0)
    if sin(phase) > 0 {
        amplitude = kRMS
    } else {
        amplitude = -kRMS
    }
    return amplitude
}

@inline(__always) func triangle(_ phase: CGFloat) -> CGFloat {
    return asin(sin(phase)) / (CGFloat.pi / 2.0)
}

@inline(__always) func reverseSawtooth(_ phase: CGFloat) -> CGFloat {
    var amplitude = CGFloat(0.0)
    amplitude = (2 * CGFloat.pi - phase) / CGFloat(2*CGFloat.pi) // 0.0 ... 1.0
    amplitude = amplitude * 2.0 - 1.0 // -1.0 ... 1.0
    return amplitude
}

@inline(__always) func sawtooth( timeInterval: CGFloat, frequency: CGFloat) -> CGFloat {
    (2.0 / CGFloat.pi) * (frequency * CGFloat.pi * fmod(CGFloat(timeInterval), 1.0 / frequency) - (CGFloat.pi / 2.0))
    // https://math.stackexchange.com/questions/1638340/sawtooth-wave-as-a-sum-of-sines
    
}

@inline(__always) func invert(_ amplitude: CGFloat) -> CGFloat {
    return amplitude * -1.0
}

@inline(__always) func w(_ frequency: CGFloat) -> CGFloat {
    return frequency * 2.0 * CGFloat.pi
}
