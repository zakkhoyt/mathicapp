//
//  Bezier.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/22/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//
//  https://en.wikipedia.org/wiki/B%C3%A9zier_curve
//  https://medium.com/@Acegikmo/the-ever-so-lovely-b%C3%A9zier-curve-eb27514da3bf
//  UICubicTimingParameters
// @available(iOS 10.0, *)
// open class UICubicTimingParameters : NSObject, UITimingCurveProvider {

import Foundation

class Bezier {
    struct Solution {
        let point: CGPoint
        let tangentPoint1: CGPoint
        let tangentPoint2: CGPoint
    }
    
    enum Error: Swift.Error, LocalizedError {
        case inputOutOfRange(t: CGFloat)
        case pointOutOfRange(point: CGPoint)
        
        var errorDescription: String? {
            switch self {
            case .inputOutOfRange(let t):
                return "Input out of range: \(t)"
            case .pointOutOfRange(let point):
                return "Point out of range: \(point.debugDescription)"
            }
        }
    }
    
    /// Solves for a point on a bezier curve.
    /// - Parameters:
    ///   - points: The contorls points in normalized range 0...1.
    ///   - t: Time from 0...1
    /// - Throws: Errors if any points or 1 is out of bounds.
    /// - Returns: tuple. .0 is a point along the bezier curve where x/y in 0...1), .1 and .2 are the start/end points of the tangent line (derivative)
    /// - Returns: An instance of `Bezier.Solution`
    static func solve(points: [CGPoint], t: CGFloat) throws -> Solution {
        let range: ClosedRange<CGFloat> = CGFloat(0.0)...CGFloat(1.0)
        
        guard range.contains(t) else {
            throw Bezier.Error.inputOutOfRange(t: t)
        }
        
        for point in points {
            if !range.contains(point.x) || !range.contains(point.y) {
                throw Bezier.Error.pointOutOfRange(point: point)
            }
        }
        
        func reduce(points: [CGPoint], t: CGFloat) -> (CGFloat, CGPoint, CGPoint) {
            var nextPoints: [CGPoint] = []
            for i in 0..<points.count {
                if i == 0 {
                    continue
                }
                let point0 = points[i - 1]
                let point1 = points[i]
                let point = CGPoint.interpolate(percent: t, point1: point0, point2: point1)
                
                if points.count == 2 {
                    return (point.y, point0, point1)
                } else {
                    nextPoints.append(point)
                }
            }
            
            return reduce(points: nextPoints, t: t)
        }

        let tuple = reduce(points: points, t: t)
        let point0 = CGPoint(
            x: CGPoint.interpolate(percent: t, point1: tuple.1, point2: tuple.2).x,
            y: tuple.0
        )
        let solution = Solution(
            point: point0,
            tangentPoint1: tuple.1,
            tangentPoint2: tuple.2
        )
        return solution
//        return (point0, tuple.1, tuple.2)
    }
    
    /// Same as solve, but adds origin and 1,1 as control points
    static func solveNormalized(points: [CGPoint], t: CGFloat) throws -> Solution {
        let firstPoint = CGPoint(x: 0, y: 0)
        let lastPoint = CGPoint(x: 1, y: 1)
        var allPoints = [firstPoint]
        allPoints.append(contentsOf: points)
        allPoints.append(lastPoint)
        return try solve(points: allPoints, t: t)
    }
}
