//
//  BezierView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/2/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class BezierCurveView: MathView {
    // MARK: Private vars
    
    // Keeps track of the current cycle (which is CGFloat)
    // by casting to Int.
    private var currentCycleCount: Int = -1
    private var numberOfSets = 0
    private var numberOfLines = 0
    private var numberOfDots = 0
    
    // Catch long presses and remove all points
    private lazy var longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
    
    public override var frequency: TimeInterval { 0.33 }
    
    // MARK: Public vars
    
    // Start off at 5 seconds into the animation.
    open override var previewElapsed: TimeInterval { 5 }
    
    // Initial points.
    open var bezierPoints: [CGPoint] = [
    ]
    
    // Keeps track of our calculated bezier path
    open var bezierPathPoints: [CGPoint] = []

    // MARK: Publics functions
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        addGestureRecognizer(longPressGesture)
        
        guard let superview = self.superview else { return }
        let x1: CGFloat = 0.1 * superview.bounds.width
        let x2: CGFloat = 0.5 * superview.bounds.width
        let x3: CGFloat = 0.9 * superview.bounds.width
        let y1: CGFloat = 0.33 * superview.bounds.height
        let y2: CGFloat = 0.66 * superview.bounds.height
        bezierPoints = [
            CGPoint(x: x1, y: y2),
            CGPoint(x: x2, y: y1),
            CGPoint(x: x3, y: y2)
        ]
    }
    
    // Single tap is handled here. Long press is gesture recognizer
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let point = touches.first?.location(in: self) else { return }
        bezierPoints.append(point)
        bezierPathPoints.removeAll()
    }
    
    open override func draw(_ rect: CGRect) {
        let lineWidth: CGFloat = 1
        
        let alphaPer = CGFloat(1) / CGFloat(bezierPoints.count)
                
        // Recursive function. Base case is points.count == 0
        func connectPoints(_ points: [CGPoint],
                           radius: CGFloat,
                           alpha: CGFloat) {
            // Base case
            if points.count == 0 { return }
            
            numberOfSets += 1
            
            let fillColor = color(index: bezierPoints.count).withAlphaComponent(alpha)
            let strokeColor = fillColor
            
            // Loop throught points:
            //  1) Drawing a dot at each
            //  2) Connecting each point to its predecessor (except 0
            //  3) Using `self.cycle`, interpolate new set of points such that newPoints.count = points.count - 1
            var nextPoints: [CGPoint] = []
            for i in 0..<points.count {
                func drawDot() {
                    defer { numberOfDots += 1 }
                    let point = points[i]
                    drawCircle(center: point,
                               radius: radius,
                               strokeColor: strokeColor,
                               fillColor: fillColor,
                               lineWidth: 1)
 
                    // Draw point text for first set
                    guard points.count == bezierPoints.count else { return }
                    let font = UIFont.preferredFont(forTextStyle: .caption2)
                    drawString("P\(numberOfDots)",
                        textColor: .label,
                        at: CGPoint(x: point.x, y: point.y + font.pointSize + radius),
                        attributes: [.font: font])
                }
                drawDot()
                
                func connectDotWithPreviousDot() {
                    guard i > 0 else { return }
                    let point1 = points[i - 1]
                    let point2 = points[i]
                    drawLine(point1: point1,
                             point2: point2,
                             strokeColor: strokeColor,
                             fillColor: fillColor,
                             lineWidth: lineWidth)
                    numberOfLines += 1
                }
                connectDotWithPreviousDot()
                
                func prepareNextDot() {
                    guard i > 0 else { return }
                    let point1 = points[i - 1]
                    let point2 = points[i]
                    let point = CGPoint.interpolate(percent: cycle,
                                                    point1: point1,
                                                    point2: point2)
                    nextPoints.append(point)
                    if points.count == 2 {
                        bezierPathPoints.append(point)
                    }
                }
                prepareNextDot()
            }
            
            // Recursive call
            connectPoints(nextPoints, radius: radius + 1, alpha: alpha + alphaPer)
        }
        
        // Initial call (will use recursion on itself
        numberOfLines = 0
        numberOfDots = 0
        numberOfSets = 0
        connectPoints(bezierPoints, radius: 4, alpha: alphaPer)

        /// Each frame adds one point to bezierPathPoints.
        /// Draw a line useing them
        func drawBezierPath() {
            // Ensure we haven't started a new cycle. If we have, erase
            // the point which make up the bezier path.
            guard Int(currentCycleCount) == Int(floor(cycleCount)) else {
                self.currentCycleCount = Int(floor(cycleCount))
                bezierPathPoints.removeAll()
                return
            }
            
            // Draw bezier path point by point
            let path = UIBezierPath()
            path.lineCapStyle = .round
            for (i, point) in bezierPathPoints.enumerated() {
                if i == 0 {
                    path.move(to: point)
                } else {
                    path.addLine(to: point)
                }
            }
            UIColor.label.setStroke()
            path.lineWidth = 8
            path.stroke()
        }
        drawBezierPath()
    }
    
    // MARK: Private functions
    
    // swiftlint:disable cyclomatic_complexity
    private func color(index: Int) -> UIColor {
        switch index % 10 {
        case 0: return .systemRed
        case 1: return .label
        case 2: return .systemGreen
        case 3: return .brown
        case 4: return .systemYellow
        case 5: return .systemOrange
        case 6: return .systemPurple
        case 7: return .systemPink
        case 8: return .magenta
        case 9: return .cyan
        default: return .systemBlue
        }
    }
    // swiftlint:enable cyclomatic_complexity
    
    @objc
    private func longPress(gesture: UILongPressGestureRecognizer) {
        bezierPoints.removeAll()
        currentCycleCount = -1
    }
}

extension BezierCurveView: ParameterInstructable {
    public var instructions: String? {
        """
        Tap: Add a point.
        Long Press: Remove all points.
        """
    }
}

extension BezierCurveView: ParameterDisplayable {
    public var params: [ParameterSet] {
        return [
            ParameterSet(name: "T", value: String(format: "%.2f", cycle)),
            ParameterSet(name: "Sets", value: "\(Int(numberOfSets))"),
            ParameterSet(name: "Lines", value: "\(Int(numberOfLines))"),
            ParameterSet(name: "Dots", value: "\(Int(numberOfDots))")
        ]
    }
}

extension BezierCurveView: TitledAnimation {
    public static var animationTitle: String { "Bezier Curve" }
}
