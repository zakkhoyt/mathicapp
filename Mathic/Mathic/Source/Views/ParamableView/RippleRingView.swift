//
//  RippleRingView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class RippleRingView: PhasedCircleView {
    public var numberOfRings: CGFloat = 20
    
    open override var unitCenter: CGPoint {
        return bounds.center
    }
    
    open override var frequency: TimeInterval {
        return 0.2
    }
    
    open override func draw(_ rect: CGRect) {
        func drawRings() {
            UIColor.white.setStroke()
            let lineWidth = radius / CGFloat(numberOfRings * 4)
            let ratio: CGFloat = 2.0
            for i in 0..<Int(numberOfRings) {
                let ringWidth = bounds.width / 2.0 * (CGFloat(i + 1) / numberOfRings)
                let ringHeight = ringWidth / ratio
                let percent = (1.0 * CGFloat.pi) * CGFloat(i) / numberOfRings
                let hue = (sin(phase + percent) + 1.0) / 2.0
                let color = UIColor.hueColor(hue: hue)
                color.setStroke()
                
                let yScale = 0.5 * 1.0 - (CGFloat(i) / numberOfRings)
                let yOffset = (bounds.height / 4.0 + (yScale * bounds.height / 4.0)) * sin(phase + percent)
                let rect = CGRect(x: bounds.center.x - ringWidth / 2.0,
                                  y: bounds.center.y + yOffset - ringHeight / 2.0,
                                  width: ringWidth,
                                  height: ringHeight)
                let path = UIBezierPath(ovalIn: rect)
                path.lineWidth = lineWidth
                
                switch traitCollection.userInterfaceStyle {
                case .dark: path.stroke(with: .plusLighter, alpha: 1.0)
                default: path.stroke(with: .plusDarker, alpha: 1.0)
                }
            }
        }
        drawRings()
    }
}

extension RippleRingView: ParameterPanable {
    public var nameX: String? { return "# of Rings" }
    public var nameY: String? { return nil }
    public var nameZ: String? { return nil }

    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 50 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 0 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfRings }
        set { numberOfRings = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension RippleRingView: TitledAnimation {
    public static var animationTitle: String { "Ripple Rings" }
}
