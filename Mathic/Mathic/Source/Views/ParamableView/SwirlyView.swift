//
//  SwirlyView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/9/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

typealias SwirlyMask = (UIView, CircleView)

@IBDesignable
open class SwirlyView: MathView {
    let numberOfTuples = 3
//    let numberOfTuples = 2
    
    private var tuples: [SwirlyMask] = []
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()

        tuples.forEach { spiralView, circleView in
            // Scale

            // Animate Rotation
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = 2 * CGFloat.pi
            rotationAnimation.duration = 2.5
            rotationAnimation.repeatCount = Float.greatestFiniteMagnitude
            spiralView.layer.add(rotationAnimation, forKey: nil)
        }
    }
    
    private func commonInit() {
        clipsToBounds = true
        for i in 0..<numberOfTuples {
            let spiralView = SpiralView()
            spiralView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(spiralView)
            
            let constant: CGFloat = 0
            spiralView.color = .systemBackground
            spiralView.reversed = i % 2 == 0 ? false : true
            spiralView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: constant).isActive = true
            spiralView.topAnchor.constraint(equalTo: topAnchor, constant: constant).isActive = true
            spiralView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -constant).isActive = true
            spiralView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -constant).isActive = true

            let circleView = CircleView()
            circleView.percent = 1.0 - (CGFloat(i) / CGFloat(numberOfTuples))
            
            let tuple = (spiralView, circleView)
            tuples.append(tuple)
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        tuples.forEach {
            $0.1.frame = bounds
            $0.0.mask = $0.1
        }
    }
}

class SpiralView: UIView {
    let numberOfSpirals: CGFloat = 10
    let numberOfLines: Int = 2
    var color: UIColor = .label
    var reversed: Bool = false

    override func draw(_ rect: CGRect) {
        let travelPerSpiral: CGFloat = radius / numberOfSpirals
        let circumference: CGFloat = bounds.width / 8

        UIColor.label.setStroke()
        
        let paths: [UIBezierPath] = {
            var paths: [UIBezierPath] = []
            for _ in 0..<numberOfLines {
                let path = UIBezierPath()
                let lineWidth: CGFloat = travelPerSpiral / 2
                path.lineWidth = lineWidth
                path.lineCapStyle = .round
                path.move(to: center)
                paths.append(path)
            }
            return paths
        }()
        
        let radiusPerI: CGFloat = travelPerSpiral / CGFloat(circumference)
        var magnitude: CGFloat = 0
        for _ in 0..<Int(numberOfSpirals) {
            for i in 0..<Int(circumference) {
                for l in 0..<numberOfLines {
                    let path = paths [l]
                    let subAngle: CGFloat = 2 * CGFloat.pi * (CGFloat(l) / CGFloat(numberOfLines))
                    let angle: CGFloat = (reversed ? -1 : 1) * (2 * CGFloat.pi * CGFloat(i) / CGFloat(circumference))
                    let x = magnitude * cos(angle + subAngle)
                    let y = magnitude * sin(angle + subAngle)
                    let point = CGPoint(x: bounds.center.x + x,
                                        y: bounds.center.y + y)
                    path.addLine(to: point)
                    magnitude += radiusPerI
                }
            }
        }
        paths.forEach { $0.stroke() }
    }
}

class CircleView: UIView {
    var percent: CGFloat = 0.50
    
    convenience init() {
        self.init(frame: .zero)
        self.backgroundColor = .clear
    }

    override func draw(_ rect: CGRect) {
        UIColor.green.setFill()
        let path = UIBezierPath(arcCenter: bounds.center,
                                radius: radius * percent,
                                startAngle: 0,
                                endAngle: 2 * CGFloat.pi,
                                clockwise: true)
        path.fill()
    }
}

extension SwirlyView: TitledAnimation {
    public static var animationTitle: String { "Swirly" }
}
