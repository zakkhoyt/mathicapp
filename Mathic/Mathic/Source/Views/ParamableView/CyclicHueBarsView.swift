//
//  CyclicHueBarsView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/31/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class CyclicHueBarsView: PhasedCircleView {
    open var numberOfColumns: CGFloat = 20
    open var numberOfBars: CGFloat = 20
    
    open override var frequency: TimeInterval {
        return 1.0
    }
    
    open override func draw(_ rect: CGRect) {
        let anglePerColumn: CGFloat = (2.0 * CGFloat.pi) / CGFloat(numberOfColumns)
        let thicknessPerBar = (radius / CGFloat(numberOfBars)) / 2.0
        let spacingAngle = anglePerColumn * 0.5
        
        for c in 0..<Int(numberOfColumns) {
            let startAngle = CGFloat(c) * anglePerColumn
            let endAngle = startAngle + anglePerColumn - spacingAngle
            let hue = startAngle / (CGFloat.pi * 2.0)
            let color = UIColor.hueColor(hue: hue)

            let subPhase: CGFloat = 2.0 * 2.0 * CGFloat.pi * CGFloat(c) / CGFloat(numberOfColumns)
            let percent: CGFloat = 0.5 * sin(phase + subPhase) // -0.5 ... 0.5

            // Half number of bars +/- half number of bars
            let barCount = Int(numberOfBars / 2) + Int(numberOfBars * percent)
            for b in 0..<barCount {
                let point1: CGPoint = {
                    let x = 2.0 * CGFloat(b) * thicknessPerBar * cos(startAngle)
                    let y = 2.0 * CGFloat(b) * thicknessPerBar * sin(startAngle)
                    return CGPoint(x: bounds.center.x + x,
                                   y: bounds.center.y + y)
                }()
                
                let point2: CGPoint = {
                    let x = 2.0 * CGFloat(b) * thicknessPerBar * cos(endAngle)
                    let y = 2.0 * CGFloat(b) * thicknessPerBar * sin(endAngle)
                    return CGPoint(x: bounds.center.x + x,
                                   y: bounds.center.y + y)
                }()
                
                drawLine(point1: point1,
                         point2: point2,
                         strokeColor: color,
                         fillColor: .clear,
                         lineWidth: thicknessPerBar)
            }
        }
    }
}

extension CyclicHueBarsView: ParameterPanable {
    public var nameX: String? { return "# of Columns" }
    public var nameY: String? { return "# of Bars" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 50 }
    public var minimumY: CGFloat { return 1 }
    public var maximumY: CGFloat { return 50 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfColumns }
        set { numberOfColumns = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return numberOfBars }
        set { numberOfBars = newValue.rounded() }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension CyclicHueBarsView: TitledAnimation {
    public static var animationTitle: String { "Cyclic Hue Bars" }
}
