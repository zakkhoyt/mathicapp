//
//  MatrixView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://upload.wikimedia.org/wikipedia/commons/c/cc/Digital_rain_animation_medium_letters_shine.gif
//
import UIKit

@IBDesignable
open class MatrixView: MathView {
    open var color1: UIColor = .white
    open var color2: UIColor = .green
    open var numberOfGhosts: CGFloat = 10
    open var fontSize: CGFloat = 22

    public override var frequency: TimeInterval {
        return 15
    }
    
    public struct Characters {
        let charString: String
        var randomCharacter: String {
            let r = Int.random(in: 0..<charString.count)
            let startIndex = charString.index(charString.startIndex, offsetBy: r)
            let endIndex = charString.index(startIndex, offsetBy: 1)
            return String(charString[startIndex..<endIndex])
        }
    }
    
    public var characters = Characters(charString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    
    private class Element {
        var xPosition: Int
        var yPosition: Int
        var numberOfGhosts: Int
        var characters: [String]
        
        init(xPosition: Int,
             yPosition: Int,
             numberOfGhosts: Int) {
            self.xPosition = xPosition
            self.yPosition = yPosition
            self.numberOfGhosts = numberOfGhosts
            self.characters = []
        }
    }
    
    private var elements: [Element] = []
    private var lastCycleCount = 0
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        func setupElements() {
            guard let superview = superview else { return }
            let numberOfColumns = superview.bounds.width / fontSize
            let numberOfRows = superview.bounds.height / fontSize
            
            var elements: [Element] = []
            for _ in 0..<3 {
                let xPosition = Int.random(in: 0...Int(numberOfColumns))
                let yPosition = Int.random(in: 0...Int(numberOfRows))
                let element = Element(xPosition: xPosition, yPosition: yPosition, numberOfGhosts: Int(numberOfGhosts))
                for _ in 0..<yPosition {
                    element.characters.append(characters.randomCharacter)
                }
                elements.append(element)
            }
            self.elements = elements
            setNeedsDisplay()
        }
        setupElements()
    }
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let numberOfColumns = bounds.width / fontSize
        let numberOfRows = bounds.height / fontSize
        
        let columnWidth = bounds.width / numberOfColumns
        let rowHeight = bounds.height / numberOfRows

        if Int(lastCycleCount) != Int(cycleCount) {
            lastCycleCount = Int(cycleCount)

            func addNewElement() {
                // Try up to 5 times to find an open column
                for _ in 0..<5 {
                    let randomX = Int.random(in: 0...Int(numberOfColumns))
                    if (elements.filter { $0.xPosition  == randomX }).count > 0 { return }
                    
                    let element = Element(xPosition: randomX, yPosition: 0, numberOfGhosts: Int(numberOfGhosts))
                    elements.append(element)
                    break
                }
            }
            addNewElement()
            
            func moveExistingElements() {
                for element in elements {
                    element.yPosition += 1
                    let newCharacter = characters.randomCharacter
                    element.characters.insert(newCharacter, at: 0)
                    if element.characters.count > element.numberOfGhosts {
                        element.characters.removeLast()
                    }
                }
            }
            moveExistingElements()

            func deleteOldElements() {
                elements.removeAll { $0.yPosition > Int(numberOfRows) + $0.numberOfGhosts }
            }
            deleteOldElements()
        }
        
        // Draw elements
        for element in elements {
            // Position of head
            let x = columnWidth * CGFloat(element.xPosition)
            let y = columnWidth * CGFloat(element.yPosition)
            
            for (c, character) in element.characters.enumerated() {
                let textColor = c == 0 ? color1 : color2
                let alpha: CGFloat = CGFloat(element.characters.count - c) / CGFloat(element.characters.count)
                let alphaColor = textColor.withAlphaComponent(alpha)
                let point = CGPoint(x: x + columnWidth / 2.0,
                                    y: y - (CGFloat(c) * rowHeight) + rowHeight / 2.0)
                let font = UIFont.preferredFont(forTextStyle: .title3)
                drawString(character,
                           textColor: alphaColor,
                           at: point,
                           attributes: [.font: font])
            }
        }
    }
}

extension MatrixView: ParameterPanable {
    public var nameX: String? { return nil }
    public var nameY: String? { return nil }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 200 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 0 }
    public var minimumZ: CGFloat { return 0 }
    public var maximumZ: CGFloat { return 0 }
    
    public var valueX: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
    
    public var valueY: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension MatrixView: TitledAnimation {
    public static var animationTitle: String { "Matrix" }
}
