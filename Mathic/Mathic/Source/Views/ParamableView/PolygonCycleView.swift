//
//  RotatingPolygonView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  http://66.media.tumblr.com/1789b63316899d072a23db31f6aec0c4/tumblr_mi301zN7vJ1s5nl47o1_500.gif
//

import UIKit

open class PolygonCycleView: MathView {
    open var numberOfSides: CGFloat = 4
    open var angleOffset: CGFloat = -CGFloat.pi / 2
    
    override public var frequency: TimeInterval {
        return 0.2
    }
    
    open override var previewElapsed: TimeInterval {
        return 1.5
    }
    
    private var polygonCenter: CGPoint {
        return CGPoint(x: bounds.width / 4.0,
                       y: bounds.height / 2.0)
    }
    
    private var polygonRadius: CGFloat {
        return bounds.width / 8.0
    }
    
    private var color: UIColor {
        let swing = maximumX - minimumX
        let hue = (numberOfSides - minimumX) / swing
        return UIColor.hueColor(hue: hue)
    }
    
    private var buffer: [CGFloat] = []
    
    open override func draw(_ rect: CGRect) {
        func drawSeparatorLine() {
            drawLine(point1: CGPoint(x: bounds.midX, y: 0),
                     point2: CGPoint(x: bounds.midX, y: bounds.height),
                     strokeColor: .label,
                     fillColor: nil,
                     lineWidth: 0.5)
        }
        drawSeparatorLine()
        
        let polygonPath = UIBezierPath()
        func drawPolygon() {
            polygonPath.lineWidth = 1.0
            let anglePerStep = 2.0 * CGFloat.pi / numberOfSides
            
            color.setStroke()
            color.withAlphaComponent(0.1).setFill()
            for a in 0...Int(numberOfSides) {
                let x = polygonRadius * cos(angleOffset + CGFloat(a) * anglePerStep)
                let y = polygonRadius * sin(angleOffset + CGFloat(a) * anglePerStep)
                let point = CGPoint(x: polygonCenter.x + x,
                                    y: polygonCenter.y + y)
                if a == 0 {
                    polygonPath.move(to: point)
                } else {
                    polygonPath.addLine(to: point)
                }
            }
            polygonPath.close()
            polygonPath.stroke()
            polygonPath.fill()
        }
        drawPolygon()
        
        func findTangentPoint() -> CGPoint? {
            // Keep decreasing the lenth of the line until we enter polygonPath.
            for i in stride(from: Int(polygonRadius), to: 0, by: -1) {
                let angle = 2.0 * CGFloat.pi * cycle
                let x = CGFloat(i) * cos(angle)
                let y = CGFloat(i) * sin(angle)
                let point = CGPoint(x: polygonCenter.x + x,
                                    y: polygonCenter.y + y)
                if polygonPath.contains(point) {
                    return point
                }
            }
            return nil
        }

        guard let tangentPoint = findTangentPoint() else {
            return
        }
        
        self.drawLine(point1: polygonCenter,
                      point2: tangentPoint,
                      strokeColor: color,
                      fillColor: nil,
                      lineWidth: 0.5)
        
        self.drawCircle(center: tangentPoint,
                        radius: smallDotRadius,
                        strokeColor: .label,
                        fillColor: .label,
                        lineWidth: 1)

        func drawConnectingLine() {
            let point: CGPoint = {
                let x = bounds.width / 2.0
                let y = tangentPoint.y
                return CGPoint(x: x, y: y)
            }()
            self.drawLine(point1: tangentPoint,
                          point2: point,
                          strokeColor: color,
                          fillColor: nil,
                          lineWidth: 0.5)
            
            self.drawCircle(center: point,
                            radius: smallDotRadius,
                            strokeColor: .label,
                            fillColor: .label,
                            lineWidth: 1)

            self.buffer.insert(point.y, at: 0)
            if self.buffer.count > Int(bounds.width / 2.0) {
                self.buffer.removeLast()
            }
        }
        drawConnectingLine()
        
        func drawBuffer() {
            color.setStroke()
            let path = UIBezierPath()
            path.lineWidth = 1.0
            for i in 0..<buffer.count {
                let point = CGPoint(x: bounds.width / 2.0 + CGFloat(i),
                                    y: buffer[i])
                if i == 0 {
                    path.move(to: point)
                } else {
                    path.addLine(to: point)
                }
            }
            path.stroke()
        }
        drawBuffer()
    }
}

extension PolygonCycleView: ParameterPanable {
    public var nameX: String? { return "# of Sides" }
    public var nameY: String? { return "Angle Adjust" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 3 }
    public var maximumX: CGFloat { return 16 }
    public var minimumY: CGFloat { return -CGFloat.pi / 2.0 }
    public var maximumY: CGFloat { return CGFloat.pi / 2.0 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }
    
    public var valueX: CGFloat {
        get { return numberOfSides }
        set { numberOfSides = CGFloat(Int(newValue)) }
    }
    
    public var valueY: CGFloat {
        get { return angleOffset }
        set { angleOffset = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension PolygonCycleView: TitledAnimation {
    public static var animationTitle: String { "Polygon Cycle" }
}
