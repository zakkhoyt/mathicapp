//
//  PolarGridView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//    https://www.reddit.com/r/math/comments/anozr0/row_6_column_2_thisssis_the_abc/?st=jrtfd80c&sh=a77dd299
//    https://i.redd.it/44x3k7f3nse21.gif

import Foundation

@IBDesignable
open class PolarGridView: PhasedCircleView {
    open var numberOfCircles: CGFloat = 12
    open var dotRadius: CGFloat = 4
    
    override open var frequency: TimeInterval {
        return 0.1
    }
    
    private var numberOfGrids: CGFloat {
        return numberOfCircles + 1
    }
    
    private var xDotPoints: [CGPoint] = []
    private var yDotPoints: [CGPoint] = []
    
    private var isFirstSample = true
    private var cachedPaths: [UIBezierPath] = []
    private var previousPhase: CGFloat?
    
    open override func draw(_ rect: CGRect) {
        func resetIfNeeded() {
            if let previousPhase = previousPhase, previousPhase < phase {
                isFirstSample = false
            } else {
                // reset
                isFirstSample = true
                
                // TODO: To save memory, we can use a count of numberOfCircles,
                // but this will complicate lookups
                xDotPoints = [CGPoint](repeating: .zero, count: Int(numberOfGrids))
                yDotPoints = [CGPoint](repeating: .zero, count: Int(numberOfGrids))
                
                // We can't use repeating here becuase UIBezierPath is a class
                // (it would be an array all pointing to the same instance)
                // TODO: To save memory, we can use a count of numberOfCircles,
                // but this will complicate lookups
                cachedPaths.removeAll()
                for _ in 0..<Int(numberOfGrids) {
                    for _ in 0..<Int(numberOfGrids) {
                        let path = UIBezierPath()
                        path.lineWidth = 1.0
                        cachedPaths.append(path)
                    }
                }
            }
            previousPhase = phase
        }
        resetIfNeeded()

        let width: CGFloat = centerRect.width / numberOfGrids
        let height: CGFloat = centerRect.height / numberOfGrids
        let radius: CGFloat = (min(width, height) / 2.0) - 4
        
        // Draws dot but also returns the point of the dot
        func drawInputDot(relativeTo gridCenter: CGPoint,
                          phase: CGFloat,
                          vertically: Bool) -> CGPoint {
            let x = radius * cos(phase)
            let y = radius * sin(phase)
            let dotCenter = CGPoint(x: gridCenter.x + x,
                                    y: gridCenter.y + y)
            drawCircle(center: dotCenter,
                       radius: dotRadius,
                       strokeColor: nil,
                       fillColor: .label,
                       lineWidth: 0)
            
            func drawGuideLine() {
                let point1 = dotCenter
                let point2: CGPoint = {
                    if vertically {
                        return CGPoint(x: point1.x,
                                       y: centerRect.origin.y + centerRect.height)
                    } else {
                        return CGPoint(x: centerRect.origin.x + centerRect.width,
                                       y: point1.y)
                    }
                }()
                drawLine(point1: point1,
                         point2: point2,
                         strokeColor: .lightGray,
                         fillColor: nil,
                         lineWidth: 0.5)
            }
            drawGuideLine()
            
            return dotCenter
        }
        
        func phaseFor(index: Int) -> CGFloat {
            return phase * CGFloat(index)
        }

        func drawInputCircles() {
            let font = UIFont.systemFont(ofSize: width / 4)
            for x in 1..<Int(numberOfGrids) {
                let gridCenter = CGPoint(x: centerRect.origin.x + CGFloat(x) * width + radius,
                                     y: centerRect.origin.y + CGFloat(0) * height + radius)
                let hue = CGFloat(x - 1) / numberOfCircles
                let color: UIColor = {
                    switch traitCollection.userInterfaceStyle {
                    case .dark: return UIColor.pastelColor(hue: hue)
                    default: return UIColor.darkPastelColor(hue: hue)
                    }
                }()
                super.drawCircle(center: gridCenter,
                                 radius: radius,
                                 strokeColor: color,
                                 fillColor: nil,
                                 lineWidth: 1.0)
                let title = "\(x)x"
                super.drawString(title,
                                 textColor: color,
                                 at: gridCenter,
                                 attributes: [.font: font])
                
                xDotPoints[x] = drawInputDot(relativeTo: gridCenter,
                                        phase: phaseFor(index: x),
                                        vertically: true)
            }
            
            for y in 1..<Int(numberOfGrids) {
                UIColor.lightGray.setStroke()
                let gridCenter = CGPoint(x: centerRect.origin.x + CGFloat(0) * width + radius,
                                     y: centerRect.origin.y + CGFloat(y) * height + radius)
                let hue = CGFloat(y - 1) / numberOfCircles
                let color: UIColor = {
                    switch traitCollection.userInterfaceStyle {
                    case .dark: return UIColor.pastelColor(hue: hue)
                    default: return UIColor.darkPastelColor(hue: hue)
                    }
                }()
                super.drawCircle(center: gridCenter,
                                 radius: radius,
                                 strokeColor: color,
                                 fillColor: nil,
                                 lineWidth: 1.0)
                let title = "\(y)x"
                super.drawString(title,
                                 textColor: color,
                                 at: gridCenter,
                                 attributes: [.font: font])

                yDotPoints[y] = drawInputDot(relativeTo: gridCenter,
                                        phase: phaseFor(index: y),
                                        vertically: false)
            }
        }
        drawInputCircles()
        
        func cachedPathsIndex(x: Int, y: Int) -> Int {
            return y * Int(numberOfGrids) + x
        }
        
        func cacheAndDrawIntersetions() {
            for yy in 1..<Int(numberOfGrids) {
                for xx in 1..<Int(numberOfGrids) {
                    // Creat point from cached points
                    let x: CGFloat = xDotPoints[xx].x
                    let y: CGFloat = yDotPoints[yy].y
                    let point = CGPoint(x: x, y: y)
                    
                    // Dot where two lines intersect
                    drawCircle(center: point,
                               radius: 2,
                               strokeColor: nil,
                               fillColor: .label,
                               lineWidth: 0)
                    
                    // Calculate index to get UIBezierPath
                    let index = cachedPathsIndex(x: xx, y: yy)
                    let cachedPath = cachedPaths[index]
                    if isFirstSample {
                        cachedPath.move(to: point)
                    } else {
                        cachedPath.addLine(to: point)
                    }

                    // Interpolate color
                    let hueX = CGFloat(xx - 1) / CGFloat(numberOfCircles)
                    let colorX: UIColor = {
                        switch traitCollection.userInterfaceStyle {
                        case .dark: return UIColor.pastelColor(hue: hueX)
                        default: return UIColor.darkPastelColor(hue: hueX)
                        }
                    }()
                    let hueY = CGFloat(yy - 1) / CGFloat(numberOfCircles)
                    let colorY: UIColor = {
                        switch traitCollection.userInterfaceStyle {
                        case .dark: return UIColor.pastelColor(hue: hueY)
                        default: return UIColor.darkPastelColor(hue: hueY)
                        }
                    }()
                    let color = UIColor.interpolate(color1: colorX, color2: colorY)
                    
                    // Draw the path
                    color.setStroke()
                    cachedPath.stroke()
                }
            }
        }
        cacheAndDrawIntersetions()
    }
}

extension PolarGridView: ParameterPanable {
    public var nameX: String? { return nil }
    public var nameY: String? { return nil }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 0 }
    public var maximumX: CGFloat { return 0 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 0 }
    public var minimumZ: CGFloat { return 0 }
    public var maximumZ: CGFloat { return 0 }
    
    public var valueX: CGFloat {
        get { return 0 }
        set { _ = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return 0 }
        set { _ = newValue.rounded() }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension PolarGridView: TitledAnimation {
    public static var animationTitle: String { "Polar Grid" }
}
