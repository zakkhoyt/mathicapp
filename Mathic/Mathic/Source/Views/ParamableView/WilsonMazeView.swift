//
//  WilsonMazeView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/15/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//
//  https://i.imgur.com/GZ9d7hv.mp4
// https://weblog.jamisbuck.org/2011/1/20/maze-generation-wilson-s-algorithm

import UIKit

open class WilsonMazeView: MathView {
    class Maze {
        enum Direction: CaseIterable {
            case north
            case south
            case east
            case west
        }
        
        struct Cell: Hashable, CustomStringConvertible {
            let x: Int
            let y: Int
            let walls = Direction.allCases
            var description: String { "\(x),\(y)" }
            
            func hash(into hasher: inout Hasher) { hasher.combine(description) }
        }
        
        struct PathStep {
            let cell: Cell
            let direction: Direction
        }
        
        class Maker {
            struct PathMaker {
                var startCell: Cell
                var steps: [PathStep] = []
                
//                func step() {
//                    let currentCell: Maze.Cell = {
//                        guard let lastStep = steps.last else {
//                            return self.startCell
//                        }
//                        return lastStep.cell
//                    }()
//
// //                    let nextCell = randomOutNeighbor(cell: currentCell)
//                }
            }
            
            var paths: [[PathStep]] = []
            var inCells: [String: Cell] = [:]
            var outCells: [String: Cell] = [:]
            var randomOutCell: Cell { [Cell](outCells.values)[Int.random(in: 0..<outCells.count)] }
            var currentPathMaker: PathMaker?
            
            func randomOutNeighbor(cell: Cell) -> Cell {
                for _ in 0..<1000 {
                    let x = Bool.random() ? cell.x + 1 : cell.x - 1
                    let y = Bool.random() ? cell.y + 1 : cell.y - 1
                    let key = "\(x),\(y)"
                    if let cell = outCells[key] {
                        return cell
                    }
                }
                preconditionFailure("Too many iterations")
            }
            
            func step() {
//                guard let currentPathMaker = currentPathMaker else { return }
//                //currentPathMaker.step()
//                let currentCell: Maze.Cell = {
//                    guard let lastStep = currentPathMaker.steps.last else {
//                        return currentPathMaker.startCell
//                    }
//                    return lastStep.cell
//                }()
//
//                let nextCell = randomOutNeighbor(cell: currentCell)
//                let step = PathStep(cell: nextCell, direction: <#T##WilsonMazeView.Maze.Direction#>)
//                currentPathMaker.steps.append(nextCell)
//
//                maze.maker.outCells[cell.description] = nil
//                maze.maker.inCells[cell.description] = cell
//                let randomOutCell = maze.maker.randomOutCell
//                maze.maker.currentPathMaker = Maze.Maker.PathMaker(startCell: randomOutCell)
//
            }
        }
        
        let width: Int
        let height: Int
        var maker = Maker()
        
        init(width: Int, height: Int) {
            self.width = width
            self.height = height
            
            for y in 0..<height {
                for x in 0..<width {
                    let cell = Cell(x: x, y: y)
                    maker.outCells[cell.description] = cell
                }
            }
        }
    }
    
    var maze = Maze(width: 5, height: 5)
    
    let stackView = UIStackView()
    let stepButton = UIButton(type: .system)
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        func setup() {
            self.isUserInteractionEnabled = true
            stackView.axis = .vertical
            addSubview(stackView)
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            
            stepButton.setTitle("Step", for: .normal)
            stepButton.addTarget(self, action: #selector(stepButtonTouchUpInside), for: .touchUpInside)
            stepButton.isUserInteractionEnabled = true
            stepButton.translatesAutoresizingMaskIntoConstraints = false
            stepButton.heightAnchor.constraint(equalToConstant: 200).isActive = true
            stackView.addArrangedSubview(stepButton)
        }
        setup()
        
        stop()
    }
    
//    var pathMaker: Maze.Maker.PathMaker?
    
    @objc
    private func stepButtonTouchUpInside(sender: UIButton) {
        maze.maker.step()
//        if let currentPathMaker = maze.maker.currentPathMaker {
//            currentPathMaker.step()
//        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard let touch = touches.first else { return }
        let point = touch.location(in: self)
        
        let cellWidth: CGFloat = bounds.width / CGFloat(maze.width)
        let x = Int(floor(point.x / cellWidth))
        
        let cellHeight: CGFloat = bounds.height / CGFloat(maze.height)
        let y = Int(floor(point.y / cellHeight))

        let cell = Maze.Cell(x: x, y: y)
        
        // Start a pathMaker
        if maze.maker.currentPathMaker == nil {
            maze.maker.outCells[cell.description] = nil
            maze.maker.inCells[cell.description] = cell
            let randomOutCell = maze.maker.randomOutCell
            maze.maker.currentPathMaker = Maze.Maker.PathMaker(startCell: randomOutCell)
            ()
        }
        
        setNeedsDisplay()
    }
    
    open override func draw(_ rect: CGRect) {
        let cellWidth: CGFloat = bounds.width / CGFloat(maze.width)
        let cellHeight: CGFloat = bounds.height / CGFloat(maze.height)
        
        func drawCellBackground(x xIndex: Int, y yIndex: Int, color: UIColor) {
            let x: CGFloat = CGFloat(xIndex) * cellWidth
            let y: CGFloat = CGFloat(yIndex) * cellHeight

            color.setFill()
            let cellFrame = CGRect(x: x, y: y, width: cellWidth, height: cellHeight)
            let cellPath = UIBezierPath(rect: cellFrame)
            cellPath.fill()
        }
        
        func drawNorthWall(x xIndex: Int, y yIndex: Int) {
            let x: CGFloat = CGFloat(xIndex) * cellWidth
            let y: CGFloat = CGFloat(yIndex) * cellHeight

            let path = UIBezierPath()
            path.move(to: CGPoint(x: x, y: y))
            path.addLine(to: CGPoint(x: x + cellWidth, y: y))
            path.stroke()
        }
        
        func drawEastWall(x xIndex: Int, y yIndex: Int) {
            let x: CGFloat = CGFloat(xIndex) * cellWidth
            let y: CGFloat = CGFloat(yIndex) * cellHeight

            let path = UIBezierPath()
            path.move(to: CGPoint(x: x + cellWidth, y: y))
            path.addLine(to: CGPoint(x: x + cellWidth, y: y + cellHeight))
            path.stroke()
        }
        
        func drawSouthWall(x xIndex: Int, y yIndex: Int) {
            let x: CGFloat = CGFloat(xIndex) * cellWidth
            let y: CGFloat = CGFloat(yIndex) * cellHeight

            let path = UIBezierPath()
            path.move(to: CGPoint(x: x + cellWidth, y: y + cellHeight))
            path.addLine(to: CGPoint(x: x, y: y + cellHeight))
            path.stroke()
        }
        
        func drawWestWall(x xIndex: Int, y yIndex: Int) {
            let x: CGFloat = CGFloat(xIndex) * cellWidth
            let y: CGFloat = CGFloat(yIndex) * cellHeight

            let path = UIBezierPath()
            path.move(to: CGPoint(x: x, y: y + cellHeight))
            path.addLine(to: CGPoint(x: x, y: y))
            path.stroke()
        }
        
        for y in 0..<maze.height {
            for x in 0..<maze.width {
                let cell = Maze.Cell(x: x, y: y)
                backgroundColor?.setStroke()
                
                if maze.maker.outCells[cell.description] != nil {
                    drawCellBackground(x: x, y: y, color: .lightGray)
                } else if maze.maker.inCells[cell.description] != nil {
                    drawCellBackground(x: x, y: y, color: .label)
                }

                cell.walls.forEach {
                    switch $0 {
                    case .north: drawNorthWall(x: x, y: y)
                    case .east: drawEastWall(x: x, y: y)
                    case .south: drawSouthWall(x: x, y: y)
                    case .west: drawWestWall(x: x, y: y)
                    }
                }
            }
        }

        func draw(cell: Maze.Cell, color: UIColor = backgroundColor ?? .black) {
//
//            if maze.maker.outCells[cell.description] != nil {
//                drawCellBackground(x: cell.x, y: cell.y, color: .lightGray)
//            } else if maze.maker.inCells[cell.description] != nil {
//                drawCellBackground(x: cell.x, y: cell.y, color: .label)
//            }
            
            drawCellBackground(x: cell.x, y: cell.y, color: color)

            cell.walls.forEach {
                switch $0 {
                case .north: drawNorthWall(x: cell.x, y: cell.y)
                case .east: drawEastWall(x: cell.x, y: cell.y)
                case .south: drawSouthWall(x: cell.x, y: cell.y)
                case .west: drawWestWall(x: cell.x, y: cell.y)
                }
            }
        }
        
        maze.maker.paths.forEach { steps in
            steps.forEach { draw(cell: $0.cell) }
        }
        
        if let currentPathMaker = maze.maker.currentPathMaker {
            draw(cell: currentPathMaker.startCell, color: .systemPink)
            currentPathMaker.steps.forEach { draw(cell: $0.cell, color: .systemYellow) }
        }
    }
}

extension WilsonMazeView: TitledAnimation {
    public static var animationTitle: String { "Wilson Maze" }
}
