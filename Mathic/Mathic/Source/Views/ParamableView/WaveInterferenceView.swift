//
//  WaveInterferenceView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/14/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class WaveInterferenceView: MathView {
    let numberOfStaticPhases: CGFloat = 2
    var numberOfDynamicPhases: CGFloat = 15
    var amplitude: CGFloat = 0.5
    
    enum WaveType: Int {
        case sine
        case triangle
//        case sawtooth
        case square
    }
    
    var waveTypeValue: CGFloat = 1
    var lineWidth: CGFloat = 4

    open override func draw(_ rect: CGRect) {
        let maxYStatic: CGFloat = bounds.height / 4
        let maxYDynamic: CGFloat = bounds.height / 4
        
        func staticAngle(percent: CGFloat) -> CGFloat {
            (2 * CGFloat.pi * numberOfStaticPhases) * (percent + cycle)
        }
        
        func dynamicAngle(percent: CGFloat) -> CGFloat {
            (2 * CGFloat.pi * numberOfDynamicPhases) * percent
        }

        func calculate(for angle: CGFloat) -> CGFloat {
            let waveType = WaveType(rawValue: Int(waveTypeValue)) ?? .square
            switch waveType {
            case .sine: return sine(angle)
            case .triangle: return triangle(angle)
            case .square: return square(angle)
            }
        }

        func staticY(angle: CGFloat) -> CGFloat {
            maxYStatic * sin(angle)
        }

        func dynamicY(angle: CGFloat) -> CGFloat {
             amplitude * maxYDynamic * calculate(for: angle)
        }
        
        // Draw static wave
        do {
            UIColor.label.setStroke()
            let path = UIBezierPath()
            path.lineWidth = 0.5
            path.move(to: CGPoint(x: 0, y: bounds.center.y))
            for x in 0..<Int(bounds.width - 1) {
                let percent = CGFloat(x) / bounds.width
                let angle = staticAngle(percent: percent)
                let y = staticY(angle: angle)
                let point = CGPoint(x: CGFloat(x), y: bounds.center.y + y)
                path.addLine(to: point)
            }
            path.stroke()
        }
        
        // Draw dynamic wave
        do {
            UIColor.label.setStroke()
            let path = UIBezierPath()
            path.lineWidth = 0.5
            path.move(to: CGPoint(x: 0, y: bounds.center.y))
            for x in 0..<Int(bounds.width - 1) {
                let percent = CGFloat(x) / bounds.width
                let angle = dynamicAngle(percent: percent)
                let y = dynamicY(angle: angle)
                let point = CGPoint(x: CGFloat(x), y: bounds.center.y + y)
                path.addLine(to: point)
            }
            path.stroke()
        }
        
        // Draw Interference wave
        do {
            let path = UIBezierPath()
            path.lineWidth = lineWidth
            path.move(to: CGPoint(x: 0, y: bounds.center.y))
            for x in 0..<Int(bounds.width - 1) {
                let percent = CGFloat(x) / bounds.width
                let sAngle = staticAngle(percent: percent)
                let sY = staticY(angle: sAngle)

                let dAngle = dynamicAngle(percent: percent)
                let dY = dynamicY(angle: dAngle)

                let point = CGPoint(x: CGFloat(x), y: bounds.center.y + sY + dY)
                path.addLine(to: point)
                
                if x == 0 {
                    let sY = staticY(angle: CGFloat.pi / 2)
                    let dY = dynamicY(angle: CGFloat.pi / 2)
                    let hue = abs((sY + dY) / (bounds.height / 2))
                    UIColor(hue: hue, saturation: 1, brightness: 1, alpha: 1).setStroke()
                }
            }
            path.stroke()
        }
    }
}

extension WaveInterferenceView: ParameterPanable {
    public var nameX: String? { return "# Phases" }
    public var nameY: String? { return "Amplitude" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return -4 }
    public var maximumX: CGFloat { return 4 }
    public var minimumY: CGFloat { return -1 }
    public var maximumY: CGFloat { return 1 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfDynamicPhases }
        set { numberOfDynamicPhases = numberOfStaticPhases * pow(2, newValue) }
    }
    
    public var valueY: CGFloat {
        get { return amplitude }
        set { amplitude = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension WaveInterferenceView {
    public var displayableMinimumX: String { "pow(2, \(minimumX.twoSigFigs))" }
    public var displayableMaximumX: String { "pow(2, \(maximumX.twoSigFigs))" }
    public var displayableValueX: String { "pow(2, \((log2(valueX) - 1).twoSigFigs))" }
}

extension WaveInterferenceView: ParameterPinchable {
    public var pinchName: String? { return "Wave Type" }

    public var pinchMinimum: CGFloat { return 0 }
    public var pinchMaximum: CGFloat { return 2 }

    public var pinchValue: CGFloat {
        get { return waveTypeValue }
        set { waveTypeValue = newValue }
    }
}

extension WaveInterferenceView: TitledAnimation {
    public static var animationTitle: String { "Wave Interference" }
}
