//
//  CardiodCircleView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/8/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class CardioidCircleView: MathView {
    public var numberOfDots = CGFloat(16)
    
    public override var frequency: TimeInterval { 0.5 }
    
    open override func draw(_ rect: CGRect) {
        let circleWidth: CGFloat = radius / 5
        // Draw base circle
        drawCircle(center: bounds.center,
                   radius: circleWidth,
                   strokeColor: .systemRed,
                   fillColor: nil,
                   lineWidth: 1)
        
        do {
            let anglePerDot: CGFloat = 2.0 * CGFloat.pi / numberOfDots
            for i in 0..<Int(numberOfDots) {
                let phase: CGFloat = -2.0 * CGFloat.pi * cycle
                let angle: CGFloat = anglePerDot * CGFloat(i)
                // draw point orbiting around main circle
                let point = CGPoint(x: bounds.center.x + circleWidth * cos(phase + angle),
                                    y: bounds.center.y + circleWidth * sin(phase + angle))
                drawCircle(center: point,
                           radius: 4,
                           strokeColor: .label,
                           fillColor: .label,
                           lineWidth: 1)
                
                // draw expanding circle centered on the dot circle
                let progress = ((phase + angle).truncatingRemainder(dividingBy: 2 * CGFloat.pi)) / (2 * CGFloat.pi)
                
//                let dynamicRadius: CGFloat = 2 * circleWidth * (progress * 2 - 1)
                let dynamicRadius: CGFloat = 2 * circleWidth * sin(progress * CGFloat.pi)
                drawCircle(center: point,
                           radius: dynamicRadius,
                           strokeColor: .label,
                           fillColor: nil,
                           lineWidth: 1)
            }
        }
        
//        let anglePerDot: CGFloat = 2.0 * CGFloat.pi / numberOfDots
//        let halfRadius = radius / 2.0
//        let travel = radius * 0.75
//        for i in 0..<Int(numberOfDots) {
//            let subPhase: CGFloat = 3.0 * 2.0 * CGFloat.pi * CGFloat(i) / numberOfDots
//            let percent: CGFloat = 0.5 * sin(phase + subPhase)
//            let angle: CGFloat = anglePerDot * CGFloat(i)
//            let x = bounds.center.x + (halfRadius + percent * travel) * cos(angle)
//            let y = bounds.center.y + (halfRadius + percent * travel) * sin(angle)
//            let point = CGPoint(x: x, y: y)
//            let hue = angle / (CGFloat.pi * 2.0)
//            let color = UIColor.hueColor(hue: hue)
//            drawCircle(center: point,
//                       radius: 4,
//                       strokeColor: color,
//                       fillColor: color,
//                       lineWidth: 1.0)
//        }
    }
}

extension CardioidCircleView: ParameterPanable {
    public var nameX: String? { return "# of Dots" }
    public var nameY: String? { return nil }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 32 }
    public var minimumY: CGFloat { return 0.5 }
    public var maximumY: CGFloat { return 64 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfDots }
        set { numberOfDots = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension CardioidCircleView: TitledAnimation {
    public static var animationTitle: String { "Cardioid Circles" }
}
