//
//  CardiogramView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/15/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class CardiogramView: MathView {
    open var a: CGFloat = 20
    open var maximumLineWidth: CGFloat = 3
    
    private var lineWidth: CGFloat {
        // The higher a, the thinner the line (don't overcrowd the heart)
        // The lower a, the thicker the line
        let minimumLineWidth: CGFloat = 0.5
        let minimumA: CGFloat = 1
        let maximumA: CGFloat = 20
        let clippedA = min(max(abs(self.a), minimumA), maximumA)
        let percentA = 1.0 - ((clippedA - minimumA) / (maximumA - minimumA))
        let lineWidth = minimumLineWidth + (percentA * (maximumLineWidth - minimumLineWidth))
        return lineWidth
    }
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawHeart()
        #if DEBUG
        drawHorizontalLine()
        #endif
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTouch(touch)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTouch(touch)
    }

    private func processTouch(_ touch: UITouch) {
        let point = touch.location(in: self)
        let ratio = point.y / bounds.height
        self.a = 30 * ratio
        setNeedsDisplay()
    }

    open func drawHeart() {
        UIColor.red.setStroke()
        let path = UIBezierPath()
        path.lineWidth = lineWidth
        // print("lineWidth: \(lineWidth)")
        // Expects input -1 ... 1
        func cardio(x: CGFloat) -> CGFloat {
            // x^(2/3) + (0.9 * (3.3 - x^2))^(1/2) * sin(a * π * x)
            return pow(abs(x), CGFloat(2) / CGFloat(3))
                + pow(0.9 * (3.3 - pow(x, 2)), CGFloat(1) / CGFloat(2))
                * sin(a * CGFloat.pi * x)
        }
        
        var pathStarted = false
        // How many steps to take so we are 1 sample per pixel
        let maximumInput: CGFloat = 2.0
        let steps = 2 * bounds.width / maximumInput
        for i in 0..<Int(steps) {
            // Scale input to fit in range of 0.0 ... maximiumInput, one sample per pixel
            let x = maximumInput * (CGFloat(i) / steps * 2.0 - 1.0)
            let y = cardio(x: x)
            if y.isNaN { continue }
            
            let xx = bounds.center.x + (bounds.width / 6.0 * x)
            let midY = bounds.center.y
            let scale = bounds.width / 8.0
            let yOffset = (0.5 * scale) // Center the heart vertically
            let yy = (midY - scale * y) + yOffset
            let point = CGPoint(x: xx, y: yy)
            if !pathStarted {
                pathStarted = true
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        path.stroke()
    }

    open func drawHorizontalLine() {
        let point1: CGPoint = {
            let x = CGFloat(0)
            let y = bounds.center.y
            return CGPoint(x: x, y: y)
        }()
        let point2: CGPoint = {
            let x = bounds.width
            let y = bounds.center.y
            return CGPoint(x: x, y: y)
        }()
        drawLine(point1: point1,
                 point2: point2,
                 strokeColor: UIColor.lightGray,
                 fillColor: nil,
                 lineWidth: 1.0)
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setNeedsDisplay()
    }
}

extension CardiogramView: ParameterPanable {
    public var nameX: String? { return "Density" }
    public var nameY: String? { return "Max Line Width" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 20 }
    public var minimumY: CGFloat { return 0.5 }
    public var maximumY: CGFloat { return 8 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return a }
        set { a = newValue }
    }
    
    public var valueY: CGFloat {
        get { return maximumLineWidth }
        set { maximumLineWidth = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension CardiogramView: TitledAnimation {
    public static var animationTitle: String { "Cardiogram" }
}
