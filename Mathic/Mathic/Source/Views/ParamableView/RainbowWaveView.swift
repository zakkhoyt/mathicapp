//
//  RainbowWaveView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/31/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://gifer.com/en/TaT4
//
//  This one is going to be hard to do without shaders.
//

import UIKit

open class RainbowWaveView: PhasedCircleView {
    open var numberOfHues: CGFloat = 8
    open var numberOfAlphas: CGFloat = 2
    
    open var numberOfPeriods: CGFloat = 1.0
    open var amplitude: CGFloat {
        return radius / 4.0
    }
    open override var frequency: TimeInterval {
        return 0.5
    }

    open override func draw(_ rect: CGRect) {
        let lineWidth = 2.0 * amplitude / CGFloat(numberOfHues)
        let anglePerAlpha = (CGFloat.pi / 2.0) / CGFloat(numberOfAlphas)
        for a in 0..<Int(numberOfAlphas) {
            let phaseShift = anglePerAlpha * CGFloat(a)
            let alpha = CGFloat(a + 1) / CGFloat(numberOfAlphas)
            for h in 0..<Int(numberOfHues) {
                let hue = CGFloat(h) / CGFloat(numberOfHues - 1)
                let color = UIColor(hue: hue, saturation: 1.0, brightness: alpha, alpha: 1.0)
                color.setStroke()
                let path = UIBezierPath()
                path.lineWidth = lineWidth
                path.lineCapStyle = .round
                let yOffset: CGFloat = lineWidth * (CGFloat(h) - CGFloat(numberOfHues) / 2.0)
                let hPercent: CGFloat = CGFloat(h) / CGFloat(numberOfHues - 1)
                let startX = Int(bounds.width * hPercent)
                var isFirstPoint = true
                for xx in startX..<Int(bounds.width) {
                    let x = CGFloat(xx)
                    let subPhase: CGFloat = 2.0 * CGFloat.pi * x / bounds.width
                    let y = amplitude * sin(phase + subPhase + phaseShift)
                    let point = CGPoint(x: x, y: bounds.center.y + y + yOffset)
                    if isFirstPoint {
                        isFirstPoint = false
                        path.move(to: point)
                    } else {
                        path.addLine(to: point)
                    }
                }
                path.stroke()
            }
        }
    }
}

extension RainbowWaveView: ParameterPanable {
    public var nameX: String? { return "# of Hues" }
    public var nameY: String? { return "# of Alphas" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 2 }
    public var maximumX: CGFloat { return 8 }
    public var minimumY: CGFloat { return 1 }
    public var maximumY: CGFloat { return 4 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfHues }
        set { numberOfHues = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return numberOfAlphas }
        set { numberOfAlphas = newValue.rounded() }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension RainbowWaveView: TitledAnimation {
    public static var animationTitle: String { "Rainbow Wave" }
}
