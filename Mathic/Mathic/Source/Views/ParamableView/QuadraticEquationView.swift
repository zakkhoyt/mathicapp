//
//  QuadraticEquationView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/24/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class QuadraticEquationView: FunctionView {
    open var exponentA: CGFloat = 1
    open var b: CGFloat = 1
    open var c: CGFloat = 0
    
    override open var domain: ClosedRange<CGFloat> {
        return -2 ... 2
    }
    
    private func function(_ x: CGFloat) -> CGFloat {
        return pow(x, exponentA) + b * x + c
    }

    open override func draw(_ rect: CGRect) {
        func drawBinomial() {
            drawCartesian(tickMod: 1)
            let color: UIColor = b > 0 ? .green : .red
            
            drawFunction(strokeColor: color, function: function)
            
//            func drawXIntercept() {
//                let xToSolveFor = -c / b
//                if xToSolveFor.isNaN { return }
//                let y = function(xToSolveFor)
//                if y.isNaN { return }
//                let point = pointForPlot(x: xToSolveFor, y: y)
//                drawCircle(center: point,
//                           radius: mediumDotRadius,
//                           strokeColor: .magenta,
//                           fillColor: .magenta,
//                           lineWidth: 1.0)
//
//                let fontSize: CGFloat = 12
//                drawString(String(format: "%.2f", xToSolveFor),
//                           textColor: .magenta,
//                           at: CGPoint(x: point.x, y: point.y + mediumDotRadius + fontSize),
//                           attributes: [
//                            .font: UIFont.systemFont(ofSize: fontSize)
//                    ]
//                )
//            }
//            drawXIntercept()
//            //
//            func drawYIntercept() {
//                let point = pointForPlot(x: 0, y: c)
//                drawCircle(center: point,
//                           radius: mediumDotRadius,
//                           strokeColor: .magenta,
//                           fillColor: .magenta,
//                           lineWidth: 1.0)
//
//                let fontSize: CGFloat = 12
//                drawString(String(format: "%.2f", c),
//                           textColor: .magenta,
//                           at: CGPoint(x: point.x, y: point.y + mediumDotRadius + fontSize),
//                           attributes: [
//                            .font: UIFont.systemFont(ofSize: fontSize)
//                    ]
//                )
//            }
//            drawYIntercept()
        }
        drawBinomial()
        
        func drawEquation() {
            let exponentAString = "\(Int(exponentA))"
            let bSignString = b < 0 ? "-" : "+"
            let bString = String(format: "%.2f", b).replacingOccurrences(of: "-", with: "")
            let cSignString = c < 0 ? "-" : "+"
            let cString = String(format: "%.2f", c).replacingOccurrences(of: "-", with: "")
            let string = "1*x^\(exponentAString) \(bSignString) \(bString)*x \(cSignString) \(cString)"
            drawString(string,
                       textColor: .label,
                       at: CGPoint(x: bounds.width / 2.0, y: bounds.height * 3.0 / 4.0),
                       attributes: [
                        .font: UIFont.systemFont(ofSize: 20),
                        .backgroundColor: UIColor.secondaryLabel.withAlphaComponent(0.25)
                       ]
            )
        }
        drawEquation()
    }
}

extension QuadraticEquationView: ParameterPanable {
    public var nameX: String? { return "B" }
    public var nameY: String? { return "C" }
    public var nameZ: String? { return "A^X" }
    
    public var minimumX: CGFloat { return domain.lowerBound }
    public var maximumX: CGFloat { return domain.upperBound }
    public var minimumY: CGFloat { return range.upperBound }
    public var maximumY: CGFloat { return range.lowerBound }
    public var minimumZ: CGFloat { return 1 }
    public var maximumZ: CGFloat { return 20 }
    
    public var valueX: CGFloat {
        get { return b }
        set { b = newValue }
    }
    
    public var valueY: CGFloat {
        get { return c }
        set { c = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return exponentA }
        set { exponentA = 3 + (0 * newValue)/*floor(newValue)*/ }
    }
}

extension QuadraticEquationView: TitledAnimation {
    public static var animationTitle: String { "Quadratic Equation" }
}
