//
//  PiDigitsView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 3/24/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class PiDigitsView: MathView {
    private var ringRadius: CGFloat {
        return radius - 20
    }
    
    private var textRadius: CGFloat {
        return ringRadius - 20
    }
    
    open override func draw(_ rect: CGRect) {
        func pointFor(index: Int, radius: CGFloat) -> CGPoint? {
            guard index >= 0 && index <= 9 else { return nil }
            let anglePer: CGFloat = 2.0 * CGFloat.pi / 10
            let angle = (CGFloat(index) * anglePer) + (CGFloat.pi / 2.0)
            let x: CGFloat = radius * cos(angle)
            let y: CGFloat = radius * sin(angle)
            return CGPoint(x: bounds.center.x + x,
                                y: bounds.center.y - y)
        }
        
        func drawRing() {
            drawCircle(center: bounds.center,
                       radius: ringRadius,
                       strokeColor: .label,
                       fillColor: nil,
                       lineWidth: 4)
        }
        drawRing()
        
        func drawDigits() {
            for i in 0..<10 {
                guard let point = pointFor(index: i, radius: ringRadius) else {
                    continue
                }
                drawCircle(center: point,
                           radius: mediumDotRadius,
                           strokeColor: nil,
                           fillColor: .green,
                           lineWidth: 1)
                
                guard let textPoint = pointFor(index: i, radius: textRadius) else { continue }
                drawString("\(i)",
                    textColor: .green,
                    at: textPoint)
            }
        }
        drawDigits()
    }
}
