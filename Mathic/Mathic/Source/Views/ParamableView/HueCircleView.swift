//
//  HueCircleView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://i.gifer.com/HJXH.gif

import UIKit

open class HueCircleView: PhasedCircleView {
    public var numberOfDots: CGFloat = 32
    public var dotRadius: CGFloat = 8
    
    public var crossRadius: CGFloat = 8
    
    open override var unitCenter: CGPoint {
        return bounds.center
    }
    
    open override var frequency: TimeInterval {
        return 0.5
    }
    
    open override func draw(_ rect: CGRect) {
        func drawDots() {
            let anglePerDot = 2.0 * CGFloat.pi / numberOfDots
            let travel = self.radius - 2.0 * dotRadius
            for i in 0..<Int(numberOfDots) {
                let angle = CGFloat(i) * anglePerDot
                let x = bounds.center.x + travel * cos(angle)
                let y = bounds.center.y + travel * sin(angle)
                let center = CGPoint(x: x, y: y)
                var hue = angle / (CGFloat.pi * 2.0)
                hue += (1.0 - phase / (CGFloat.pi * 2.0))
                if hue > 1.0 { hue -= 1.0 }
                let color = UIColor.hueColor(hue: hue)
                drawCircle(center: center,
                           radius: dotRadius,
                           strokeColor: color,
                           fillColor: color,
                           lineWidth: 1.0)
            }
        }
        drawDots()
        
        drawCross(center: bounds.center,
                  crossRadius: crossRadius,
                  lineWidth: 1.0,
                  strokeColor: .lightGray)
    }
}

extension HueCircleView: ParameterPanable {
    public var nameX: String? { return "# of Dots" }
    public var nameY: String? { return "Dot Radius" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 32 }
    public var minimumY: CGFloat { return 0.5 }
    public var maximumY: CGFloat { return 64 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfDots }
        set { numberOfDots = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return dotRadius }
        set { dotRadius = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension HueCircleView: TitledAnimation {
    public static var animationTitle: String { "Hue Circle" }
}
