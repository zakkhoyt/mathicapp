//
//  EllipseView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
// https://www.mathopenref.com/ellipsetangent.html

import UIKit

open class EllipseView: MathView {
    open var xPercent: CGFloat = 1.0
    open var yPercent: CGFloat = 0.5
    
    open override var previewElapsed: TimeInterval { 2.5 }
    
    public var majorRadius: CGFloat {
        return max(xPercent, yPercent) * centerRect.width
    }
    
    public var minorRadius: CGFloat {
        return min(xPercent, yPercent) * centerRect.width
    }
    
    public var isMajorAxisHorizontal: Bool {
        return xPercent >= yPercent
    }
    
    public var focusA: CGPoint {
        let x = isMajorAxisHorizontal ? focalLength : 0
        let y = isMajorAxisHorizontal ? 0 : focalLength
        return CGPoint(x: centerRect.center.x + x,
                       y: centerRect.center.y + y)
    }
    
    public var focusB: CGPoint {
        let x = isMajorAxisHorizontal ? focalLength : 0
        let y = isMajorAxisHorizontal ? 0 : focalLength
        return CGPoint(x: centerRect.center.x - x,
                       y: centerRect.center.y - y)
    }
    
    /// The distance between each focus and the center is called the focal length of the ellipse.
    /// The following equation relates the focal length f with the majorRadius p and the minorRadius q:
    /// f^2 = p^2 − q^2
    public var focalLength: CGFloat {
        let pSquared = pow(majorRadius / 2, 2)
        let qSquared = pow(minorRadius / 2, 2)
        let diff = pSquared - qSquared
        return sqrt(diff)
    }
    
    public var tangentPoint: CGPoint {
        let angle = 2.0 * CGFloat.pi * cycle
        return tangentPointFrom(angle: angle)
    }
    
    public var perimeter: CGFloat {
        // pi [ 3(a+b) - sqrt((3a+b)(a+3b))]
        let a = centerRect.width / 2.0 * xPercent
        let b = centerRect.height / 2.0 * yPercent
        return CGFloat.pi * (3 * (a + b) - sqrt((3 * a + b) * (a + 3 * b)))
    }
    
    public var lineLoopLength: CGFloat {
        var output: CGFloat = 0
        output += CGPoint.lineLength(point1: focusA, point2: tangentPoint)
        output += CGPoint.lineLength(point1: tangentPoint, point2: focusB)
        output += CGPoint.lineLength(point1: focusB, point2: focusA)
        return output
    }
    
    private var color: UIColor {
        let bigger = max(majorRadius, minorRadius)
        let smaller = min(majorRadius, minorRadius)
        let hue = smaller / bigger
        return UIColor.hueColor(hue: hue)
    }
    
    func tangentPointFrom(angle: CGFloat) -> CGPoint {
        let x = centerRect.width / 2.0 * xPercent * cos(angle)
        let y = centerRect.height / 2.0 * yPercent * sin(angle)
        return CGPoint(x: centerRect.center.x + x,
                       y: centerRect.center.y + y)
    }
    
    open override func draw(_ rect: CGRect) {
        let ellipsePath = UIBezierPath()
        
        func drawEllipse() {
            color.setStroke()
            color.withAlphaComponent(0.1).setFill()
            let count = Int(bounds.width * 2 * CGFloat.pi)
            for i in 0..<count {
                let percent = CGFloat(i) / CGFloat(count)
                let angle = 2.0 * CGFloat.pi * percent
                let point = tangentPointFrom(angle: angle)
                if i == 0 {
                    ellipsePath.move(to: point)
                } else {
                    ellipsePath.addLine(to: point)
                }
            }
            ellipsePath.stroke()
            ellipsePath.fill()
        }
        drawEllipse()
        
        func drawLineLoop() {
            func drawAndLabelLine(point1: CGPoint, point2: CGPoint) {
                drawLine(point1: point1,
                         point2: point2,
                         strokeColor: .label,
                         fillColor: nil,
                         lineWidth: 0.5)
                let lineLength = CGPoint.lineLength(point1: point1,
                                                    point2: point2)
                let linePoint = CGPoint.lineCenter(point1: point1,
                                                   point2: point2)
                drawString("\(Int(lineLength))",
                    textColor: .label,
                    at: linePoint,
                    attributes: [
                        .font: UIFont.preferredFont(forTextStyle: .caption1)
                    ])
                
                drawCircle(center: point1,
                           radius: smallDotRadius,
                           strokeColor: .label,
                           fillColor: .label,
                           lineWidth: 1.0)
            }
            
            drawAndLabelLine(point1: focusA, point2: tangentPoint)
            drawAndLabelLine(point1: tangentPoint, point2: focusB)
            drawAndLabelLine(point1: focusB, point2: focusA)
        }
        drawLineLoop()
        
        func drawFoci() {
            drawCircle(center: focusA,
                       radius: smallDotRadius,
                       strokeColor: .label,
                       fillColor: .label,
                       lineWidth: 1.0)
            drawCircle(center: focusB,
                       radius: smallDotRadius,
                       strokeColor: .label,
                       fillColor: .label,
                       lineWidth: 1.0)
        }
        drawFoci()
        
        func displayParameters() {
            var parameterSets = [ParameterSet]()
            let focalLengthSet = ParameterSet(name: "Focal Length", value: "\(Int(focalLength))")
            parameterSets.append(focalLengthSet)
        }
    }
}

extension EllipseView: ParameterPanable {
    public var nameX: String? { return "X Scale" }
    public var nameY: String? { return "Y Scale" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 0.00001 }
    public var maximumX: CGFloat { return 1 }
    public var minimumY: CGFloat { return 0.00001 }
    public var maximumY: CGFloat { return 1 }
    public var minimumZ: CGFloat { return 0 }
    public var maximumZ: CGFloat { return 0 }
    
    public var valueX: CGFloat {
        get { return xPercent }
        set { xPercent = newValue }
    }
    
    public var valueY: CGFloat {
        get { return yPercent }
        set { yPercent = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension EllipseView: ParameterDisplayable {
    public var params: [ParameterSet] {
        return [
            ParameterSet(name: "Perimeter", value: "\(Int(perimeter))"),
            ParameterSet(name: "Focal Length", value: "\(Int(focalLength))"),
            ParameterSet(name: "Line Loop Length", value: "\(Int(lineLoopLength))")
        ]
    }
}

extension EllipseView: TitledAnimation {
    public static var animationTitle: String { "Ellipse Geometry" }
}
