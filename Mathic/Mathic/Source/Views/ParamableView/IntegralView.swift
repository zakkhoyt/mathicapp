//
//  IntegralView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class IntegralView: FunctionView {
    open var interval: ClosedRange<CGFloat> = -1 ... 1
    open var dx: CGFloat = 0.25
    
    open var exponentA: CGFloat = 2
    open var a: CGFloat = 1
    open var b: CGFloat = 0
    open var c: CGFloat = 3
    
    public enum Mode: Int, CaseIterable {
        case below
        case middle
        case above
    }
    
    public var mode: Mode = .below
    
    override open var domain: ClosedRange<CGFloat> {
        return -2 ... 2
    }

    override open var range: ClosedRange<CGFloat> {
        return -1 ... 5
    }

    public private(set) var area: CGFloat = 0

    private func function(_ x: CGFloat) -> CGFloat {
        return -a * pow(x, exponentA) + b * x + c
    }
    
    open override func draw(_ rect: CGRect) {
        drawCartesian(tickMod: 1)
        let color: UIColor = b > 0 ? .green : .red
        
        drawFunction(strokeColor: color, function: function)

        func drawInterval() {
            func drawIntervalHalf(x: CGFloat) {
                let point1 = pointForPlot(x: x, y: 0)
                let point2 = CGPoint(x: point1.x, y: 0)
                drawLine(point1: point1,
                         point2: point2,
                         strokeColor: .label,
                         fillColor: nil,
                         lineWidth: 0.5)
            }
            drawIntervalHalf(x: interval.lowerBound)
            drawIntervalHalf(x: interval.upperBound)
        }
        drawInterval()
        
        func drawIntegral() {
            // interval: -1 ... 1
            // dx: 1
            // start at i
            // end at i + dx
            // y = min(f(start), f(end))
            
            if dx > (interval.upperBound - interval.lowerBound) { return }
            
            var area: CGFloat = 0
            for x in stride(from: interval.lowerBound, through: interval.upperBound, by: dx) {
                let lower = x
                let upper = x + dx
                if upper > interval.upperBound { continue }
                
                let y: CGFloat = {
                    switch mode {
                    case .below: return min(function(lower), function(upper))
                    case .middle: return (function(lower) + function(upper)) / 2.0
                    case .above: return max(function(lower), function(upper))
                    }
                }()
                let color: UIColor = y > 0 ? .cyan : .red
                color.setStroke()
                color.withAlphaComponent(0.1).setFill()
                let point1 = pointForPlot(x: x, y: 0)
                let point2 = pointForPlot(x: x, y: y)
                let point3 = pointForPlot(x: x + dx, y: y)
                let point4 = pointForPlot(x: x + dx, y: 0)
                let path = UIBezierPath()
                path.move(to: point1)
                path.addLine(to: point2)
                path.addLine(to: point3)
                path.addLine(to: point4)
                path.close()
                path.stroke()
                path.fill()
                
                // Add each rectangle to area approimation
                if y > 0 {
                    area += dx * y
                }
            }
            self.area = area
        }
        drawIntegral()
        
        func drawEquation() {
            let exponentAString = "\(Int(exponentA))"
            let bSignString = b < 0 ? "-" : "+"
            let bString = String(format: "%.2f", b).replacingOccurrences(of: "-", with: "")
            let cSignString = c < 0 ? "-" : "+"
            let cString = String(format: "%.2f", c).replacingOccurrences(of: "-", with: "")
            let string = "1*x^\(exponentAString) \(bSignString) \(bString)*x \(cSignString) \(cString)"
            drawString(string,
                       textColor: .label,
                       at: CGPoint(x: bounds.width / 2.0, y: bounds.height * 3.0 / 4.0),
                       attributes: [
                        .font: UIFont.preferredFont(forTextStyle: .title3),
                        .backgroundColor: UIColor.secondaryLabel.withAlphaComponent(0.25)
                       ]
            )
        }
        drawEquation()
    }
}

extension IntegralView: ParameterPanable {
    public var nameX: String? { return "interval" }
    public var nameY: String? { return "dx" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 0.0001 }
    public var maximumX: CGFloat { return domain.upperBound }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return domain.upperBound / 4 }
    public var minimumZ: CGFloat { return 1 }
    public var maximumZ: CGFloat { return 1 }
    
    public var valueX: CGFloat {
        get { return interval.upperBound }
        set { interval = -newValue ... newValue }
    }
    
    public var valueY: CGFloat {
        get { return dx }
        set { dx = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 1 }
        set { _ = newValue }
    }
}

extension IntegralView: ParameterDisplayable {
    public var params: [ParameterSet] {
        return [
            ParameterSet(name: "Area", value: String(format: "%.2f", area))
        ]
    }
}

extension IntegralView: TitledAnimation {
    public static var animationTitle: String { "Intergral" }
}
