//
//  PurpleDotsView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class PurpleDotsView: PhasedCircleView {
    public var numberOfDots: CGFloat = 12
    public var dotRadius: CGFloat = 16
    public var crossRadius: CGFloat = 8
    
    open override var unitCenter: CGPoint {
        return bounds.center
    }
    
    open override var frequency: TimeInterval {
        return 1.0
    }
    
    open override func draw(_ rect: CGRect) {
        func drawDots() {
            let anglePerDot = 2.0 * CGFloat.pi / CGFloat(numberOfDots)
            let travel = self.radius - 2.0 * dotRadius
            for i in 0..<Int(numberOfDots) {
                let indexToHide: Int = Int((phase / (2.0 * CGFloat.pi)) * numberOfDots)
                if i == indexToHide { continue; }
                let angle = CGFloat(i) * anglePerDot
                let x = bounds.center.x + travel * cos(angle)
                let y = bounds.center.y + travel * sin(angle)
                let center = CGPoint(x: x, y: y)
                drawCircle(center: center,
                           radius: dotRadius,
                           strokeColor: UIColor.magenta,
                           fillColor: UIColor.magenta,
                           lineWidth: 1.0)
            }
        }
        drawDots()

        drawCross(center: bounds.center,
                  crossRadius: crossRadius,
                  lineWidth: 1.0,
                  strokeColor: .label)
    }
}

extension PurpleDotsView: ParameterPanable {
    public var nameX: String? { return "# of Dots" }
    public var nameY: String? { return "Dot Radius" }
    public var nameZ: String? { return "Cross Radius" }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 32 }
    public var minimumY: CGFloat { return 0.5 }
    public var maximumY: CGFloat { return 64 }
    public var minimumZ: CGFloat { return 4 }
    public var maximumZ: CGFloat { return min(bounds.width / 2.0, bounds.height / 2.0) }

    public var valueX: CGFloat {
        get { return numberOfDots }
        set { numberOfDots = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return dotRadius }
        set { dotRadius = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return crossRadius }
        set { crossRadius = newValue }
    }
}

extension PurpleDotsView: TitledAnimation {
    public static var animationTitle: String { "Purple Dots" }
}
