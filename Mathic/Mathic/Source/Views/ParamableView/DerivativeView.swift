//
//  DerivativeView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class DerivativeView: FunctionView {
    open var atX: CGFloat = 0
    open var dx: CGFloat = 1
    
    open var exponentA: CGFloat = 2
    open var b: CGFloat = 0
    open var c: CGFloat = 1
    
    override open var domain: ClosedRange<CGFloat> {
        return -2 ... 2
    }
    
    private func function(_ x: CGFloat) -> CGFloat {
        return -pow(x, exponentA) + b * x + c
    }

    open override func draw(_ rect: CGRect) {
        drawCartesian(tickMod: 1)
        let color: UIColor = b > 0 ? .green : .red
        
        drawFunction(strokeColor: color, function: function)
        
        func drawAtX() {
            let x = atX
            let y = function(x)
            let point = pointForPlot(x: x, y: y)
            drawCircle(center: point,
                       radius: smallDotRadius,
                       strokeColor: .label,
                       fillColor: .label,
                       lineWidth: 1)
        }
        drawAtX()
        
        func drawDerivativeLine() {
            let point1: CGPoint = {
                let x = atX - dx
                let y = function(x)
                return pointForPlot(x: x, y: y)
            }()
            drawCircle(center: point1,
                       radius: smallDotRadius,
                       strokeColor: .label,
                       fillColor: .label,
                       lineWidth: 1)
            
            let point2: CGPoint = {
                let x = atX + dx
                let y = function(x)
                return pointForPlot(x: x, y: y)
            }()
            drawCircle(center: point2,
                       radius: smallDotRadius,
                       strokeColor: .label,
                       fillColor: .label,
                       lineWidth: 1)
            
            // Draw line connecgting point1 and point2
            drawLine(point1: point1,
                     point2: point2,
                     strokeColor: .label,
                     fillColor: nil,
                     lineWidth: 0.5)
            
            func extendLinesToViewEdge() {
                let slope = CGPoint.slope(point1: point1, point2: point2)
                
                func drawLeftLine() {
                    let x = bounds.origin.x
                    let dx = point1.x - x
                    let y = point1.y + dx * slope
                    let point = CGPoint(x: x, y: y)
                    drawLine(point1: point1,
                             point2: point,
                             strokeColor: .green,
                             fillColor: nil,
                             lineWidth: 0.5)
                }
                drawLeftLine()
                
                func drawRightLine() {
                    let x = bounds.origin.x + bounds.width
                    let dx = x - point2.x
                    let y = point2.y + dx * -slope
                    let point = CGPoint(x: x, y: y)
                    drawLine(point1: point2,
                             point2: point,
                             strokeColor: .green,
                             fillColor: nil,
                             lineWidth: 0.5)
                }
                drawRightLine()
            }
            extendLinesToViewEdge()
        }
        drawDerivativeLine()
        
        func drawEquation() {
            let exponentAString = "\(Int(exponentA))"
            let bSignString = b < 0 ? "-" : "+"
            let bString = String(format: "%.2f", b).replacingOccurrences(of: "-", with: "")
            let cSignString = c < 0 ? "-" : "+"
            let cString = String(format: "%.2f", c).replacingOccurrences(of: "-", with: "")
            let string = "1*x^\(exponentAString) \(bSignString) \(bString)*x \(cSignString) \(cString)"
            drawString(string,
                       textColor: .label,
                       at: CGPoint(x: bounds.width / 2.0, y: bounds.height * 3.0 / 4.0),
                       attributes: [
                        .font: UIFont.preferredFont(forTextStyle: .title3),
                        .backgroundColor: UIColor.secondaryLabel.withAlphaComponent(0.25)
                       ]
            )
        }
        drawEquation()
    }
}

extension DerivativeView: ParameterPanable {
    public var nameX: String? { return "x" }
    public var nameY: String? { return "dx" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return domain.lowerBound }
    public var maximumX: CGFloat { return domain.upperBound }
    public var minimumY: CGFloat { return domain.lowerBound }
    public var maximumY: CGFloat { return domain.upperBound }
    public var minimumZ: CGFloat { return 1 }
    public var maximumZ: CGFloat { return 1 }
    
    public var valueX: CGFloat {
        get { return atX }
        set { atX = newValue }
    }
    
    public var valueY: CGFloat {
        get { return dx }
        set { dx = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 1 }
        set { _ = newValue }
    }
}

extension DerivativeView: TitledAnimation {
    public static var animationTitle: String { "Derviative" }
}
