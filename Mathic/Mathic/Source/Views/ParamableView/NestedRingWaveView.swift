//
//  NestedRingWaveView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://i.gifer.com/MZny.gif

import UIKit

open class NestedRingWaveView: PhasedCircleView {
    open var numberOfRings: CGFloat = 20
    open var saturation: CGFloat = 0.0
    
    open override var frequency: TimeInterval {
        return 1.0
    }

    open override var previewElapsed: TimeInterval { 2.5 }
    
    open override func draw(_ rect: CGRect) {
        let ringThickness = (radius / CGFloat(numberOfRings)) / 2.0
        let halfRingThickness = ringThickness / 2.0
        let delayPerRing = (1.0 / frequency) / TimeInterval(numberOfRings)
        
        func normalizedCycle(offset: TimeInterval) -> TimeInterval {
            let time = elapsed - offset
            let normalizedPercent = 2.0 * (time).truncatingRemainder(dividingBy: cycleDuration) / cycleDuration - 1.0
            return max(0, normalizedPercent)
        }
        
        for i in 0..<Int(numberOfRings) {
            let offset = TimeInterval(i) * delayPerRing * 2
            let normalized = normalizedCycle(offset: offset)
            let hue = CGFloat(normalized)
            let color: UIColor = {
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0)
                default:
                    return UIColor(hue: hue, saturation: saturation, brightness: saturation, alpha: 1.0)
                }
            }()
            
            let center: CGPoint = {
                let phase = CGFloat(normalized * 2.0 * TimeInterval.pi) - (CGFloat.pi / 2.0)
                let x = halfRingThickness * cos(phase)
                let y = halfRingThickness * sin(phase)
                return CGPoint(x: bounds.center.x + x, y: bounds.center.y + y + halfRingThickness)
            }()
            
            let radius = 2.0 * ringThickness * CGFloat(i + 1)
            drawCircle(center: center,
                       radius: radius - halfRingThickness,
                       strokeColor: color,
                       fillColor: nil,
                       lineWidth: ringThickness)
        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTouch(touch)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTouch(touch)
    }
    
    private func processTouch(_ touch: UITouch) {
        let point = touch.location(in: self)
        
        let xRatio = point.x / bounds.width
        saturation = xRatio

        let yRatio = point.y / bounds.height
        numberOfRings = 50 * yRatio
        
        setNeedsDisplay()
    }
}

extension NestedRingWaveView: ParameterPanable {
    public var nameX: String? { return "# of Rings" }
    public var nameY: String? { return "Saturation" }
    public var nameZ: String? { return nil }

    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 50 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 1.0 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }
    
    public var valueX: CGFloat {
        get { return numberOfRings }
        set { numberOfRings = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return saturation }
        set { saturation = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension NestedRingWaveView: TitledAnimation {
    public static var animationTitle: String { "Nested Ring Wave" }
}
