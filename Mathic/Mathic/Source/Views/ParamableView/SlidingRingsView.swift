//
//  SlidingRingsView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class SlidingRingsView: PhasedCircleView {
    open var numberOfRings: CGFloat = 30
    open var saturation: CGFloat = 0.25
    
    override open var previewElapsed: TimeInterval {
        return 1.0
    }

    open override var unitCenter: CGPoint {
        return bounds.center
    }

    open override func draw(_ rect: CGRect) {
        let radiusPerRing = radius / CGFloat(numberOfRings)
        
        func drawRings(center: CGPoint, color: UIColor) {
            color.setStroke()
            let lineWidth = radius / CGFloat(numberOfRings * 2)
            for i in 0..<Int(numberOfRings) {
                let radius = radiusPerRing * CGFloat(i)
                let rect = CGRect(x: center.x - radius,
                                  y: center.y - radius,
                                  width: 2.0 * radius,
                                  height: 2.0 * radius)
                let path = UIBezierPath(ovalIn: rect)
                path.lineWidth = lineWidth
                switch traitCollection.userInterfaceStyle {
                case .dark: path.stroke(with: .plusLighter, alpha: 1.0)
                default: path.stroke(with: .plusDarker, alpha: 1.0)
                }
            }
        }
        
        let halfRadius = radius / 2.0

        let color1: UIColor
        let color2: UIColor
        let color3: UIColor
        switch traitCollection.userInterfaceStyle {
        case .dark:
            color1 = UIColor.blue.saturate(saturation: saturation)
            color2 = UIColor.green.saturate(saturation: saturation)
            color3 = UIColor.red.saturate(saturation: saturation)
        default:
            color1 = UIColor.blue.brightness(brightness: saturation)
            color2 = UIColor.green.brightness(brightness: saturation)
            color3 = UIColor.red.brightness(brightness: saturation)
        }
        
        let point1 = bounds.center
        drawRings(center: point1, color: color1)
        
        let point2 = CGPoint(x: bounds.center.x + halfRadius - (halfRadius * cos(phase)),
                             y: bounds.center.y)
        drawRings(center: point2, color: color2)
        
        let point3 = CGPoint(x: bounds.center.x - halfRadius + (halfRadius * cos(phase)),
                             y: bounds.center.y)
        drawRings(center: point3, color: color3)
    }
}

extension SlidingRingsView: ParameterPanable {
    public var nameX: String? { return "# of Rings" }
    public var nameY: String? { return "Saturation" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 50 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 1.0 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfRings }
        set { numberOfRings = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return CGFloat(saturation) }
        set { saturation = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension SlidingRingsView: TitledAnimation {
    public static var animationTitle: String { "Sliding Rings" }
}
