//
//  DerivativeView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/24/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class LinearEquationView: FunctionView {
    open var exponentA: CGFloat = 1
    open var m: CGFloat = 1
    open var b: CGFloat = 0
    
    override open var domain: ClosedRange<CGFloat> {
        return -22 ... 22
    }

    private func function(_ x: CGFloat) -> CGFloat {
        return m * x + b
    }

    private var lineWidth: CGFloat = 1.0
    private var dotRadius: CGFloat = 2

    open override func draw(_ rect: CGRect) {
        func drawBinomial() {
            drawCartesian(tickMod: 10)
            let color: UIColor = m > 0 ? .green : .red
            
            let function: ((CGFloat) -> CGFloat) = { [weak self] x in
                guard let safeSelf = self else { return 0 }
                return safeSelf.m * x + safeSelf.b
            }
            drawFunction(strokeColor: color, function: function, lineWidth: lineWidth)
            
            func drawXIntercept() {
                let xToSolveFor = -b / m
                if xToSolveFor.isNaN { return }
                let y = function(xToSolveFor)
                if y.isNaN { return }
                let point = pointForPlot(x: xToSolveFor, y: y)
                drawCircle(center: point,
                           radius: dotRadius,
                           strokeColor: .magenta,
                           fillColor: .magenta,
                           lineWidth: 1.0)
                
                let fontSize: CGFloat = 12
                drawString(String(format: "%.2f", xToSolveFor),
                           textColor: .magenta,
                           at: CGPoint(x: point.x, y: point.y + mediumDotRadius + fontSize),
                           attributes: [
                            .font: UIFont.preferredFont(forTextStyle: .footnote)
                           ]
                )
            }
            drawXIntercept()

            func drawYIntercept() {
                let point = pointForPlot(x: 0, y: b)
                drawCircle(center: point,
                           radius: dotRadius,
                           strokeColor: .magenta,
                           fillColor: .magenta,
                           lineWidth: 1.0)
                
                let fontSize: CGFloat = 12
                drawString(String(format: "%.2f", b),
                           textColor: .magenta,
                           at: CGPoint(x: point.x, y: point.y + mediumDotRadius + fontSize),
                           attributes: [
                            .font: UIFont.preferredFont(forTextStyle: .footnote)
                           ]
                )
            }
            drawYIntercept()
        }
        drawBinomial()
        
        func drawEquation() {
            let mSignString = m < 0 ? "-" : "+"
            let mString = String(format: "%.2f", m).replacingOccurrences(of: "-", with: "")
            let bSignString = b < 0 ? "-" : "+"
            let bString = String(format: "%.2f", b).replacingOccurrences(of: "-", with: "")
            let string = "\(mSignString) \(mString)*x \(bSignString) \(bString)"
            drawString(string,
                       textColor: .label,
                       at: CGPoint(x: bounds.width / 2.0, y: bounds.height * 3.0 / 4.0),
                       attributes: [
                        .font: UIFont.preferredFont(forTextStyle: .title3),
                        .backgroundColor: UIColor.secondaryLabel.withAlphaComponent(0.25)
                       ]
            )
        }
        drawEquation()
    }
}

extension LinearEquationView: ParameterPanable {
    public var nameX: String? { return "Slope" }
    public var nameY: String? { return "Y-Intercept" }
    public var nameZ: String? { return nil }

    public var minimumX: CGFloat { return domain.lowerBound }
    public var maximumX: CGFloat { return domain.upperBound }
    public var minimumY: CGFloat { return range.upperBound }
    public var maximumY: CGFloat { return range.lowerBound }
    public var minimumZ: CGFloat { return 1 }
    public var maximumZ: CGFloat { return 1 }

    public var valueX: CGFloat {
        get { return m }
        set { m = newValue }
    }

    public var valueY: CGFloat {
        get { return b }
        set { b = newValue }
    }

    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension LinearEquationView: ParameterPinchable {
    public var pinchName: String? { return "Line Width" }
    
    public var pinchMinimum: CGFloat { return 0.5 }
    public var pinchMaximum: CGFloat { return 12.0 }
    
    public var pinchValue: CGFloat {
        get { return lineWidth }
        set { lineWidth = max(min(newValue, pinchMaximum), pinchMinimum) }
    }
}

extension LinearEquationView: ParameterRotatable {
    public var rotateName: String? { return "Intercept Dot Size" }

    public var rotateMinimum: CGFloat { return 4 }
    public var rotateMaximum: CGFloat { return 22 }

    public var rotateValue: CGFloat {
        get { return dotRadius }
        set { dotRadius = max(min(newValue, rotateMaximum), rotateMinimum) }
    }
}

extension LinearEquationView: TitledAnimation {
    public static var animationTitle: String { "Linear Equation" }
}
