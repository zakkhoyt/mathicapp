//
//  RotatingPlusView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://media.giphy.com/media/VakJhPMYg9qN2/giphy.gif
//

import UIKit

@IBDesignable
open class RotatingPlusView: PhasedCircleView {
    open var numberOfPluses: CGFloat = 20

    public override var frequency: TimeInterval {
        return 0.2
    }
    
    override open func draw(_ rect: CGRect) {
        func drawPlus(in rect: CGRect,
                      rotation: CGFloat,
                      fillColor: UIColor) {
            func plusPath(rect: CGRect) -> UIBezierPath {
                func pointIndex(x: Int, y: Int) -> CGPoint {
                    let thirdWidth = rect.width / 3.0
                    let thirdHeight = rect.height / 3.0
                    return CGPoint(x: rect.origin.x + CGFloat(x) * thirdWidth,
                                   y: rect.origin.y + CGFloat(y) * thirdHeight)
                }
                let path = UIBezierPath()
                path.move(to: pointIndex(x: 0, y: 1)) // 1
                path.addLine(to: pointIndex(x: 1, y: 1)) // 2
                path.addLine(to: pointIndex(x: 1, y: 0)) // 3
                path.addLine(to: pointIndex(x: 2, y: 0)) // 4
                path.addLine(to: pointIndex(x: 2, y: 1)) // 5
                path.addLine(to: pointIndex(x: 3, y: 1)) // 6
                path.addLine(to: pointIndex(x: 3, y: 2)) // 7
                path.addLine(to: pointIndex(x: 2, y: 2)) // 8
                path.addLine(to: pointIndex(x: 2, y: 3)) // 9
                path.addLine(to: pointIndex(x: 1, y: 3)) // 10
                path.addLine(to: pointIndex(x: 1, y: 2)) // 11
                path.addLine(to: pointIndex(x: 0, y: 2)) // 12
                return path
            }
            let path = plusPath(rect: rect)
            let translate = CGAffineTransform(translationX: -rect.center.x, y: -rect.center.y)
            let rotate = CGAffineTransform(rotationAngle: rotation)
            // Rotate around the center of the plus by first translating, rotating, then undoing the translate
            path.apply(translate)
            path.apply(rotate)
            path.apply(translate.inverted())

            fillColor.setFill()
            path.fill()
        }

        let whiteBackground = normalizedCycle < 0 ? false : true
        let rotation = CGFloat.pi / 2 * abs(normalizedCycle)
        
        func drawBackground() {
            if whiteBackground {
                UIColor.white.setFill()
            } else {
                UIColor.darkGray.setFill()
            }
            
            let path = UIBezierPath(rect: centerRect)
            path.fill()
        }
        
        func clipToCenterRect() {
            let context = UIGraphicsGetCurrentContext()
            context?.saveGState()
            let path = UIBezierPath(rect: centerRect)
            path.addClip()
        }

        func drawPlusses() {
            let width = centerRect.width / numberOfPluses
            let height = width
            let thirdWidth = width / 3.0
            let thirdHeight = height / 3.0
            
            let delta = Int(numberOfPluses) / 3 + 1
            let xCount = Int(numberOfPluses) + delta
            let yCount = Int(numberOfPluses) + delta

            let xOffset: CGFloat = whiteBackground ? 0 : 1 * thirdWidth
            let yOffset: CGFloat = whiteBackground ? 0 : -2 * thirdHeight
            for y in 0..<yCount {
                for x in -delta..<xCount - delta {
                    let rect = CGRect(x: xOffset + centerRect.origin.x + CGFloat(x) * width + CGFloat(y) * thirdWidth,
                                      y: yOffset + centerRect.origin.y + CGFloat(y) * height - CGFloat(x) * thirdHeight,
                                      width: width,
                                      height: height)
                    drawPlus(in: rect, rotation: rotation, fillColor: whiteBackground ? .darkGray : .white)
                }
            }
        }
        clipToCenterRect()
        drawBackground()
        drawPlusses()
    }
}

extension RotatingPlusView: ParameterPanable {
    public var nameX: String? { return "# of Plusses" }
    public var nameY: String? { return nil }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 2 }
    public var maximumX: CGFloat { return 30 }
    public var minimumY: CGFloat { return 0 }
    public var maximumY: CGFloat { return 0 }
    public var minimumZ: CGFloat { return 0 }
    public var maximumZ: CGFloat { return 0 }
    
    public var valueX: CGFloat {
        get { return numberOfPluses }
        set { numberOfPluses = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return 0 }
        set { _ = newValue.rounded() }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension RotatingPlusView: TitledAnimation {
    public static var animationTitle: String { "Rotating Crosses" }
}
