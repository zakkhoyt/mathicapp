//
//  QuadraticBezierView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/21/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class QuadraticBezierView: MathView {
    // MARK: Private vars
    
    private var touchPoint: CGPoint = CGPoint(x: 1, y: 0)
    private var controlItems: [ControlItem] = []
    private var controlPoints: [CGPoint] {
        var points = controlItems.compactMap { $0.point }
        points.append(CGPoint(x: 0.1, y: 0.1))
        return points
    }
    private var controlPointViews: [ControlPointView] { controlItems.compactMap { $0.view } }
    private lazy var panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan))
    
    // MARK: Override functions
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        stop()
        isUserInteractionEnabled = true
        setupControlPointViews()
        addGestureRecognizer(panGesture)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        for controlItem in controlItems {
            let x = controlItem.point.x * bounds.width
            let y = (1.0 - controlItem.point.y) * bounds.height
            controlItem.view.center = CGPoint(x: x, y: y)
        }
    }

    open override func draw(_ rect: CGRect) {
        let strideValue = 20
        drawTangents(stride: strideValue)
        drawBezierPath(stride: strideValue)
        drawTouchTangent(t: touchPoint.x / bounds.width)
    }
    
    // MARK: Private functions
    
    private func setupControlPointViews() {
        let size: CGFloat = 44
        let halfSize = size / 2
        let numberOfControlPoints = 4
        for i in 0..<numberOfControlPoints {
            let controlPointView = ControlPointView(text: "P\(i)")
            controlPointView.frame = CGRect(
                x: halfSize + CGFloat(i) * size,
                y: halfSize + CGFloat(i) * size,
                width: size,
                height: size
            )
            controlPointView.backgroundColor = {
                switch i {
                case 0: return UIColor.cyan
                case 1: return UIColor.magenta
                default: return UIColor.red
                }
            }()
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan))
            controlPointView.addGestureRecognizer(panGesture)
            addSubview(controlPointView)
            
            let controlPoint: CGPoint = {
                switch i {
                case 0: return CGPoint(x: 0, y: 0)
                case 1: return CGPoint(x: 0.1, y: 0.9)
                case 2: return CGPoint(x: 0.9, y: 0.1)
                case 3: return CGPoint(x: 1, y: 1)
                default: return .zero
                }
            }()

            let controlItem = ControlItem(point: controlPoint, view: controlPointView)
            controlItems.append(controlItem)
        }
    }
    
    /// Draws faint tangent lines every `strideValue` points along the bezier curve.
    /// - Parameter strideValue: A value in points.
    private func drawTangents(stride strideValue: Int) {
        for i in stride(from: 0, to: Int(bounds.width), by: strideValue) {
            let t = CGFloat(i) / bounds.width
            do {
                let solution = try Bezier.solve(
                    points: controlPoints,
                    t: t
                )
                drawLine(
                    point1: expand(point: solution.tangentPoint1),
                    point2: expand(point: solution.tangentPoint2),
                    strokeColor: UIColor.systemGreen.withAlphaComponent(0.4),
                    fillColor: nil,
                    lineWidth: 0.5
                )
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    /// Draws the bezier path itself with a segment every `strideValue` points.
    /// - Parameter strideValue: A value in points.
    private func drawBezierPath(stride strideValue: Int) {
        UIColor.label.setStroke()
        let path = UIBezierPath()
        path.lineWidth = 4
        
        let strideValue = 20
        for i in stride(from: 0, to: Int(bounds.width), by: strideValue) {
            let t = CGFloat(i) / bounds.width
            do {
                let solution = try Bezier.solve(
                    points: controlPoints,
                    t: t
                )
                let point = expand(point: solution.point)
                if i == 0 {
                    path.move(to: point)
                } else {
                    path.addLine(to: point)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        UIColor.label.setStroke()
        path.stroke()
    }
            
    /// Draws a bold tangent line along the bezier path which intersects at the current touchPoint.
    /// - Parameter t: T value from 0...1
    private func drawTouchTangent(t: CGFloat) {
        do {
            let solution = try Bezier.solve(
                points: controlPoints,
                t: t
            )
            drawLine(
                point1: expand(point: solution.tangentPoint1),
                point2: expand(point: solution.tangentPoint2),
                strokeColor: .magenta,
                fillColor: nil,
                lineWidth: 1
            )
            
            let point = CGPoint.interpolate(
                percent: t,
                point1: solution.tangentPoint1,
                point2: solution.tangentPoint2
            )
            drawCircle(
                center: expand(point: point),
                radius: 8,
                strokeColor: .magenta,
                fillColor: .magenta,
                lineWidth: 1
            )
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc
    private func pan(sender: UIPanGestureRecognizer) {
        switch sender {
        case self.panGesture:
            touchPoint = sender.location(in: self)
            setNeedsDisplay()
        default:
            guard let cpView = sender.view as? ControlPointView else { return }
            switch sender.state {
            case .began:
                cpView.startPoint = cpView.center
                cpView.panStart = sender.location(in: self)
            case .changed, .ended:
                let point = sender.location(in: self)
                let deltaX = point.x - cpView.panStart.x
                let deltaY = point.y - cpView.panStart.y
                let newPoint = CGPoint(
                    x: cpView.startPoint.x + deltaX,
                    y: cpView.startPoint.y + deltaY
                ).clip(to: bounds)
                cpView.center = newPoint
                
                do {
                    let controlPoint = contract(point: newPoint)
                    guard let index = controlPointViews.firstIndex(of: cpView) else { return }
                    controlItems[index].point = controlPoint
                }
                
                setNeedsDisplay()
            default: break
            }
        }
    }
}

extension QuadraticBezierView: TitledAnimation {
    public static var animationTitle: String { "Cubic Bezier" }
}

extension CGPoint {
    func clip(to rect: CGRect) -> CGPoint {
        CGPoint(x: min(max(rect.origin.x, x), rect.origin.x + rect.size.width),
                y: min(max(rect.origin.y, y), rect.origin.y + rect.size.height))
    }
}

extension QuadraticBezierView {
    class ControlPointView: UIView {
        var startPoint: CGPoint = .zero
        var panStart: CGPoint = .zero
        
        init(text: String) {
            super.init(frame: .zero)
            commonInit(text: text)
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            commonInit()
        }
        
        private func commonInit(text: String = "") {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            addSubview(label)
            label.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            label.topAnchor.constraint(equalTo: topAnchor).isActive = true
            label.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            label.text = text
            label.font = UIFont.preferredFont(forTextStyle: .callout)
            label.textAlignment = .center
            label.textColor = .label
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            layer.masksToBounds = true
            layer.cornerRadius = self.radius
        }
    }
    
    class ControlItem {
        var point: CGPoint
        var view: ControlPointView
        
        init(point: CGPoint, view: ControlPointView) {
            self.point = point
            self.view = view
        }
    }
}
