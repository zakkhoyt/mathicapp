//
//  CircleWaveView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class CircleWaveView: PhasedCircleView {
    open var countX: CGFloat = 20
    open var countY: CGFloat = 20
    
    open override var unitCenter: CGPoint {
        return bounds.center
    }

    override open var frequency: TimeInterval {
        return 0.5
    }
    
    open override func draw(_ rect: CGRect) {
        UIColor.lightGray.setStroke()
        let width = bounds.width / countX
        let height = bounds.height / countY

        let radius = width / 2.0
        let dotRadius = smallDotRadius
        let anglePerStep = 2.0 * CGFloat.pi / CGFloat(countX)
        for y in 0..<Int(countY) {
            for x in 0..<Int(countX) {
                let point = CGPoint(x: CGFloat(x) * width + radius,
                                    y: CGFloat(y) * height + radius)
//                drawCircle(center: point,
//                           radius: radius,
//                           strokeColor: UIColor.lightGray,
//                           fillColor: nil,
//                           lineWidth: 0.5)
                
                func drawDot() {
                    let angle = (CGFloat(x) + CGFloat(y)) * anglePerStep + phase
                    let x = point.x + radius * cos(angle)
                    let y = point.y + radius * sin(angle)
                    let dotCenter = CGPoint(x: x, y: y)
                    let hue = angle / (CGFloat.pi * 2.0)
                    let color = UIColor.hueColor(hue: hue)
                    drawCircle(center: dotCenter,
                               radius: dotRadius,
                               strokeColor: color,
                               fillColor: color,
                               lineWidth: 1.0)
                }
                drawDot()
            }
        }
    }
}

extension CircleWaveView: ParameterPanable {
    public var nameX: String? { return "# of Dots Wide" }
    public var nameY: String? { return "# of Dots High" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 40 }
    public var minimumY: CGFloat { return 1 }
    public var maximumY: CGFloat { return 40 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return countX }
        set { countX = newValue }
    }
    
    public var valueY: CGFloat {
        get { return countY }
        set { countY = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension CircleWaveView: TitledAnimation {
    public static var animationTitle: String { "Circle Wave" }
}
