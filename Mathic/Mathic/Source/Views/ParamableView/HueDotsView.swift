//
//  HueDotsView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  http://i.imgur.com/N5dI81e.gif

import UIKit

open class HueDotsView: PhasedCircleView {
    public var numberOfDots: CGFloat = 16
    public var dotRadius: CGFloat = 6
    
    open override var unitCenter: CGPoint {
        return bounds.center
    }
    
    open override var frequency: TimeInterval {
        return 0.7
    }
    
    private var axis: CGFloat {
        return min(bounds.width, bounds.height)
    }
    
    private var minimumRadius: CGFloat {
        return axis - dotRadius
    }
    
    private var maximumRadius: CGFloat {
        return axis - dotRadius
    }

    open override func draw(_ rect: CGRect) {
        let anglePerDot: CGFloat = 2.0 * CGFloat.pi / numberOfDots
        let halfRadius = radius / 2.0
        let travel = radius * 0.75
        for i in 0..<Int(numberOfDots) {
            let subPhase: CGFloat = 3.0 * 2.0 * CGFloat.pi * CGFloat(i) / numberOfDots
            let percent: CGFloat = 0.5 * sin(phase + subPhase)
            let angle: CGFloat = anglePerDot * CGFloat(i)
            let x = bounds.center.x + (halfRadius + percent * travel) * cos(angle)
            let y = bounds.center.y + (halfRadius + percent * travel) * sin(angle)
            let point = CGPoint(x: x, y: y)
            let hue = angle / (CGFloat.pi * 2.0)
            let color = UIColor.hueColor(hue: hue)
            drawCircle(center: point,
                       radius: dotRadius,
                       strokeColor: color,
                       fillColor: color,
                       lineWidth: 1.0)
        }
    }
}

extension HueDotsView: ParameterPanable {
    public var nameX: String? { return "# of Dots" }
    public var nameY: String? { return "Dot Radius" }
    public var nameZ: String? { return nil }
    
    public var minimumX: CGFloat { return 1 }
    public var maximumX: CGFloat { return 32 }
    public var minimumY: CGFloat { return 0.5 }
    public var maximumY: CGFloat { return 64 }
    public var minimumZ: CGFloat { return 0.5 }
    public var maximumZ: CGFloat { return 8 }

    public var valueX: CGFloat {
        get { return numberOfDots }
        set { numberOfDots = newValue.rounded() }
    }
    
    public var valueY: CGFloat {
        get { return dotRadius }
        set { dotRadius = newValue }
    }
    
    public var valueZ: CGFloat {
        get { return 0 }
        set { _ = newValue }
    }
}

extension HueDotsView: TitledAnimation {
    public static var animationTitle: String { "Hue Dots" }
}
