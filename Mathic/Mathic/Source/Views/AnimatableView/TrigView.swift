//
//  TrigView.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//
//

import UIKit

open class TrigView: PhasedCircleView {
    public enum TrigFunction: Int, CaseIterable {
        case sin
        case cos
        case tan
        case sec
        case csc
        case cot
        case phase
        case rightAngles

        public static let all: Set<TrigFunction> = [.sin, .cos, .tan, .sec, .csc, .cot, .phase]
        
        public var title: String {
            switch self {
            case .sin: return "sin"
            case .cos: return "cos"
            case .tan: return "tan"
            case .sec: return "sec"
            case .csc: return "csc"
            case .cot: return "cot"
            case .phase: return "⦝"
            case .rightAngles: return "⦜"
            }
        }
        
        public var strokeColor: UIColor {
            switch self {
            case .sin: return .systemBlue
            case .cos: return .systemOrange
            case .tan: return .systemGreen
            case .sec: return .systemPink
            case .csc: return .systemRed
            case .cot: return .systemPurple
            case .phase: return .label
            case .rightAngles: return .label
            }
        }
    }
    
    public var trigFunctions: Set<TrigFunction> = TrigFunction.all
    
    private var yArchive: [CGFloat] = []
    private var rightAngleWidth: CGFloat = 8
    
    open override func draw(_ rect: CGRect) {
        func drawTrigFunctionTitle(trigFunction: TrigFunction,
                                   between point1: CGPoint,
                                   and point2: CGPoint) {
            drawString(text: trigFunction.title,
                       textColor: trigFunction.strokeColor,
                       between: point1,
                       and: point2)
        }
        
        func drawUnitCircle() {
            let x = unitRadius * cos(phase)
            let y = unitRadius * sin(phase)
            let tangentPoint = CGPoint(x: unitCenter.x + x, y: unitCenter.y - y)
            UIColor.yellow.setStroke()
            drawCircle(center: unitCenter, radius: unitRadius, strokeColor: .label, fillColor: nil)
            drawLine(point1: unitCenter, point2: tangentPoint, strokeColor: .yellow)
            
            if trigFunctions.contains(.sin) {
                let point2 = CGPoint(x: tangentPoint.x, y: unitCenter.y)
                drawLine(point1: tangentPoint, point2: point2, strokeColor: TrigFunction.sin.strokeColor)
                drawTrigFunctionTitle(trigFunction: .sin, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.cos) {
                let point2 = CGPoint(x: unitCenter.x, y: tangentPoint.y)
                drawLine(point1: tangentPoint, point2: point2, strokeColor: TrigFunction.cos.strokeColor)
                drawTrigFunctionTitle(trigFunction: .cos, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.tan) {
                let theta = atan(y / x)
                let xx = y * tan(theta)
                let point2 = CGPoint(x: tangentPoint.x + xx,
                                     y: unitCenter.y)
                drawLine(point1: tangentPoint, point2: point2, strokeColor: TrigFunction.tan.strokeColor)
                drawTrigFunctionTitle(trigFunction: .tan, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.sec) {
                let xx = unitRadius * 1 / cos(phase)
                let point2 = CGPoint(x: unitCenter.x + xx, y: unitCenter.y)
                drawLine(point1: unitCenter, point2: point2, strokeColor: TrigFunction.sec.strokeColor)
                drawTrigFunctionTitle(trigFunction: .sec, between: unitCenter, and: point2)
            }
            
            if trigFunctions.contains(.csc) {
                let yy = unitRadius * -1 / sin(phase)
                let point2 = CGPoint(x: unitCenter.x, y: unitCenter.y + yy)
                drawLine(point1: unitCenter, point2: point2, strokeColor: TrigFunction.csc.strokeColor)
                drawTrigFunctionTitle(trigFunction: .csc, between: unitCenter, and: point2)
            }
            
            if trigFunctions.contains(.cot) {
                let theta = atan(x / y)
                let yy = x * tan(theta)
                let point2 = CGPoint(x: unitCenter.x,
                                     y: tangentPoint.y - yy)
                drawLine(point1: tangentPoint, point2: point2, strokeColor: TrigFunction.cot.strokeColor)
                drawTrigFunctionTitle(trigFunction: .cot, between: tangentPoint, and: point2)
            }
            
            func drawRightAngles() {
                func drawSinRightAngle() {
                    let rightY: CGFloat = y > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightX: CGFloat = x > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightPoint = CGPoint(x: unitCenter.x + rightX, y: tangentPoint.y + rightY)
                    let verticalPoint = CGPoint(x: rightPoint.x, y: rightPoint.y - rightY)
                    let horizontalPoint = CGPoint(x: rightPoint.x - rightX, y: rightPoint.y)
                    drawLine(point1: rightPoint,
                             point2: verticalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                    drawLine(point1: rightPoint,
                             point2: horizontalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                }
                
                func drawCosRightAngle() {
                    let rightY: CGFloat = y > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightX: CGFloat = x > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightPoint = CGPoint(x: tangentPoint.x - rightX, y: unitCenter.y - rightY)
                    let verticalPoint = CGPoint(x: rightPoint.x, y: rightPoint.y + rightY)
                    let horizontalPoint = CGPoint(x: rightPoint.x + rightX, y: rightPoint.y)
                    drawLine(point1: rightPoint,
                             point2: verticalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                    drawLine(point1: rightPoint,
                             point2: horizontalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                }
                
                func drawOriginRightAngle() {
                    let rightY: CGFloat = y > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightX: CGFloat = x > 0 ? rightAngleWidth : -rightAngleWidth
                    let rightPoint = CGPoint(x: unitCenter.x + rightX, y: unitCenter.y - rightY)
                    let verticalPoint = CGPoint(x: rightPoint.x, y: rightPoint.y + rightY)
                    let horizontalPoint = CGPoint(x: rightPoint.x - rightX, y: rightPoint.y)
                    drawLine(point1: rightPoint,
                             point2: verticalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                    drawLine(point1: rightPoint,
                             point2: horizontalPoint,
                             strokeColor: TrigView.TrigFunction.rightAngles.strokeColor,
                             fillColor: nil,
                             lineWidth: 1)
                }
                drawSinRightAngle()
                drawCosRightAngle()
                drawOriginRightAngle()
            }
            if trigFunctions.contains(.rightAngles) {
                drawRightAngles()
            }
        }
        drawUnitCircle()

        func drawGraph() {
            let graphRect = CGRect(x: bounds.width / 2,
                                   y: (bounds.height - 2 * unitRadius) / 2.0,
                                   width: bounds.width / 2.0 - 44,
                                   height: unitRadius * 2.0)
            
            UIColor.label.withAlphaComponent(0.1).setFill()
            let graphPath = UIBezierPath(rect: graphRect)
            graphPath.fill()
            
            let quarterWidth = graphRect.width / 4.0
            
            func drawYAxis(xOffset: CGFloat, lineWidth: CGFloat) {
                UIColor.secondaryLabel.setStroke()
                let path = UIBezierPath()
                path.lineWidth = lineWidth
                path.move(to: CGPoint(x: graphRect.origin.x + xOffset,
                                      y: graphRect.origin.y))
                path.addLine(to: CGPoint(x: graphRect.origin.x + xOffset,
                                         y: graphRect.origin.y + graphRect.height))
                path.stroke()
            }
            drawYAxis(xOffset: 0 * quarterWidth, lineWidth: 1)
            drawYAxis(xOffset: 1 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 2 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 3 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 4 * quarterWidth, lineWidth: 0.5)
            
            func drawXAxis() {
                UIColor.secondaryLabel.setStroke()
                let path = UIBezierPath()
                path.lineWidth = 0.5
                path.move(to: CGPoint(x: graphRect.origin.x,
                                      y: graphRect.origin.y + graphRect.height / 2.0))
                path.addLine(to: CGPoint(x: graphRect.origin.x + graphRect.width,
                                         y: graphRect.origin.y + graphRect.height / 2.0))
                path.stroke()
            }
            drawXAxis()
            
            func drawGraphLabel(label: String, xOffset: CGFloat) {
                let attributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont.preferredFont(forTextStyle: .footnote),
                    .foregroundColor: UIColor.label
                ]
                let text = label as NSString
                let size = text.size(withAttributes: attributes)
                let point = CGPoint(x: graphRect.origin.x + xOffset,
                                    y: graphRect.origin.y + graphRect.height + 8)
                let frame = CGRect(x: point.x - size.width / 2.0,
                                   y: point.y - size.height / 2.0,
                                   width: size.width,
                                   height: size.height)
                text.draw(with: frame,
                          options: .usesLineFragmentOrigin,
                          attributes: attributes,
                          context: nil)
            }
            
            drawGraphLabel(label: "0", xOffset: 0 * quarterWidth)
            drawGraphLabel(label: "π/2", xOffset: 1 * quarterWidth)
            drawGraphLabel(label: "π", xOffset: 2 * quarterWidth)
            drawGraphLabel(label: "3π/2", xOffset: 3 * quarterWidth)
            drawGraphLabel(label: "2π", xOffset: 4 * quarterWidth)
            
            func plotTrigFunctions() {
                func plot(trigFunction: TrigFunction, calculate: ((_ phase: CGFloat) -> CGFloat)) {
                    trigFunction.strokeColor.setStroke()
                    
                    let path = UIBezierPath()
                    path.lineWidth = 0.5
                    for x in 0..<Int(graphRect.width) {
                        let phase = 2.0 * CGFloat.pi * CGFloat(x) / graphRect.width
                        var y = graphRect.height / 2.0 * calculate(phase)
                        // Ensure that y is no inf, -inf, NaN, etc....
                        if y.isInfinite || y.isNaN {
                            if y < 0 {
                                y = -CGFloat.greatestFiniteMagnitude
                            } else {
                                y = CGFloat.greatestFiniteMagnitude
                            }
                        }
                        let point = CGPoint(x: graphRect.origin.x + CGFloat(x),
                                            y: graphRect.origin.y + graphRect.height / 2.0 - y)
                        if x == 0 {
                            path.move(to: point)
                        } else {
                            path.addLine(to: point)
                        }
                    }
                    path.stroke()
                    
                    // draw dot
                    trigFunction.strokeColor.setFill()
                    let percent = phase / (2.0 * CGFloat.pi)
                    let x = percent * graphRect.width
                    let y = (graphRect.height / 2.0 * calculate(phase))
                    let point = CGPoint(x: graphRect.origin.x + x,
                                        y: graphRect.origin.y + graphRect.height / 2.0 - y)
                    drawCircle(center: point,
                               radius: 4,
                               strokeColor: nil,
                               fillColor: trigFunction.strokeColor,
                               lineWidth: 0)
                    
                    drawString(trigFunction.title,
                               textColor: trigFunction.strokeColor,
                               at: CGPoint(x: point.x, y: point.y + 12))
                }
                
                if trigFunctions.contains(.sin) {
                    plot(trigFunction: .sin) { return sin($0) }
                }
                if trigFunctions.contains(.cos) {
                    plot(trigFunction: .cos) { return cos($0) }
                }
                if trigFunctions.contains(.tan) {
                    plot(trigFunction: .tan) { return tan($0) }
                }
                if trigFunctions.contains(.sec) {
                    plot(trigFunction: .sec) { return 1 / cos($0) }
                }
                if trigFunctions.contains(.csc) {
                    plot(trigFunction: .csc) { return 1 / sin($0) }
                }
                if trigFunctions.contains(.cot) {
                    plot(trigFunction: .cot) { return 1 / tan($0) }
                }
            }
            plotTrigFunctions()
        }
        drawGraph()
        if trigFunctions.contains(.phase) {
            super.drawPhase(strokeColor: TrigView.TrigFunction.rightAngles.strokeColor, lineWidth: 4)
        }
        super.drawTangentPoint()
    }
}

extension TrigView: TitledAnimation {
    public static var animationTitle: String { "Trigonometry" }
}
