//
//  AboutView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class AboutView: PhasedCircleView {
    open override var frequency: TimeInterval {
        return 0.2
    }
    
    open override func draw(_ rect: CGRect) {
        func makeRainbowString(string: String, attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
            let attributedString = NSMutableAttributedString(string: string, attributes: attributes)
            let font = attributes[.font] as? UIFont ?? UIFont.preferredFont(forTextStyle: .title3)
            for i in 0..<string.count {
                let hue = (CGFloat(i) / CGFloat(string.count - 1) + cycle).truncatingRemainder(dividingBy: 1.0)
                let color = UIColor.hueColor(hue: hue)
                
                let attr: [NSAttributedString.Key: Any] = [
                    .font: font,
                    .foregroundColor: color
                ]
                let range = NSRange(location: i, length: 1)
                attributedString.addAttributes(attr, range: range)
            }
            return NSAttributedString(attributedString: attributedString)
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center

        let attributedString = NSMutableAttributedString()
        let normalAttributes: [NSAttributedString.Key: Any] =  [
            .foregroundColor: UIColor.label,
            .font: UIFont.preferredFont(forTextStyle: .body),
            .paragraphStyle: paragraphStyle
        ]
        attributedString.append(NSAttributedString(string: "A\nCore Graphics\nShowcase\n",
                                                   attributes: normalAttributes))
        let rainbowAttributes: [NSAttributedString.Key: Any] =  [
            .font: UIFont.preferredFont(forTextStyle: .title3)
        ]
        attributedString.append(makeRainbowString(string: "Source code on gitlab.",
                                                  attributes: rainbowAttributes))
        drawAttributedString(attributedString, at: bounds.center)
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let url = URL(string: "https://gitlab.com/zakkhoyt/mathicapp") else { return }
        UIApplication.shared.open(url)
    }
}

extension AboutView: TitledAnimation {
    public static var animationTitle: String { "About" }
}
