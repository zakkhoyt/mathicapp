//
//  CaliforniaView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 1/21/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class CaliforniaView: MathView {
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawFlag()
    }

    private var flagRect: CGRect {
        if bounds.width > bounds.height {
            let width = bounds.height * 1.5
            return CGRect(x: (bounds.width - width) / 2.0,
                          y: 0,
                          width: width,
                          height: bounds.height)
        } else {
            let height = bounds.width / 1.5
            return CGRect(x: 0,
                          y: (bounds.height - height) / 2.0,
                          width: bounds.width,
                          height: height)
        }
    }
    
    private var whiteRect: CGRect {
        let height = (flagRect.height * 5.0 / 6.0)
        return CGRect(x: flagRect.origin.x,
                      y: flagRect.origin.y,
                      width: flagRect.width,
                      height: height)
    }

    private var upperWhiteRect: CGRect {
        let height = whiteRect.height / 2.0
        return CGRect(x: whiteRect.origin.x + 0,
                      y: whiteRect.origin.y,
                      width: whiteRect.width,
                      height: height)
    }
    
    private var lowerWhiteRect: CGRect {
        let height = whiteRect.height / 2.0
        return CGRect(x: whiteRect.origin.x + 0,
                      y: whiteRect.origin.y + height,
                      width: whiteRect.width,
                      height: height)
    }
    
    private var redRect: CGRect {
        let height = flagRect.height * 1.0 / 6.0
        return CGRect(x: flagRect.origin.x,
                      y: flagRect.origin.y + flagRect.height - height,
                      width: flagRect.width,
                      height: height)
    }
    
    private var bearRect: CGRect {
        let width = flagRect.width * 4.0 / 9.0
        let height = flagRect.height * 1.0 / 3.0
        return CGRect(x: whiteRect.origin.x + (whiteRect.width - width) / 2.0,
                      y: whiteRect.origin.y + (whiteRect.height - height) / 2.0,
                      width: width,
                      height: height)
    }
    
    private var grassRect: CGRect {
        let height = flagRect.height * 11.0 / 120.0
        let width = height * 11
        return CGRect(x: lowerWhiteRect.origin.x + (lowerWhiteRect.width - width) / 2.0,
                      y: bearRect.origin.y + bearRect.height - height / 2.0,
                      width: width,
                      height: height)
    }
    
    private var starRect: CGRect {
        let width = flagRect.width * 1.0 / 10.0
        let height = width
        let centerX: CGFloat = flagRect.origin.x + flagRect.width * 1.0 / 6.0
        let centerY: CGFloat = flagRect.origin.y + flagRect.height * 1.0 / 5.0
        return CGRect(x: centerX - width / 2.0,
                      y: centerY - height / 2.0,
                      width: width,
                      height: height)
    }

    // https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Flag_of_California_Metrics.svg/
    // 2560px-Flag_of_California_Metrics.svg.png
    // * The said bear flag shall consist of a flag of a length equal to one and one-half the width
    // * The upper five-sixths of the width thereof to be a white field
    // * The lower sixth of the width thereof to be a red stripe
    // In the center of the white field a California grizzly bear upon a grass plat,
    // In the position of walking toward the left of the said field;
    // Said bear shall be dark brown in color and in length, equal to one-third of the length of said flag.
    // The size of the bear is 2/3 the size of the hoist width and has a ratio of 2 by 1.
    // The grass plot has a ratio of 11 to 1
    // There shall appear in the white field in the upper left-hand corner a single red star
    // At the bottom of the white field the words 'California Republic,'
    open func drawFlag() {
        func drawWhiteBackground() {
            UIColor.darkGray.setStroke()
            UIColor.white.setFill()
            let path = UIBezierPath(rect: flagRect)
            path.fill()
            path.stroke()
        }
        drawWhiteBackground()
        
        func drawRedStripe() {
            UIColor.oldGloryRed.setFill()
            let path = UIBezierPath(rect: redRect)
            path.fill()
        }
        drawRedStripe()
        
        func drawText() {
            let width = flagRect.width * 2.0 / 3.0
            let height = flagRect.height * 1.0 / 12.0
            
            let outlineRect = CGRect(x: flagRect.origin.x + (flagRect.width - width) / 2.0,
                                     y: flagRect.origin.y + flagRect.height * 4.0 / 6.0,
                                     width: width,
                                     height: height)
            
//            func drawOutline() {
//                UIColor.lightGray.setStroke()
//                let path = UIBezierPath(rect: outlineRect)
//                path.stroke()
//            }
//            drawOutline()
            
            // TODO: Calculate the font size by iterating to find max
            // font which fits in outlineRect
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: height, weight: .medium),
                .foregroundColor: UIColor.seal
            ]
            
            let text = "CALIFORNIA REPUBLIC"
            let textSize = text.size(withAttributes: attributes)
            let textRect = CGRect(x: outlineRect.origin.x + (outlineRect.width - textSize.width) / 2.0,
                                  y: outlineRect.origin.y + (outlineRect.height - textSize.height) / 2.0,
                                  width: textSize.width,
                                  height: textSize.height)
            text.draw(with: textRect,
                      options: .usesLineFragmentOrigin,
                      attributes: attributes,
                      context: nil)
        }
        drawText()
        
        func drawGrass() {
//            func drawOutline() {
//                UIColor.irishGreen.setFill()
//                UIColor.lightGray.setStroke()
//                let path = UIBezierPath(rect: grassRect)
//               //path.fill()
//                path.stroke()
//            }
//            drawOutline()

            guard let context = UIGraphicsGetCurrentContext() else { return }
            context.clip(to: grassRect)

            UIColor.irishGreen.withAlphaComponent(0.45).setFill()
            UIColor.lightGray.setStroke()
            
            let width = flagRect.width * 2
            let height = width
            let rect = CGRect(x: grassRect.origin.x + (grassRect.width - width) / 2.0,
                              y: grassRect.origin.y,
                              width: width,
                              height: height)
            let path = UIBezierPath(ovalIn: rect)
            path.fill()
            path.stroke()
            context.resetClip()
        }
        drawGrass()

        func drawBear() {
            func drawOutline() {
                UIColor.lightGray.setStroke()
                UIColor.mapleSugar.withAlphaComponent(0.5).setFill()
                let path = UIBezierPath(rect: bearRect)
                path.fill()
                path.stroke()
            }
            drawOutline()
        }
        drawBear()
        
        func drawStar() {
//            func drawOutline() {
//                UIColor.oldGloryRed.setFill()
//                UIColor.lightGray.setStroke()
//                let path = UIBezierPath(ovalIn: starRect)
//                //path.fill()
//                path.stroke()
//            }
//            drawOutline()
            
            UIColor.oldGloryRed.setFill()
            UIColor.lightGray.setStroke()
            let path = UIBezierPath()
            let anglePerPoint = 2.0 * CGFloat.pi / 5.0
            
            func starPoint(i: Int) -> CGPoint {
                let radius = starRect.width / 2.0
                let center = starRect.center
                let angleOffset = -CGFloat.pi / 2.0
                let angle = CGFloat(i) * anglePerPoint + angleOffset
                let x: CGFloat = radius * cos(angle)
                let y: CGFloat = radius * sin(angle)
                return CGPoint(x: center.x + x,
                                    y: center.y + y)
            }
            
            let numberOfPoints = 5
            func nextIndex(index: Int) -> Int {
                let next = index - 2
                return next < 0 ? next + numberOfPoints : next
            }
            
            var index = 3
            var isFirst = true
            for _ in 0..<numberOfPoints {
                if isFirst {
                    isFirst = false
                    path.move(to: starPoint(i: index))
                }
                index = nextIndex(index: index)
                path.addLine(to: starPoint(i: index))
            }
            path.fill()
        }
        drawStar()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setNeedsDisplay()
    }
}

extension CGRect {
    var center: CGPoint {
        let x: CGFloat = origin.x + width / 2.0
        let y: CGFloat = origin.y + height / 2.0
        return CGPoint(x: x, y: y)
    }
}

public extension UIColor {
    // https://en.wikipedia.org/wiki/Flag_of_California
    static let oldGloryRed = UIColor(red: CGFloat(183) / CGFloat(255),
                                            green: CGFloat(18) / CGFloat(255),
                                            blue: CGFloat(52) / CGFloat(255),
                                            alpha: 1.0)
    static let mapleSugar = UIColor(red: CGFloat(189) / CGFloat(255),
                                           green: CGFloat(138) / CGFloat(255),
                                           blue: CGFloat(94) / CGFloat(255),
                                           alpha: 1.0)
    static let seal = UIColor(red: CGFloat(88) / CGFloat(255),
                                     green: CGFloat(69) / CGFloat(255),
                                     blue: CGFloat(40) / CGFloat(255),
                                     alpha: 1.0)
    static let irishGreen = UIColor(red: CGFloat(0) / CGFloat(255),
                                           green: CGFloat(133) / CGFloat(255),
                                           blue: CGFloat(66) / CGFloat(255),
                                           alpha: 1.0)
}

extension CaliforniaView: TitledAnimation {
    public static var animationTitle: String { "California" }
}
