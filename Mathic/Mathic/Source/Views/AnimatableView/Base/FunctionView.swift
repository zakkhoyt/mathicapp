//
//  FunctionView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/24/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class FunctionView: CartesianView {
    func drawFunction(strokeColor: UIColor = .label,
                      function f: ((CGFloat) -> CGFloat),
                      lineWidth: CGFloat = 2) {
        strokeColor.setStroke()
        let path = UIBezierPath()
        path.lineWidth = lineWidth
        
        var isFirst = true
        
        for xx in stride(from: domain.lowerBound, through: domain.upperBound, by: step) {
            let x = CGFloat(xx)
            let y = f(x)
            if y.isNaN {
                print("y: NaN")
                continue
            }
            let point = pointForPlot(x: x, y: y)
            if isFirst {
                isFirst = false
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        path.stroke()
    }
}
