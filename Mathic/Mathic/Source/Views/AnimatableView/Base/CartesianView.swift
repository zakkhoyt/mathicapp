//
//  CartesianView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/24/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

open class CartesianView: MathView {
    open var domain: ClosedRange<CGFloat> {
        return -11 ... 11
    }

    open var range: ClosedRange<CGFloat> {
        let ratio = bounds.height / bounds.width
        if ratio.isNaN { return -1 ... 1 }
        return domain.lowerBound*ratio ... domain.upperBound*ratio
    }
    
    public var span: CGFloat {
        return domain.upperBound - domain.lowerBound
    }
    
    public var step: CGFloat {
        return span / bounds.width
    }

    /// Input coordinates to plot on a cartesian coordinate
    /// returns a CGPoint to use with UIKit
    public func pointForPlot(x: CGFloat, y: CGFloat) -> CGPoint {
        func convertX(_ x: CGFloat) -> CGFloat {
            let swing = domain.upperBound - domain.lowerBound // 3 -(-1) == 4
            let percent = (x - domain.lowerBound) / swing     // 0 -(-1) = 1 / 4 = 0.25
            return bounds.origin.x + bounds.width * percent
        }
        func convertY(_ y: CGFloat) -> CGFloat {
            let swing = range.upperBound - range.lowerBound // 3 -(-1) == 4
            let percent = (y - range.lowerBound) / swing     // 0 -(-1) = 1 / 4 = 0.25
            return bounds.origin.y + bounds.height - bounds.height * percent
       }
        return CGPoint(x: convertX(x), y: convertY(y))
    }
    
    /// Draws either the full grid or the two axes with ticks (if tickSize is defined)
    public func drawCartesian(tickMod: CGFloat = 10,
                              tickRadius: CGFloat? = 8) {
        let xLower = Int(domain.lowerBound / tickMod)
        let xUpper = Int(domain.upperBound / tickMod)
        let xRange: ClosedRange<Int> = xLower ... xUpper
        for xx in stride(from: xRange.lowerBound, through: xRange.upperBound, by: 1) {
            let x = CGFloat(xx) * CGFloat(tickMod)
            let tick = pointForPlot(x: x, y: 0)
            let point1: CGPoint = {
                if xx != 0, let tickRadius = tickRadius {
                    return CGPoint(x: tick.x, y: tick.y + tickRadius)
                } else {
                    return CGPoint(x: tick.x, y: 0)
                }
            }()
            let point2: CGPoint = {
                if xx != 0, let tickRadius = tickRadius {
                    return CGPoint(x: tick.x, y: tick.y - tickRadius)
                } else {
                    return CGPoint(x: tick.x, y: bounds.height)
                }
            }()

            let lineWidth: CGFloat = xx == 0 ? 1.0 : 0.5
            drawLine(point1: point1,
                     point2: point2,
                     strokeColor: .label,
                     fillColor: nil,
                     lineWidth: lineWidth)
            
            drawString("\(Int(x))",
                textColor: .label,
                at: tick,
                offset: TextOffset(vertical: x < 0 ? .positive : .negative, horizontal: .none),
                attributes: [
                    .font: UIFont.preferredFont(forTextStyle: .caption1)
                ]
            )
        }
        
        let yLower = Int(range.lowerBound / CGFloat(tickMod))
        let yUpper = Int(range.upperBound / CGFloat(tickMod))
        let yRange: ClosedRange<Int> = yLower ... yUpper
        for yy in stride(from: yRange.lowerBound, through: yRange.upperBound, by: 1) {
            let y = CGFloat(yy) * CGFloat(tickMod)
            let tick = pointForPlot(x: 0, y: y)
            let point1: CGPoint = {
                if yy != 0, let tickRadius = tickRadius {
                    return CGPoint(x: tick.x + tickRadius, y: tick.y)
                } else {
                    return CGPoint(x: 0, y: tick.y)
                }
            }()
            let point2: CGPoint = {
                if yy != 0, let tickRadius = tickRadius {
                    return CGPoint(x: tick.x - tickRadius, y: tick.y)
                } else {
                    return CGPoint(x: bounds.width, y: tick.y)
                }
            }()
            let lineWidth: CGFloat = yy == 0 ? 1.0 : 0.5
            drawLine(point1: point1,
                     point2: point2,
                     strokeColor: .label,
                     fillColor: nil,
                     lineWidth: lineWidth)

            drawString("\(Int(y))",
                textColor: .label,
                at: tick,
                offset: TextOffset(vertical: .none, horizontal: y < 0 ? .negative : .positive),
                attributes: [
                    .font: UIFont.preferredFont(forTextStyle: .caption1)
                ]
            )
        }
    }
}
