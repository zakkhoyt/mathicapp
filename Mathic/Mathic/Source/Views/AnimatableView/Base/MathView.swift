//
//  MathView.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/23/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

public struct TextOffset {
    public enum Direction {
        case none
        case positive
        case negative
    }

    public let vertical: Direction
    public let horizontal: Direction
}

extension TextOffset {
    public static var zero: TextOffset {
        TextOffset(vertical: .none, horizontal: .none)
    }
}

open class MathView: AnimationView {
    // MARK: Open methods
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        backgroundColor = .systemBackground
    }
    
    open func drawCircle(center: CGPoint,
                         radius: CGFloat,
                         strokeColor: UIColor? = nil,
                         fillColor: UIColor? = nil,
                         lineWidth: CGFloat = 0.5) {
        setColors(strokeColor: strokeColor, fillColor: fillColor)
        let path = UIBezierPath(arcCenter: center,
                                radius: radius,
                                startAngle: 0,
                                endAngle: 2.0 * CGFloat.pi,
                                clockwise: false)
        path.lineWidth = lineWidth
        if strokeColor != nil {
            path.stroke()
        }
        if fillColor != nil {
            path.fill()
        }
    }
    
    open func drawLine(point1: CGPoint,
                       point2: CGPoint,
                       strokeColor: UIColor? = nil,
                       fillColor: UIColor? = nil,
                       lineWidth: CGFloat = 0.5) {
        setColors(strokeColor: strokeColor, fillColor: fillColor)
        let path = UIBezierPath()
        path.move(to: point1)
        path.addLine(to: point2)
        path.lineWidth = lineWidth
        path.lineCapStyle = .round
        if strokeColor != nil {
            path.stroke()
        }
        if fillColor != nil {
            path.fill()
        }
    }
    
    open func drawString(_ text: String,
                         textColor: UIColor,
                         at point: CGPoint,
                         offset: TextOffset = .zero,
                         attributes customAttributes: [NSAttributedString.Key: Any] = [:]) {
        var attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.preferredFont(forTextStyle: .footnote),
            .foregroundColor: textColor
        ]
        
        for (key, value) in customAttributes {
            attributes[key] = value
        }
        
        let string = " \(text) " as NSString
        let size = string.size(withAttributes: attributes)
        let xOffset: CGFloat = {
            switch offset.horizontal {
            case .none: return 0
            case .positive: return size.height
            case .negative: return -size.height
            }
        }()

        let yOffset: CGFloat = {
            switch offset.vertical {
            case .none: return 0
            case .positive: return -size.height
            case .negative: return size.height
            }
        }()
        let frame = CGRect(x: point.x + xOffset - size.width / 2.0,
                           y: point.y + yOffset - size.height / 2.0,
                           width: size.width,
                           height: size.height)
        string.draw(with: frame,
                    options: .usesLineFragmentOrigin,
                    attributes: attributes,
                    context: nil)
    }
    
    open func drawString(text: String,
                         textColor: UIColor,
                         between point1: CGPoint,
                         and point2: CGPoint,
                         attributes customAttributes: [NSAttributedString.Key: Any] = [:]) {
        let point = CGPoint.lineCenter(point1: point1, point2: point2)
        drawString(text, textColor: textColor, at: point)
    }
    
    open func drawCross(center: CGPoint,
                        crossRadius: CGFloat,
                        lineWidth: CGFloat,
                        strokeColor: UIColor) {
        func drawHorizontalLine() {
            let point1: CGPoint = {
                let x = center.x - crossRadius
                let y = center.y
                return CGPoint(x: x, y: y)
            }()
            let point2: CGPoint = {
                let x = center.x + crossRadius
                let y = center.y
                return CGPoint(x: x, y: y)
            }()
            drawLine(point1: point1,
                     point2: point2,
                     strokeColor: strokeColor,
                     fillColor: nil,
                     lineWidth: lineWidth)
        }
        func drawVerticalLine() {
            let point1: CGPoint = {
                let x = center.x
                let y = center.y - crossRadius
                return CGPoint(x: x, y: y)
            }()
            let point2: CGPoint = {
                let x = center.x
                let y = center.y + crossRadius
                return CGPoint(x: x, y: y)
            }()
            drawLine(point1: point1,
                     point2: point2,
                     strokeColor: strokeColor,
                     fillColor: nil,
                     lineWidth: lineWidth)
        }
        drawVerticalLine()
        drawHorizontalLine()
    }
    
    @discardableResult
    open func drawAttributedString(_ attributedString: NSAttributedString,
                                   at point: CGPoint) -> CGRect {
        let size = attributedString.size()
        let rect = CGRect(x: point.x - size.width / 2.0,
                          y: point.y - size.height / 2.0,
                          width: size.width,
                          height: size.height)
        attributedString.draw(in: rect)
        return rect
    }
    
    // MARK: Private methods
    
    private func setColors(strokeColor: UIColor?, fillColor: UIColor?) {
        if let strokeColor = strokeColor {
            strokeColor.setStroke()
        }
        if let fillColor = fillColor {
            fillColor.setFill()
        }
    }
}

public extension CGPoint {
    static func lineCenter(point1: CGPoint,
                           point2: CGPoint) -> CGPoint {
        let x = point1.x + (point2.x - point1.x) / 2.0
        let y = point1.y + (point2.y - point1.y) / 2.0
        return CGPoint(x: x, y: y)
    }
    
    static func lineLength(point1: CGPoint,
                           point2: CGPoint) -> CGFloat {
        let x = abs(point2.x - point1.x)
        let y = abs(point2.y - point1.y)
        let xSquared = pow(x, 2)
        let ySquared = pow(y, 2)
        return sqrt(xSquared + ySquared)
    }
    
    static func slope(point1: CGPoint,
                      point2: CGPoint) -> CGFloat {
        let rise = -(point2.y - point1.y)
        let run = point2.x - point1.x
        return rise / run
    }
}
