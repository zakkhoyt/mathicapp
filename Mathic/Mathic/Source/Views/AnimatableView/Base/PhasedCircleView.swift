//
//  CircleView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 12/30/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

open class PhasedCircleView: MathView {
    // Center of our circle
    open var unitCenter: CGPoint {
        return CGPoint(x: bounds.width / 4.0, y: bounds.height / 2.0)
    }
    
    // Radius of our circle
    open var unitRadius: CGFloat {
        return bounds.width / 8.0
    }

    // Only set during touch/drag events
    open var touchPhase: CGFloat?
    
    // Phase in terms of frequency iVar
    open var timePhase: CGFloat {
        return 2.0 * CGFloat.pi * cycle
    }
    
    // If touch is happening, use touchPhase, otherwise timePhase.
    open var phase: CGFloat {
        return touchPhase ?? timePhase
    }
    
    open func drawTangentPoint(radius: CGFloat = 4,
                               strokeColor: UIColor = .cyan,
                               fillColor: UIColor = UIColor.cyan.withAlphaComponent(0.25),
                               lineWidth: CGFloat = 0.5) {
        let x = unitRadius * cos(phase)
        let y = unitRadius * sin(phase)
        let tangentCenter = CGPoint(x: unitCenter.x + x,
                                    y: unitCenter.y - y)
        
        drawCircle(center: tangentCenter,
                   radius: radius,
                   strokeColor: strokeColor,
                   fillColor: fillColor,
                   lineWidth: lineWidth)
    }
    
    open func drawPhase(strokeColor: UIColor = .cyan,
                        lineWidth: CGFloat = 4) {
        strokeColor.setStroke()
        // iOS always treats angles as clockwise. So we need to multiply
        // phase by -1
        let path = UIBezierPath(arcCenter: unitCenter,
                                radius: unitRadius / 4.0,
                                startAngle: 0,
                                endAngle: -phase,
                                clockwise: false)
        path.lineWidth = lineWidth
        path.stroke()
        
        drawPhaseText()
    }
    
    open func drawPhaseText() {
        let text = String(format: "%.03f", phase)
        super.drawString(text,
                         textColor: .label,
                         at: unitCenter,
                         attributes: [
                            .font: UIFont.preferredFont(forTextStyle: .footnote),
                            .backgroundColor: UIColor.clear])
    }
    
    open func drawTouchExample() {
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTracking(touch, with: event)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTracking(touch, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTracking(touch, with: event)
        resumeAnimationFromTouchEnd()
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        resumeAnimationFromTouchEnd()
    }
    
    private func resumeAnimationFromTouchEnd() {
        if let touchPhase = touchPhase {
            // Calculate offset for current touch angle in terms of time (based on frequency var)
            let offsetForTouchPoint = TimeInterval((1 / CGFloat(frequency)) * (touchPhase / (2.0 * CGFloat.pi)))
            startDate = Date().addingTimeInterval(-offsetForTouchPoint)
        }
        touchPhase = nil
    }
    
    private func angleBetween(centerPoint: CGPoint, startPoint: CGPoint, endPoint: CGPoint) -> CGFloat {
        return atan2(endPoint.y - centerPoint.y, endPoint.x - centerPoint.x) -
            atan2(startPoint.y - centerPoint.y, startPoint.x - centerPoint.x)
    }
    
    private func processTracking(_ touch: UITouch, with event: UIEvent?) {
        let point = touch.location(in: self)
        let startPoint = CGPoint(x: unitCenter.x + unitRadius, y: unitCenter.y)
        var angle = angleBetween(centerPoint: unitCenter, startPoint: point, endPoint: startPoint)
        if angle < 0 { angle += 2.0 * CGFloat.pi }
        if ProcessInfo.processInfo.arguments.contains("VERBOSE") {
            print("touchPhase: \(angle)")
        }
        touchPhase = angle
    }
}
