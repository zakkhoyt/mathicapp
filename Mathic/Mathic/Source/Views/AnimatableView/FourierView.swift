//
//  FourierView.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//
//  Inspired by: https://www.youtube.com/watch?v=WSkczQe6YxA

import UIKit

open class FourierView: PhasedCircleView {
    public enum Waveform: Int, CaseIterable {
        case square
        case triangle
        case sawtooth
//        case semicircle
        
        public var title: String {
            switch self {
            case .square: return "Square"
            case .triangle: return "Triangle"
            case .sawtooth: return "Sawtooth"
//            case .semicircle: return "Semicircle"
            }
        }
    }
    
    public var waveform: Waveform = .square
    public var accuracy: CGFloat = 16
    private var yArchive: [CGFloat] = []
    
    open override func draw(_ rect: CGRect) {
        var tangentPoint: CGPoint = .zero
        for i in 0..<Int(accuracy) {
            let n = CGFloat(i)
            
            func calculate() -> (c: CGFloat, x: CGFloat, y: CGFloat) {
                let c: CGFloat
                let x: CGFloat
                let y: CGFloat
                switch waveform {
                case .square:
                    c = 1.0 / (2.0 * n + 1)
                    x = c * unitRadius * cos((2 * n + 1) * phase)
                    y = c * unitRadius * sin((2 * n + 1) * phase)

                case .triangle:
                    let cTop: CGFloat = pow(-1, n)
                    let cBottom: CGFloat = pow(2.0 * n + 1.0, 2)
                    c = cTop / cBottom
                    x = c * unitRadius * cos((2.0 * n + 1) * phase)
                    y = c * unitRadius * sin((2.0 * n + 1) * phase)

                case .sawtooth:
                    c = i % 2 == 0 ? (1.0 / (n + 1)) : (-1.0 / (n + 1))
                    x = c * unitRadius * cos((n + 1) * phase)
                    y = c * unitRadius * sin((n + 1) * phase)

//                case .semicircle:
//                    // http://mathworld.wolfram.com/FourierSeriesSemicircle.html
//                    func j(_ input: CGFloat) -> CGFloat {
//                        // http://mathworld.wolfram.com/FourierSeriesSemicircle.html
//                        // We need to implement a bessel function
//                        return input
//                    }
//                    c = pow(-1, (n + 1)) * j(CGFloat(n + 1)) / (CGFloat(n + 1))
//                    x = c * unitRadius * cos((2 * n + 1) * phase)
//                    y = c * unitRadius * sin((2 * n + 1) * phase)
                }
                return (c, x, y)
            }
            let cxy = calculate()
            if i == 0 {
                tangentPoint = CGPoint(x: unitCenter.x + cxy.x, y: unitCenter.y - cxy.y)
                drawCircle(center: unitCenter, radius: unitRadius, strokeColor: .secondaryLabel)
                drawLine(point1: unitCenter, point2: tangentPoint, strokeColor: .green)
            } else {
                let previousTangentPoint = tangentPoint
                tangentPoint = CGPoint(x: tangentPoint.x + cxy.x, y: tangentPoint.y - cxy.y)
                drawCircle(center: previousTangentPoint, radius: unitRadius * cxy.c, strokeColor: .secondaryLabel)
                drawLine(point1: previousTangentPoint, point2: tangentPoint, strokeColor: .green)
            }
        }
        
        UIColor.cyan.setStroke()
        let bridgePath = UIBezierPath()
        bridgePath.lineWidth = 0.5
        bridgePath.move(to: tangentPoint)
        let drawPoint = CGPoint(x: unitCenter.x + 2 * unitRadius,
                                y: tangentPoint.y)
        bridgePath.addLine(to: drawPoint)
        bridgePath.stroke()
        
        yArchive.insert(tangentPoint.y, at: 0)
        if yArchive.count > Int(bounds.width / UIScreen.main.scale) {
            yArchive.removeLast()
        }
        
        func drawYValue() {
            let yText: CGFloat = (unitCenter.y - tangentPoint.y) / unitRadius
            let yOffset: CGFloat = yText > 0 ? -8 : 8
            let point = CGPoint(x: drawPoint.x, y: drawPoint.y + yOffset)
            let yValue = String(format: "%0.2f", yText)
            drawString(yValue,
                       textColor: .label,
                       at: point,
                       attributes: [.backgroundColor: UIColor.clear])
        }
        drawYValue()
        
        UIColor.label.setStroke()
        let historyPath = UIBezierPath()
        historyPath.lineWidth = 4
        historyPath.lineCapStyle = .round
        for (x, y) in yArchive.enumerated() {
            let point = CGPoint(x: drawPoint.x + CGFloat(x), y: CGFloat(y))
            if x == 0 {
                historyPath.move(to: point)
            } else {
                historyPath.addLine(to: point)
            }
        }
        historyPath.stroke()
        super.drawPhase()
        super.drawTangentPoint()
    }
}

extension FourierView: TitledAnimation {
    public static var animationTitle: String { "Fourier Transforms" }
}
