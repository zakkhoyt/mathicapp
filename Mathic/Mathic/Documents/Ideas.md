
// Radians: https://upload.wikimedia.org/wikipedia/commons/4/4e/Circle_radians.gif
// Brachist: https://i.imgur.com/cxpk0GD.gif
// Sin/Cos: https://i.imgur.com/uPpkWr9.gif
// Sin/Cos: https://i.imgur.com/5fpYND4.gif
// Tan: https://i.imgur.com/9JJEgo8.gif
// Polar: https://i.imgur.com/UBbzD8Z.gif
// Trig: https://i.imgur.com/2AvSwEA.mp4
// Group: https://www.reddit.com/r/educationalgifs/comments/2c7gl3/trig_functions_visualized_xpost_from_mathgifs/?st=jpzy34zf&sh=3728597f
// Pythag: https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.mathwarehouse.com%2Fanimated-gifs%2Fimages%2Fpythagorean-theorem-sum-of-squares-demonstration-gif.gif&imgrefurl=https%3A%2F%2Fwww.mathwarehouse.com%2Fanimated-gifs%2F&docid=gdQL9zqMzpl_NM&tbnid=11lkFYKpAwoz7M%3A&vet=10ahUKEwiOrqzJybbfAhVuHTQIHcA-DEkQMwhrKAEwAQ..i&w=360&h=420&client=firefox-b-1-ab&bih=678&biw=1034&q=math%20gifs&ved=0ahUKEwiOrqzJybbfAhVuHTQIHcA-DEkQMwhrKAEwAQ&iact=mrc&uact=8
// http://privatekgsmaths.weebly.com/uploads/1/4/9/3/14935550/9495829_orig.gif

// Ellipse focus: https://commons.wikimedia.org/wiki/File:Ellipse_Animation_Small.gif
// https://www.mathwarehouse.com/ellipse/equation-of-ellipse.php
// https://www.mathwarehouse.com/ellipse/focus-of-ellipse.php


// Cardiogram
https://i.imgur.com/avE8cxJ.mp4


// Disappearing dots
// https://img.buzzfeed.com/buzzfeed-static/static/2013-10/enhanced/webdr01/18/6/anigif_enhanced-buzz-17988-1382091417-10.gif
// https://whyevolutionistrue.files.wordpress.com/2014/09/anigif_enhanced-16656-1408614979-1.gif
// http://i.imgur.com/N5dI81e.gif

// https://img.buzzfeed.com/buzzfeed-static/static/2013-10/enhanced/webdr06/31/5/anigif_enhanced-buzz-11884-1383211799-15.gif
// https://i.imgur.com/XbnsCbm.gif

// Propagating waves
// https://d2w9rnfcy7mm78.cloudfront.net/981092/original_18259ca0911dc2ffe6499c0494918e47.gif
// http://i.imgur.com/hFix63q.gif

// https://img.buzzfeed.com/buzzfeed-static/static/2014-06/19/12/enhanced/webdr03/anigif_enhanced-7160-1403196003-12.gif

// Hue Waves
// https://i.gifer.com/OveW.gif


// Ripple
// https://i.gifer.com/9w80.gif

// "RotatingPlusView"
// https://giphy.com/gifs/space-illusion-VakJhPMYg9qN2?utm_source=media-link&utm_medium=landing&utm_campaign=Media%20Links&utm_term=https://www.google.com/imgres?imgurl=https%3A%2F%2Fmedia.tenor.com%2Fimages%2Fa4d0ab9f26e04cfff7c214498e942e81%2Ftenor.gif&imgrefurl=https%3A%2F%2Ftenor.com%2Fsearch%2Foptical-illusions-gifs&docid=isiPjJmp6dF0PM&tbnid=NxzPKajiNWVksM%3A&vet=10ahUKEwiXo9vkj4PgAhXPIDQIHReMCgAQMwhqKAAwAA..i&w=220&h=220&client=firefox-b-1-ab&bih=836&biw=1224&q=optical%20illusion%20gif&ved=0ahUKEwiXo9vkj4PgAhXPIDQIHReMCgAQMwhqKAAwAA&iact=mrc&uact=8

// "RainbowWaveView"
https://gifer.com/en/TaT4

https://i.redd.it/4s66rch87ud21.gif

// "PolarGridView"
// https://www.reddit.com/r/math/comments/anozr0/row_6_column_2_thisssis_the_abc/?st=jrtfd80c&sh=a77dd299/
// https://i.redd.it/44x3k7f3nse21.gif



// Digits of pi
https://i.redd.it/uapebfbyh4m21.gif


// "BezierView"
// https://upload.wikimedia.org/wikipedia/commons/3/3d/B%C3%A9zier_2_big.gif
  https://upload.wikimedia.org/wikipedia/commons/d/db/B%C3%A9zier_3_big.gif


// Fourier
// https://www.youtube.com/watch?v=ds0cmAV-Yek
// https://www.youtube.com/watch?v=kKu6JDqNma8
* Trace out each harmonic's circle path


https://i.redd.it/f37w95s6fh231.jpg

https://gfycat.com/smartimportantbaleenwhale
https://i.redd.it/7feghgjsujv31.gif


[Shader based](https://gfycat.com/deadpoliticaldamselfly)


[Rotating crosses](https://giphy.com/gifs/pattern-illusion-grid-xUOxf44nhgZCLdBJqU)

[Creepy eyeball](https://giphy.com/gifs/daily-gif-BVqEXU5KAHodi)

[Expanding box circle](https://giphy.com/gifs/minimal-optical-RxT1tjZdn3AVG)

[Cardiod with circles](https://www.reddit.com/r/math/comments/gfv991/creating_a_cardiod_with_circles/)
 
[Swirly](https://i.redd.it/oxbosvzwrsx41.gif)

[moving rings](https://www.reddit.com/r/gifs/comments/k0tiue/the_circles_are_stationary/)
