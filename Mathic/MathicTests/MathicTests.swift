//
//  MathicTests.swift
//  MathicTests
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

@testable import Mathic
import XCTest

class MathicTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testBezier() {
        let points: [CGPoint] = []
        
        do {
            let x = try Bezier.solve(points: points, t: 0.5)
            XCTAssertTrue(x == 0.5)
        } catch {
            XCTFail("Bezier threw error: \(error.localizedDescription)")
        }
    }
    
    func testBezier2() {
        let points = [
            CGPoint(x: 0.0, y: 1)
        ]

        do {
            let x = try Bezier.solve(points: points, t: 0.5)
            XCTAssertTrue(x == 0.5)
        } catch {
            XCTFail("Bezier threw error: \(error.localizedDescription)")
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
