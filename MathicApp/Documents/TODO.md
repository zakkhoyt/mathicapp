

//* Fix app name
//* Rotation issue on iPad
//* Bigger back button
//* Prettier title function
//* README
* Indicate what taps do on radial screens

* Views
    * Implement Rotating plus
    * Improve About


* Figure out device Rotation
* Gestures
    * Touch x/y/z 
    * rotate
    * zoom
* Display gesture parameters
* Display outputs


## 2021
* Consolidate Views/VCs
* Finish bezier stuff
    * https://www.youtube.com/watch?v=aVwxzDHniEw&t=733s
    * https://www.reddit.com/r/programming/comments/pagjqg/the_beauty_of_bezier_curves/
