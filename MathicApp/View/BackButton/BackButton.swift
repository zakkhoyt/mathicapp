//
//  BackButton.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class BackButton: UIButton {
    override func draw(_ rect: CGRect) {
        let color = tintColor ?? .label
        color.setStroke()
        color.setFill()
        let path = UIBezierPath()

        let quarterWidth = bounds.width / 4.0
        let quarterHeight = bounds.height / 4.0
        let firstPoint = CGPoint(x: quarterWidth, y: bounds.height / 2.0)
        path.move(to: firstPoint)

        do {
            let point = CGPoint(x: bounds.width - quarterWidth, y: quarterHeight)
            path.addLine(to: point)
        }
        
        path.move(to: firstPoint)

        do {
            let point = CGPoint(x: bounds.width - quarterWidth, y: bounds.height - quarterHeight)
            path.addLine(to: point)
        }
        path.lineWidth = 2
        path.lineCapStyle = .round
        path.stroke()
        
        func drawBorder() {
            UIColor.lightGray.withAlphaComponent(0.25).setStroke()
            let path = UIBezierPath(rect: bounds)
            path.lineWidth = 1
            path.stroke()
        }
//        drawBorder()
    }
}
