//
//  ToggleButtons.swift
//  Multislider
//
//  Created by Zakk Hoyt on 12/24/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

open class ToggleButtonSet: UIControl {
    public struct Item {
        public var title: String?
        public var image: UIImage?
        public var isOn: Bool = false
        
        public init(title: String?, image: UIImage? = nil, isOn: Bool = false) {
            self.title = title
            self.image = image
            self.isOn = isOn
        }
    }
    
    public var selectedIndexes: Set<Int> {
        toggleButtons.reduce(Set<Int>()) {
            if $1.isOn {
                var set = $0
                set.insert($1.tag)
                return set
            }
            return $0
        }
    }
    
    @IBInspectable
    public var titleColor: UIColor = .blue {
        didSet { updateUI() }
    }
    
    @IBInspectable
    public var onColor: UIColor = UIColor.blue.withAlphaComponent(0.5) {
        didSet { updateUI() }
    }
    
    @IBInspectable
    public var offColor: UIColor = .clear {
        didSet { updateUI() }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 1 {
        didSet { updateUI() }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 4 {
        didSet {
            updateUI() }
    }
    
    open var items: [Item] = [] {
        didSet {
            updateUI()
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        CGSize(width: bounds.width, height: bounds.height)
    }
    
    private var toggleButtons: [ToggleButton] = []
    private var stackView = UIStackView(frame: .zero)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        updateUI()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        let titles = ["One", "Two", "Three", "Four", "Five"]
        var items = titles.map { return ToggleButtonSet.Item(title: $0) }
        var itemTwo = items[1]
        itemTwo.isOn = true
        items[1] = itemTwo
        self.items = items
        updateUI()
    }
    
    private func updateUI() {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = onColor.cgColor
        
        for subview in stackView.subviews {
            subview.removeFromSuperview()
        }
        toggleButtons.removeAll()
        
        for (i, item) in items.enumerated() {
            let toggleButton = ToggleButton(frame: .zero)
            toggleButton.tag = i
            toggleButton.title = item.title
            toggleButton.image = item.image
            toggleButton.isOn = item.isOn
            toggleButton.titleColor = titleColor
            toggleButton.onColor = onColor
            toggleButton.offColor = offColor
            toggleButton.borderWidth = borderWidth
            toggleButton.cornerRadius = cornerRadius
            toggleButton.addTarget(self, action: #selector(toggleButtonTouchUpInside), for: .touchUpInside)
            toggleButtons.append(toggleButton)
            stackView.addArrangedSubview(toggleButton)
        }
    }
    
    @objc
    private func toggleButtonTouchUpInside(sender: ToggleButton) {
        sendActions(for: .valueChanged)
        sendActions(for: .touchUpInside)
    }
}

@IBDesignable
class ToggleButton: UIControl {
    var isOn: Bool = false {
        didSet { updateUI() }
    }
    
    var onColor: UIColor = .blue {
        didSet { updateUI() }
    }
    
    var offColor: UIColor = .label {
        didSet { updateUI() }
    }
    
    var borderWidth: CGFloat = 1 {
        didSet { updateUI() }
    }
    
    var cornerRadius: CGFloat = 4 {
        didSet { updateUI() }
    }
    
    var title: String? {
        didSet { updateUI() }
    }
    
    var titleColor: UIColor? {
        didSet { updateUI() }
    }
    
    var image: UIImage? {
        didSet { updateUI() }
    }
    
    override var intrinsicContentSize: CGSize {
        CGSize(width: bounds.width, height: bounds.height)
    }
    
    private var titleLabel = UILabel(frame: .zero)
    private var imageView = UIImageView(frame: .zero)
    private var stackView = UIStackView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        image = UIImage(named: "start_24x24")
        title = "Toggle"
        
        stackView.axis = .horizontal
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        let leading = stackView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8)
        leading.priority = UILayoutPriority.defaultLow
        leading.isActive = true
        let top = stackView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: 8)
        top.priority = UILayoutPriority.defaultLow
        top.isActive = true
        let trailing = stackView.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -8)
        trailing.priority = UILayoutPriority.defaultLow
        trailing.isActive = true
        let bottom = stackView.bottomAnchor.constraint(greaterThanOrEqualTo: bottomAnchor, constant: -8)
        bottom.priority = UILayoutPriority.defaultLow
        bottom.isActive = true
        
        imageView.contentMode = .scaleAspectFit
        
        titleLabel.textColor = offColor
        titleLabel.font = UIFont.preferredFont(forTextStyle: .body)
        stackView.addArrangedSubview(titleLabel)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        title = "Toggle"
        updateUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUI()
    }
    
    private func updateUI() {
        titleLabel.text = title
        titleLabel.textColor = titleColor
        
        if image != nil {
            if imageView.superview == nil {
                stackView.insertArrangedSubview(imageView, at: 0)
            }
        } else {
            imageView.removeFromSuperview()
        }
        
        if isOn {
            backgroundColor = onColor
            imageView.image = image?.imageWithColor(color: offColor)
        } else {
            backgroundColor = offColor
            imageView.image = image?.imageWithColor(color: onColor)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isOn.toggle()
        updateUI()
        sendActions(for: .valueChanged)
        sendActions(for: .touchUpInside)
    }
}

internal extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: size.width, height: size.height))
        context?.clip(to: rect, mask: cgImage)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
