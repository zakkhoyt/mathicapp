//
//  MainNavigationController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/3/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        false
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        [.all]
    }
    
    override var childForScreenEdgesDeferringSystemGestures: UIViewController? {
        viewControllers.last
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.barStyle = .black
        setRootViewController()
        setNeedsUpdateOfScreenEdgesDeferringSystemGestures()
    }
    
    private func setRootViewController() {
//        let name = MenuViewController.identifier
//        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
//        guard let viewController = storyboard.instantiateInitialViewController() else {
//            assert(false, "Failed to instantiateIntialViewController from storyboard \(name)")
//            return
//        }
        let viewController = MenuViewController()
        setViewControllers([viewController], animated: false)
    }
}
