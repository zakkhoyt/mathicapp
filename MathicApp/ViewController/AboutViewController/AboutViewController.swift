//
//  AboutViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class AboutViewController: AnimatableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        animatableView?.isUserInteractionEnabled = true
    }
}
