//
//  TrigFunctionViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import Mathic

class TrigFunctionViewController: AnimatableViewController {
    private var trigView: TrigView? {
        animatableView as? TrigView
    }
    
    @IBOutlet weak var toggleButtons: ToggleButtonSet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trigView?.isUserInteractionEnabled = true
        setupTrigFunctionSegment()
        
        // Move it to the stackView
        stackView.addArrangedSubview(toggleButtons)
    }

    private func setupTrigFunctionSegment() {
        guard let trigView = trigView else { return }
        let items = TrigView.TrigFunction.allCases.map { trigFunction -> ToggleButtonSet.Item in
            var item = ToggleButtonSet.Item(title: trigFunction.title)
            if trigView.trigFunctions.contains(trigFunction) {
                item.isOn = true
            }
            return item
        }
        toggleButtons.items = items
    }
    
    @IBAction
    func trigFunctionSegmentValueChanged(_ sender: ToggleButtonSet) {
        guard let trigView = trigView else { return }
        let trigFunctionsArray = sender.selectedIndexes.compactMap { TrigView.TrigFunction(rawValue: $0) }
        trigView.trigFunctions = Set<TrigView.TrigFunction>(trigFunctionsArray)
    }
}
