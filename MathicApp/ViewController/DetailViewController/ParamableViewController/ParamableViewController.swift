//
//  ParamableViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/1/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit
import Mathic

class ParamableViewController: AnimatableViewController {
    // MARK: Private vars

    private lazy var imageSnapper = ImageSnapper()
    private var xLabel = UILabel(frame: .zero)
    private var yLabel = UILabel(frame: .zero)
    private var zLabel = UILabel(frame: .zero)
    private var pinchLabel = UILabel(frame: .zero)
    private var rotateLabel = UILabel(frame: .zero)
    private var paramDisplayLabels: [UILabel] = []

    private var forceTouchEnabled: Bool {
        traitCollection.forceTouchCapability == .available
    }
    
    private var pinchBeganValue: CGFloat = 0
    private var rotateBeganValue: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
        updateLabels()
        setupGestures()
    }

    private func setupLabels() {
        xLabel.configure()
        yLabel.configure()
        zLabel.configure()
        pinchLabel.configure()
        rotateLabel.configure()
        stackView.insertArrangedSubview(xLabel, at: 0)
        stackView.insertArrangedSubview(yLabel, at: 1)
        stackView.insertArrangedSubview(zLabel, at: 2)
        stackView.insertArrangedSubview(pinchLabel, at: 3)
        stackView.insertArrangedSubview(rotateLabel, at: 4)
        
        if let informable = animatableView as? Informable {
            informable.didRenderFrame = { [weak self] in
                self?.updateParamLabels()
            }
        }
        if let paramableDisplayable = animatableView as? ParameterDisplayable {
            for i in 0..<paramableDisplayable.count {
                let label = UILabel(frame: .zero)
                label.configure()
                paramDisplayLabels.append(label)
                stackView.insertArrangedSubview(label, at: 3 + i)
            }
        }
    }
    
    // swiftlint:disable function_body_length
    private func updateLabels() {
        guard let paramView = animatableView else {
            return
        }
        
        let joiner = " <= "
        
        func updateXLabel() {
            guard let panable = paramView as? ParameterPanable,
                let name = panable.nameX else {
                    xLabel.isHidden = true
                    return
            }
            
            xLabel.isHidden = false
            xLabel.text = [
                "[x] - \(name): ",
                panable.displayableMinimumX,
                joiner,
                panable.displayableValueX,
                joiner,
                panable.displayableMaximumX
            ].joined(separator: " ")
        }
        
        func updateYLabel() {
            guard let panable = paramView as? ParameterPanable,
                let name = panable.nameY else {
                    yLabel.isHidden = true
                    return
            }

            yLabel.isHidden = false
            yLabel.text = [
                "[y] - \(name): ",
                panable.displayableMinimumY,
                joiner,
                panable.displayableValueY,
                joiner,
                panable.displayableMaximumY
            ].joined(separator: " ")
        }
        
        func updateZLabel() {
            guard let panable = paramView as? ParameterPanable,
                let name = panable.nameZ else {
                    zLabel.isHidden = true
                    return
            }

            zLabel.isHidden = false
            zLabel.text = [
                "[z] - \(name): ",
                panable.displayableMinimumZ,
                joiner,
                panable.displayableValueZ,
                joiner,
                panable.displayableMaximumZ
            ].joined(separator: " ")
        }
        func updatePinchLabel() {
            guard let pinchable = paramView as? ParameterPinchable,
                let name = pinchable.pinchName else {
                pinchLabel.isHidden = true
                return
            }
            pinchLabel.isHidden = false
            pinchLabel.text = [
                "[pinch] \(name): ",
                pinchable.displayablePinchMinimum,
                joiner,
                pinchable.displayablePinchValue,
                joiner,
                pinchable.displayablePinchMaximum
            ].joined(separator: " ")
        }

        func updateRotateLabel() {
            guard let rotatable = paramView as? ParameterRotatable,
                let name = rotatable.rotateName else {
                rotateLabel.isHidden = true
                return
            }
            rotateLabel.isHidden = false
            var text = "[rotate] \(name): "
            text += rotatable.displayableRotateMinimum
            text += joiner
            text += " \(rotatable.displayableRotateValue)"
            text += joiner
            text += rotatable.displayableRotateMaximum
            rotateLabel.text = text
            
            rotateLabel.text = [
                "[rotate] \(name): ",
                rotatable.displayableRotateMinimum,
                joiner,
                rotatable.displayableRotateValue,
                joiner,
                rotatable.displayableRotateMaximum
            ].joined(separator: " ")
        }

        updateXLabel()
        updateYLabel()
        updateZLabel()
        updatePinchLabel()
        updateRotateLabel()
    }
    // swiftlint:enable function_body_length
    
    private func updateParamLabels() {
        guard let paramableDisplayable = animatableView as? ParameterDisplayable else {
            return
        }
        for i in 0..<paramableDisplayable.count {
            let label = paramDisplayLabels[i]
            let param = paramableDisplayable.params[i]
            label.text = "\(param.name): \(param.value)"
        }
    }
}

extension UILabel {
    func configure() {
        textColor = .label
        textAlignment = .left
        font = UIFont.preferredFont(forTextStyle: .caption1)
        numberOfLines = 0
        minimumScaleFactor = 0.8
        translatesAutoresizingMaskIntoConstraints = false
    }
}

//    extension CGFloat {
//        private func string(sigFigs: Int) -> String {
//            let format = "%.0\(sigFigs)f"
//            return String(format: format, self)
//        }
//
//        var oneSigFig: String {
//            return string(sigFigs: 1)
//        }
//
//        var twoSigFigs: String {
//            return string(sigFigs: 2)
//        }
//    }

extension ParamableViewController {
    func setupGestures() {
        let xyzGesture = XYZGestureRecognizer(target: self, action: #selector(xyz))
        xyzGesture.delegate = self
        view.addGestureRecognizer(xyzGesture)

        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinch))
        pinchGesture.delegate = self
        view.addGestureRecognizer(pinchGesture)

        let rotatateGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotate))
        rotatateGesture.delegate = self
        view.addGestureRecognizer(rotatateGesture)
    }
    
    @objc
    private func xyz(sender: XYZGestureRecognizer) {
        guard var panable = animatableView as? (ParameterPanable & UIView) else { return }
        switch sender.state {
        case .began, .changed, .ended:
            panable.valueX = panable.minimumX + panable.xSpan * sender.xyzNormalized.x
            panable.valueY = panable.minimumY + panable.ySpan * sender.xyzNormalized.y
            panable.valueZ = 1.0 // panable.minimumZ + panable.zSpan * sender.xyzNormalized.z
            panable.setNeedsDisplay()
            updateLabels()
        default:
            break
        }
    }

    @objc
    private func pinch(sender: UIPinchGestureRecognizer) {
        guard var pinchable = animatableView as? (ParameterPinchable & UIView) else { return }
        if sender.state == .began {
            pinchBeganValue = pinchable.pinchValue
        }
        
        switch sender.state {
        case .began, .changed, .ended:
            let value = pinchBeganValue * sender.scale
            let clippedValue = max(min(value, pinchable.pinchMaximum), pinchable.pinchMinimum)
            pinchable.pinchValue = clippedValue
            pinchable.setNeedsDisplay()
            updateLabels()
        default: break
        }
    }
    
    @objc
    private func rotate(sender: UIRotationGestureRecognizer) {
        guard var rotatable = animatableView as? (ParameterRotatable & UIView) else { return }
        if sender.state == .began {
            rotateBeganValue = rotatable.rotateValue
        }

        switch sender.state {
        case .began, .changed, .ended:
            let maximumRotation = CGFloat.pi
            let ratio = min(max(sender.rotation / maximumRotation, -1.0), 1.0)
            let full = rotatable.rotateMaximum - rotatable.rotateMinimum
            let delta = ratio * full
            let value = rotateBeganValue + delta
            let clippedValue = max(min(value, rotatable.rotateMaximum), rotatable.rotateMinimum)
            rotatable.rotateValue = clippedValue
            rotatable.setNeedsDisplay()
            updateLabels()
        default: break
        }
    }
}

extension UIGestureRecognizer.State: CustomStringConvertible {
    public var description: String {
        switch self {
        case .possible: return "Possible"
        case .began: return "Began"
        case .changed: return "Changed"
        case .ended: return "Ended"
        case .cancelled: return "Cancelled"
        case .failed: return "Failed"
        @unknown default: return "Unknown"
        }
    }
}

extension ParamableViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
//        switch (gestureRecognizer, otherGestureRecognizer) {
//        case (is UIPanGestureRecognizer, _):
//            return false
//        case (is UIPinchGestureRecognizer, _):
//            return false
//        case (is UIRotationGestureRecognizer, _):
//            return false
//        default:
//            return true
//        }
    }
}
