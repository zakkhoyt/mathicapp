//
//  AnimatableViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/4/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit
import Mathic

private let kDuration: TimeInterval = 0.3
private let kDelay: TimeInterval = 3

class AnimatableViewController: DetailViewController {
    // MARK: Internal  vars
    
    /// Pass in a AnimatableView class which will be instantiated and presented
    var animatableViewType: AnimatableView.Type?
    var animatableView: AnimatableView?
    var titleLabelView = UIView(frame: .zero)
    var titleLabel = UILabel(frame: .zero)
    var frameRateLabel = UILabel(frame: .zero)
    var stackView = UIStackView(frame: .zero)
    
    // MARK: Private vars
    
    private lazy var imageSnapper = ImageSnapper()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupFrameRateLabel()
        setupAnimatableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.animateTitleLabel()
            self?.showInstructionsLabel()
        }, completion: nil)
    }
    
    private func showInstructionsLabel() {
        guard let instructable = animatableView as? ParameterInstructable,
            let instructions = instructable.instructions else {
            return
        }

        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        label.text = instructions
        label.alpha = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 88).isActive = true
        
        UIView.animate(withDuration: kDuration,
                       animations: {
            label.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: kDuration,
                           delay: kDelay,
                           options: [.allowAnimatedContent],
                           animations: {
                            label.alpha = 0
            }, completion: { _ in
                label.removeFromSuperview()
            })
        })
    }

    private func setupAnimatableView() {
        guard let animatableViewType = animatableViewType else {
            return
        }
        
        let animatableView = animatableViewType.init(frame: .zero)
        switch animatableView {
        case is BezierCurveView: animatableView.isUserInteractionEnabled = true
        default: animatableView.isUserInteractionEnabled = false
        }
        
        animatableView.start()
        
        // Configure snapshots on double touch (if enabled)
        if ProcessInfo.processInfo.arguments.contains("SNAPSHOT_ON_DOUBLE_TOUCH") {
            imageSnapper.monitor(view: animatableView)
        }
        
        // Get frame rate updates
        if let animationView = animatableView as? AnimationView {
            animationView.frameRateDelegate = self
        }
        
        view.insertSubview(animatableView, at: 0)
        
        animatableView.translatesAutoresizingMaskIntoConstraints = false
        let constant: CGFloat = 0
        animatableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                                constant: constant).isActive = true
        animatableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                            constant: constant).isActive = true
        animatableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                 constant: -constant).isActive = true
        animatableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                               constant: -constant).isActive = true
        self.animatableView = animatableView
    }
    
    private func setupFrameRateLabel() {
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                           constant: 16).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                          constant: -20).isActive = true

        frameRateLabel.configure()
        stackView.addArrangedSubview(frameRateLabel)
    }
    
    // swiftlint:disable function_body_length
    private func animateTitleLabel() {
        guard let animatableViewType = animatableViewType
            else { return }

        titleLabelView.isUserInteractionEnabled = false
        titleLabelView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        titleLabelView.layer.masksToBounds = true
        titleLabelView.layer.cornerRadius = 4
        titleLabelView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleLabelView)
        view.bringSubviewToFront(titleLabelView)
        titleLabelView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                                constant: 60).isActive = true
        titleLabelView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                constant: -60).isActive = true
        titleLabelView.centerYAnchor.constraint(equalTo: backButton.centerYAnchor).isActive = true
        
        titleLabel.text = animatableViewType.animationTitle
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        titleLabel.textColor = .label
        titleLabel.minimumScaleFactor = 0.8
        titleLabel.numberOfLines = 0
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabelView.addSubview(titleLabel)
        let labelInset: CGFloat = 8
        titleLabel.leadingAnchor.constraint(equalTo: titleLabelView.leadingAnchor,
                                            constant: labelInset).isActive = true
        titleLabel.topAnchor.constraint(equalTo: titleLabelView.topAnchor,
                                        constant: labelInset).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: titleLabelView.trailingAnchor,
                                             constant: -labelInset).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: titleLabelView.bottomAnchor,
                                           constant: -labelInset).isActive = true
        
        let minimized = CGAffineTransform.identity
            .scaledBy(x: 0.5, y: 0.5)
        titleLabelView.alpha = 0
        titleLabelView.transform = minimized
        
        // Animate in/out
        UIView.animate(withDuration: kDuration,
                       delay: 0,
                       options: [],
                       animations: { [weak self] in
                        self?.titleLabelView.alpha = 1.0
                        self?.titleLabelView.transform = CGAffineTransform.identity
        }, completion: { _ in
            UIView.animate(withDuration: kDuration,
                           delay: kDelay,
                           options: [],
                           animations: { [weak self] in
                            self?.titleLabelView.alpha = 0
                            self?.titleLabelView.transform = minimized
                }, completion: nil)
        })
    }
    // swiftlint:enable function_body_length
}

extension AnimatableViewController: AnimationViewFrameRateDelegate {
    func animationViewDidRenderFrame(_ animationView: AnimationView, frameRate: Double) {
        frameRateLabel.text = String(format: "%.1ffps", frameRate)
    }
}
