//
//  FourierViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Mathic
import UIKit

class FourierViewController: AnimatableViewController {
    private var fourierView: FourierView? {
        animatableView as? FourierView
    }
    
    @IBOutlet weak var parametersView: UIView!
    @IBOutlet weak var fourierSegment: UISegmentedControl!
    @IBOutlet weak var fourierIterationsStepper: UIStepper!
    @IBOutlet weak var fourierIterationsLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        fourierView?.isUserInteractionEnabled = true
        setupFourierSegment()
        setupFourierStepper()

        // Move it to the stackView
        stackView.addArrangedSubview(parametersView)
    }

    private func setupFourierSegment() {
        fourierSegment.removeAllSegments()
        
        for (i, waveform) in FourierView.Waveform.allCases.enumerated() {
            fourierSegment.insertSegment(withTitle: waveform.title, at: i, animated: false)
        }
        fourierSegment.selectedSegmentIndex = FourierView.Waveform.square.rawValue
        fourierSegmentValueChanged(fourierSegment)
    }
    
    private func setupFourierStepper() {
        fourierIterationsLabel.textColor = .lightGray
        fourierIterationsStepper.minimumValue = 1
        fourierIterationsStepper.maximumValue = 50
        fourierIterationsStepper.value = 2
        fourierIterationsStepper.stepValue = 1.0
        fourierIterationsStepperValueChanged(fourierIterationsStepper)
    }

    @IBAction
    func fourierSegmentValueChanged(_ sender: UISegmentedControl) {
        guard let fourierView = fourierView else { return }
        guard let waveform = FourierView.Waveform(rawValue: sender.selectedSegmentIndex) else { return }
        fourierView.waveform = waveform
    }
    
    @IBAction
    func fourierIterationsStepperValueChanged(_ sender: UIStepper) {
        guard let fourierView = fourierView else { return }
        fourierView.accuracy = CGFloat(sender.value)
        fourierIterationsLabel.text = "Iterations: \(fourierView.accuracy)"
    }
}
