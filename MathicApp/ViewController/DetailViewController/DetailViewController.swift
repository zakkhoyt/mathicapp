//
//  DetailViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class ThemeViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        false
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        [.all]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

class DetailViewController: ThemeViewController {
    var backButton = BackButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    private func setupBackButton() {
        backButton.tintColor = .label
        backButton.addTarget(self, action: #selector(backButtonTouchUpInside), for: .touchUpInside)
        view.addSubview(backButton)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                            constant: 8).isActive = true
        backButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                        constant: 8).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }

    @objc
    private func backButtonTouchUpInside(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension UIView {
    static var identifier: String {
        String(describing: classForCoder())
    }
}
