//
//  ParamableMenuViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/1/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit
import Mathic

protocol MenuDisplayable {
    var animationTitle: String { get }
    var animatableViewType: AnimatableView.Type { get }
    var viewControllerType: UIViewController.Type { get }
}

enum ViewMenuItem: Int, MenuDisplayable, CaseIterable {
//    case piDigits
//    case rotatingCrosses
    case wilsonMazeView
    case waveInterference
    case swirly
    case cardioidCircles
    case bezier
    case quadraticBezier
    case fourier
    case trig
    case polygonCycle
    case polarGrid
    case cardiogram
    case circleWave
    case rotatingPlusses
    case hueDots
    case purpleDots
    case hueCircle
    case slidingRings
    case rippleRings
    case nestedRingWave
    case cyclicHueBars
    case rainbowWave
    case matrix
    case ellipse
    case linearEquation
    case quadraticEquation
    case derivative
    case intergral
    case california
    
    var animationTitle: String {
        animatableViewType.animationTitle
    }
    
    var animatableViewType: AnimatableView.Type {
        switch self {
        case .wilsonMazeView: return WilsonMazeView.self
        case .waveInterference: return WaveInterferenceView.self
        case .swirly: return SwirlyView.self
//        case .rotatingCrosses: return RotatingCrossesView.self
        case .cardioidCircles: return CardioidCircleView.self
        case .bezier: return BezierCurveView.self
        case .quadraticBezier: return QuadraticBezierView.self
//        case .piDigits: return PiDigitsView.self
        case .linearEquation: return LinearEquationView.self
        case .quadraticEquation: return QuadraticEquationView.self
        case .derivative: return DerivativeView.self
        case .intergral: return IntegralView.self
        case .ellipse: return EllipseView.self
        case .matrix: return MatrixView.self
        case .trig: return TrigView.self
        case .fourier: return FourierView.self
        case .polygonCycle: return PolygonCycleView.self
        case .california: return CaliforniaView.self
        case .cardiogram: return CardiogramView.self
        case .circleWave: return CircleWaveView.self
        case .hueDots: return HueDotsView.self
        case .purpleDots: return PurpleDotsView.self
        case .hueCircle: return HueCircleView.self
        case .slidingRings: return SlidingRingsView.self
        case .rippleRings: return RippleRingView.self
        case .nestedRingWave: return NestedRingWaveView.self
        case .cyclicHueBars: return CyclicHueBarsView.self
        case .rainbowWave: return RainbowWaveView.self
        case .polarGrid: return PolarGridView.self
        case .rotatingPlusses: return RotatingPlusView.self
        }
    }
    
    var viewControllerType: UIViewController.Type {
        switch self {
        case .trig: return TrigFunctionViewController.self
        case .fourier: return FourierViewController.self
        case .california: return AnimatableViewController.self
        default: return ParamableViewController.self
        }
    }
}

enum AboutMenuItem: Int, MenuDisplayable, CaseIterable {
    case about
    
    var animationTitle: String {
        switch self {
        case .about: return "About"
        }
    }
    
    var animatableViewType: AnimatableView.Type {
        switch self {
        case .about: return AboutView.self
        }
    }
    
    var viewControllerType: UIViewController.Type {
        switch self {
        case .about: return AboutViewController.self
        }
    }
}

class MenuViewController: ThemeViewController {
    private let iPhoneSingle = true
    
    private enum CollectionViewSection: Int, CaseIterable {
        case views
        case about
    }
    
    private enum DeviceOrientation: String {
        case portrait = "Portrait"
        case landscape = "Landscape"
    }
    
    private var deviceOrientation: DeviceOrientation = .portrait {
        didSet {
            print("changed device orientation: \(deviceOrientation.rawValue)")
            updateCollectionViewLayout()
        }
    }
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let spacing: CGFloat = 20
    private let titleHeight: CGFloat = 44
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        updateCollectionViewLayout()
        updateDeviceOrientation()
        collectionView.reloadData()
        setupNotificationObservers()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        #if targetEnvironment(macCatalyst)
        // Catch window resizing
        collectionView.reloadData()
        #endif
    }

    private func setupNotificationObservers() {
        // willTransition is usually used for this but that method will not
        // fire if before/after coordinators are the same (iPad)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deviceOrientationDidChange),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: nil)
    }
    
    @objc
    private func deviceOrientationDidChange(note: Notification) {
        DispatchQueue.main.async { [weak self] in
            self?.updateDeviceOrientation()
        }
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = .darkGray
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        let nib = UINib(nibName: MenuCollectionViewCell.identifier, bundle: Bundle.main)
        collectionView.register(nib, forCellWithReuseIdentifier: MenuCollectionViewCell.identifier)
    }
    
    private func pushViewController(menuItem: MenuDisplayable) {
        guard let navigationController = navigationController else {
            assert(false, "MenuViewController is not embedded in a UINavigationController")
            return
        }
        
        // Create viewController
        let storyboardName = String(describing: menuItem.viewControllerType)
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        guard let viewController = storyboard.instantiateInitialViewController() else {
            assert(false, "No view controller was instantiated")
            return
        }
        
        // Pass params to vc if needed
        if let animatableViewController = viewController as? AnimatableViewController {
            animatableViewController.animatableViewType = menuItem.animatableViewType
            navigationController.pushViewController(animatableViewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    private func updateCollectionViewLayout() {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            assert(false, "Collection view is not UICollectionViewFlowLayout")
            return
        }
        
        #if targetEnvironment(macCatalyst)
        layout.scrollDirection = .vertical
        #else
        switch deviceOrientation {
        case .portrait:
            layout.scrollDirection = .vertical
        case .landscape:
            layout.scrollDirection = .horizontal
        }
        #endif
    }
    
    private func updateDeviceOrientation() {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            deviceOrientation = .landscape
        } else if UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown {
            deviceOrientation = .portrait
        }
    }
}

// swiftlint:disable force_cast force_unwrapping
extension MenuViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        CollectionViewSection.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let cvSection = CollectionViewSection(rawValue: section)!
        switch cvSection {
        case .views: return ViewMenuItem.allCases.count
        case .about: return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cvSection = CollectionViewSection(rawValue: indexPath.section)!
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCollectionViewCell.identifier,
                                                      for: indexPath) as! MenuCollectionViewCell
        
        switch cvSection {
        case .views:
            let paramableMenuItem = ViewMenuItem(rawValue: indexPath.item)!
            cell.menuItem = paramableMenuItem
        case .about:
            let paramableMenuItem = AboutMenuItem(rawValue: indexPath.item)!
            cell.menuItem = paramableMenuItem
        }
        return cell
    }
}

extension MenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        func value(range: ClosedRange<CGFloat>, total: CGFloat) -> CGFloat? {
            for i in 1...10 {
                let v = total / CGFloat(i)
                if range.contains(v) {
                    return v
                }
            }
            return nil
        }
        
        let defaultWidth: CGFloat = collectionView.bounds.width * 0.75
        let defaultHeight: CGFloat = collectionView.bounds.height * 0.6
        let aspectRatio: CGFloat = CGFloat(16) / CGFloat(9)
        
        #if targetEnvironment(macCatalyst)
        let width = value(range: 200...400, total: collectionView.bounds.width) ?? defaultWidth
        let height = (width / aspectRatio) + titleHeight
        return CGSize(width: width, height: height)

        #else
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            switch iPhoneSingle {
            case true:
                // Original single row/col
                switch deviceOrientation {
                case .landscape:
                    let height = collectionView.bounds.height * 0.6
                    let width = ((height - titleHeight) * aspectRatio)
                    return CGSize(width: width, height: height)
                case .portrait:
                    let width = collectionView.bounds.width * 0.75
                    let height = (width / aspectRatio) + titleHeight
                    return CGSize(width: width, height: height)
                }
            case false:
                // Double col/row
                switch deviceOrientation {
                case .landscape:
                    let height = collectionView.safeAreaSize.height / 2
                    let width = ((height - titleHeight) * aspectRatio)
                    return CGSize(width: width, height: height)
                case .portrait:
                    let width = collectionView.safeAreaSize.width / 2
                    let height = (width / aspectRatio) + titleHeight
                    return CGSize(width: width, height: height)
                }
            }
        case .pad:
            switch deviceOrientation {
            case .landscape:
                let height: CGFloat = value(range: 150...200,
                                            total: collectionView.safeAreaSize.height) ?? defaultHeight
                let width = ((height - titleHeight) * aspectRatio)
                return CGSize(width: width, height: height)
            case .portrait:
                let width = value(range: 200...400, total: collectionView.safeAreaSize.width) ?? defaultWidth
                let height = (width / aspectRatio) + titleHeight
                return CGSize(width: width, height: height)
            }
        default:
            switch deviceOrientation {
            case .landscape:
                let height = defaultHeight
                let width = ((height - titleHeight) * aspectRatio)
                return CGSize(width: width, height: height)
            case .portrait:
                let width = defaultWidth
                let height = (width / aspectRatio) + titleHeight
                return CGSize(width: width, height: height)
            }
        }

        #endif
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        #if targetEnvironment(macCatalyst)
        return 0
        #else
        switch UIDevice.current.userInterfaceIdiom {
        case .phone: return iPhoneSingle ? spacing : 0
        default: return 0
        }
        #endif
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        switch deviceOrientation {
        case .landscape:
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
        case .portrait:
            return UIEdgeInsets(top: spacing, left: 0, bottom: 0, right: 0)
        }
    }
}

extension MenuViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        #if DEBUG
        print("size: \(cell.frame.size)")
        #endif
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cvSection = CollectionViewSection(rawValue: indexPath.section)!
        switch cvSection {
        case .views:
            let menuItem = ViewMenuItem(rawValue: indexPath.item)!
            pushViewController(menuItem: menuItem)
        case .about:
            let menuItem = AboutMenuItem(rawValue: indexPath.item)!
            pushViewController(menuItem: menuItem)
        }
    }
}
// swiftlint:enable force_cast force_unwrapping
