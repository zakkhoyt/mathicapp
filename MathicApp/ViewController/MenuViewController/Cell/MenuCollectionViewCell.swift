//
//  MenuCollectionViewCell.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/1/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit
import Mathic

class MenuCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var paramCountLabel: UILabel!
    
    private var animatableView: AnimatableView?
    private var borderView = UIView(frame: .zero)
    private let cornerRadius: CGFloat = 8
    
    var menuItem: MenuDisplayable? {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = .label
        imageView.contentMode = .scaleAspectFit

        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = cornerRadius
        contentView.clipsToBounds = true
    }
    
    private func updateUI() {
        guard let menuItem = menuItem else {
            return
        }
        
        titleLabel.text = menuItem.animationTitle
        imageView.backgroundColor = .systemBackground
        contentView.backgroundColor = .systemBackground
        
        func shouldLoadParamableView() -> Bool {
            guard let animatableView = self.animatableView else { return true }
            return animatableView.classForCoder != menuItem.animatableViewType.classForCoder()
        }

        // Only load the view if it is nil or a different view
        if shouldLoadParamableView() {
            let animatableViewType = menuItem.animatableViewType
            let animatableView = animatableViewType.init(frame: .zero)
            animatableView.isUserInteractionEnabled = false
            contentView.addSubview(animatableView)
            animatableView.translatesAutoresizingMaskIntoConstraints = false
            let constant: CGFloat = 0
            animatableView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor,
                                                   constant: constant).isActive = true
            animatableView.topAnchor.constraint(equalTo: imageView.topAnchor,
                                               constant: constant).isActive = true
            animatableView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor,
                                                    constant: -constant).isActive = true
            animatableView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor,
                                                  constant: -constant).isActive = true
            self.animatableView = animatableView
        }

        updateBorderView()
        contentView.bringSubviewToFront(borderView)
    }
    
    private func updateBorderView() {
        if borderView.superview == nil {
            borderView.backgroundColor = .clear
            borderView.layer.borderColor = UIColor.label.cgColor
            borderView.layer.borderWidth = 1
            borderView.layer.masksToBounds = true
            borderView.layer.cornerRadius = cornerRadius
            borderView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(borderView)
            borderView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
            borderView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            borderView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
            borderView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        }
    }
}
