//
//  AppDelegate.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = MainNavigationController()
        window?.makeKeyAndVisible()
        
        #if targetEnvironment(macCatalyst)
        UIApplication.shared.connectedScenes.compactMap { $0 as? UIWindowScene }.forEach { windowScene in
//            windowScene.sizeRestrictions?.minimumSize = CGSize(width: 1280, height: 768)
//            windowScene.sizeRestrictions?.maximumSize = CGSize(width: 1280, height: 768)
            let s: CGFloat = 1024
            windowScene.sizeRestrictions?.minimumSize = CGSize(width: s, height: s)
            windowScene.sizeRestrictions?.maximumSize = CGSize(width: s, height: s)
        }
        #endif
        return true
    }
}
