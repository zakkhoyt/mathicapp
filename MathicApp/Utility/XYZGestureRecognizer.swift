//
//  DeepPressGestureRecognizer.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/28/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class XYZGestureRecognizer: UIGestureRecognizer {
    struct XYZ {
        var x: CGFloat = 0
        var y: CGFloat = 0
        var z: CGFloat = 0
    }

    private(set) var xyzNormalized = XYZ(x: 0, y: 0, z: 0)
    private(set) var xyz = XYZ(x: 0, y: 0, z: 0)
    
    private var target: AnyObject?
    private var action: Selector
    
    required init(target: AnyObject?, action: Selector) {
        self.target = target
        self.action = action
        super.init(target: target, action: action)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
    
        if shouldCancelGesture(touches, with: event) {
            self.state = .cancelled
            return
        }
        handle(touches, state: .began)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        handle(touches, state: .changed)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        handle(touches, state: .ended)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
    }
    
    private func shouldCancelGesture(_ touches: Set<UITouch>, with event: UIEvent) -> Bool {
        guard let touch = touches.first,
            let view = self.view else {
                return true
        }
        let point = touch.location(in: view)
        guard let htView = view.hitTest(point, with: event) else {
            return true
        }
        return !(htView === view)
    }
    
    private func handle(_ touches: Set<UITouch>, state: UIGestureRecognizer.State) {
        guard let touch = touches.first, let view = view else { return }
        let point = location(in: view)
        
        func updateXYZ() {
            let x = point.x
            let y = point.y
            let z = touch.force
            xyz = XYZ(x: x, y: y, z: z)
        }
        updateXYZ()
        func updateXYZNormalized() {
            let x = point.x / view.bounds.width
            let y = point.y / view.bounds.height
            let z = touch.force / touch.maximumPossibleForce
            xyzNormalized = XYZ(x: x, y: y, z: z)
        }
        updateXYZNormalized()
        
        self.state = state
        _ = target?.perform(action, with: self)
    }
}
