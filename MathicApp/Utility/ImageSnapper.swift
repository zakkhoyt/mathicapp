//
//  ImageSnapper.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/2/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  This tool is for grabbing snapshots of views mainly for project asset purposes.
//  When a user taps with two fingers, a JPG will be written to the documents dir.
//  The image will be named ~/Documents/$(class).jpg

import UIKit
import Mathic

class ImageSnapper {
    private var outputDirectoryUrl: URL? {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
            return nil
        }
        return URL(fileURLWithPath: path)
    }
    
    private var doubleTapRecognizer: UITapGestureRecognizer?
    
    ///  This tool is for grabbing snapshots of views mainly for project asset purposes.
    ///  When a user taps with two fingers, a JPG will be written to the documents dir.
    ///  The image will be named ~/Documents/$(class).jpg
    ///
    ///
    init() {
    }
    
    func monitor(view: AnimatableView) {
        view.isUserInteractionEnabled = true
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTapRecognizer.numberOfTouchesRequired = 2
        view.addGestureRecognizer(doubleTapRecognizer)
        self.doubleTapRecognizer = doubleTapRecognizer
    }
    
    @objc
    private func doubleTapped(sender: UITapGestureRecognizer) {
        guard let view = sender.view else {
            print("Failed to get view from gesture recognizer")
            return
        }
        guard let image = view.snapshot() else {
            print("failed to get image for view")
            return
        }
        
        guard let data = image.jpegData(compressionQuality: 1) else {
            print("failed to convert image to jpg")
            return
        }
        
        guard let url = createSnapshotUrl() else {
            print("failed to create url")
            return
        }
        
        let d = String(describing: view.classForCoder)
        print("Added gesture to \(d)")
        
        do {
            try data.write(to: url)
            print("wrote image to file at: \(url.absoluteString)")
        } catch {
            print("error writing image to file: \(error.localizedDescription)")
        }
    }
    
    private func createSnapshotUrl() -> URL? {
        guard let url = outputDirectoryUrl else {
            return nil
        }
        guard let view = doubleTapRecognizer?.view else {
            return nil
        }
        return url.appendingPathComponent(String(describing: view.classForCoder)).appendingPathExtension("jpg")
    }
}

extension UIView {
    func snapshot() -> UIImage? {
        UIGraphicsImageRenderer(bounds: bounds).image { [weak self] _ in
            guard let safeSelf = self else { return }
            self?.drawHierarchy(in: safeSelf.bounds, afterScreenUpdates: true)
        }
    }
}
