//
//  SplitViewController.swift
//  Bezier
//
//  Created by Zakk Hoyt on 8/25/21.
//
//  https://www.raywenderlich.com/4613809-uisplitviewcontroller-tutorial-getting-started

import UIKit

class SplitViewController: UISplitViewController {
    override init(style: UISplitViewController.Style = .doubleColumn) {
        super.init(style: style)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        print(#function)
     
        let bezierViewController = BezierViewController()
        let settingsViewController = SettingsViewController()
        settingsViewController.bezierDelegate = bezierViewController
        
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        viewControllers = [navigationController, bezierViewController]
                
        delegate = settingsViewController
        
        if #available(iOS 14.5, *) {
            displayModeButtonVisibility = .automatic
        }
        preferredDisplayMode = .automatic
        preferredSplitBehavior = .displace
        show(.primary)
    }
}
