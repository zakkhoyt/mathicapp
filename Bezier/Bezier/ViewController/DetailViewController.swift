//
//  DetailViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class ThemeViewController: UIViewController {
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        .lightContent
//    }
    
    override var prefersStatusBarHidden: Bool {
        false
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        [.all]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
    }
}

class DetailViewController: ThemeViewController {
//    var backButton = BackButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupBackButton()
    }
    
//    private func setupBackButton() {
//        backButton.tintColor = .label
//        backButton.addTarget(self, action: #selector(backButtonTouchUpInside), for: .touchUpInside)
//        view.addSubview(backButton)
//        backButton.translatesAutoresizingMaskIntoConstraints = false
//        backButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
//                                            constant: 8).isActive = true
//        backButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
//                                        constant: 8).isActive = true
//        backButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
//        backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
//    }
//
//    @objc
//    private func backButtonTouchUpInside(sender: UIButton) {
//        navigationController?.popViewController(animated: true)
//    }
}

extension UIView {
    static var identifier: String {
        String(describing: classForCoder())
    }
}

class BackButton: UIButton {
    override func draw(_ rect: CGRect) {
        let color = tintColor ?? .label
        color.setStroke()
        color.setFill()
        let path = UIBezierPath()

        let quarterWidth = bounds.width / 4.0
        let quarterHeight = bounds.height / 4.0
        let firstPoint = CGPoint(x: quarterWidth, y: bounds.height / 2.0)
        path.move(to: firstPoint)

        do {
            let point = CGPoint(x: bounds.width - quarterWidth, y: quarterHeight)
            path.addLine(to: point)
        }
        
        path.move(to: firstPoint)

        do {
            let point = CGPoint(x: bounds.width - quarterWidth, y: bounds.height - quarterHeight)
            path.addLine(to: point)
        }
        path.lineWidth = 2
        path.lineCapStyle = .round
        path.stroke()
        
        func drawBorder() {
            UIColor.lightGray.withAlphaComponent(0.25).setStroke()
            let path = UIBezierPath(rect: bounds)
            path.lineWidth = 1
            path.stroke()
        }
//        drawBorder()
    }
}
