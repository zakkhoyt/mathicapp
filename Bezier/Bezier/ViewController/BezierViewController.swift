//
//  AnimatableViewController.swift
//  MathicApp
//
//  Created by Zakk Hoyt on 2/4/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  This looks great! Yeah, if you're curious, take a look at Canvas, introduced this year.
//  It iterates on a lot of the core drawing principals from CoreGraphics.
//  Makes Core Graphics Context a value-type that's easier to work with.

import UIKit

private let kDuration: TimeInterval = 0.3
private let kDelay: TimeInterval = 3

class BezierViewController: DetailViewController {
    // MARK: Internal  vars
    
    // Pass in a AnimatableView class which will be instantiated and presented
    let controlPointSize: CGFloat = 40
    var animatableView: AnimatableView?
    var titleLabelView = UIView(frame: .zero)
    var titleLabel = UILabel(frame: .zero)
    var frameRateLabel = UILabel(frame: .zero)
    var stackView = UIStackView(frame: .zero)
    
    // MARK: Private vars
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFrameRateLabel()
        setupAnimatableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.animateTitleLabel()
        }, completion: nil)
    }
    
    private func setupAnimatableView() {
        let animatableView = CubicBezierView(controlPointSize: controlPointSize)
        animatableView.start()
        animatableView.frameRateDelegate = self
        
        view.insertSubview(animatableView, at: 0)
        animatableView.translatesAutoresizingMaskIntoConstraints = false
        
        let constant: CGFloat = controlPointSize / 2
        NSLayoutConstraint.activate([
            animatableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: constant),
            animatableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: constant),
            animatableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -constant),
            animatableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -constant)
        ])
        
        self.animatableView = animatableView
    }
    
    private func setupFrameRateLabel() {
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
        frameRateLabel.configure()
        stackView.addArrangedSubview(frameRateLabel)
    }
    
    // swiftlint:disable function_body_length
    private func animateTitleLabel() {
        titleLabelView.isUserInteractionEnabled = false
        titleLabelView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
        titleLabelView.layer.masksToBounds = true
        titleLabelView.layer.cornerRadius = 4
        titleLabelView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleLabelView)
        view.bringSubviewToFront(titleLabelView)
        NSLayoutConstraint.activate([
            titleLabelView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 60),
            titleLabelView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -60),
            titleLabelView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        titleLabel.text = CubicBezierView.animationTitle
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        titleLabel.textColor = .label
        titleLabel.minimumScaleFactor = 0.8
        titleLabel.numberOfLines = 0
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabelView.addSubview(titleLabel)
        let labelInset: CGFloat = 8
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: titleLabelView.leadingAnchor, constant: labelInset),
            titleLabel.topAnchor.constraint(equalTo: titleLabelView.topAnchor, constant: labelInset),
            titleLabel.trailingAnchor.constraint(equalTo: titleLabelView.trailingAnchor, constant: -labelInset),
            titleLabel.bottomAnchor.constraint(equalTo: titleLabelView.bottomAnchor, constant: -labelInset)
        ])
        
        let minimized = CGAffineTransform.identity
            .scaledBy(x: 0.5, y: 0.5)
        titleLabelView.alpha = 0
        titleLabelView.transform = minimized
        
        // Animate in/out
        UIView.animate(
            withDuration: kDuration,
            delay: 0,
            options: [],
            animations: { [weak self] in
                self?.titleLabelView.alpha = 1.0
                self?.titleLabelView.transform = CGAffineTransform.identity
            },
            completion: { _ in
                UIView.animate(
                    withDuration: kDuration,
                    delay: kDelay,
                    options: [],
                    animations: { [weak self] in
                        self?.titleLabelView.alpha = 0
                        self?.titleLabelView.transform = minimized
                    },
                    completion: nil
                )
            }
        )
    }
    // swiftlint:enable function_body_length
}

extension BezierViewController: AnimationViewFrameRateDelegate {
    func animationViewDidRenderFrame(_ animationView: AnimationView, frameRate: Double) {
        frameRateLabel.text = String(format: "%.1ffps", frameRate)
    }
}

extension UILabel {
    func configure() {
        textColor = .label
        textAlignment = .left
        font = UIFont.preferredFont(forTextStyle: .caption1)
        numberOfLines = 0
        minimumScaleFactor = 0.8
        translatesAutoresizingMaskIntoConstraints = false
    }
}

extension BezierViewController: SettingsViewControllerBezierDelegate {
    func settingsViewController(_ settingsViewController: SettingsViewController, controlViewSize: CGFloat) {
    }
}
