//
//  SettingsViewController.swift
//  Bezier
//
//  Created by Zakk Hoyt on 8/25/21.
//

import UIKit

protocol SettingsViewControllerBezierDelegate: AnyObject {
    func settingsViewController(_ settingsViewController: SettingsViewController, controlViewSize: CGFloat)
}

protocol CellContentProvider {
    var title: String { get }
    var icon: UIImage? { get }
}

extension CellContentProvider where Self: RawRepresentable, RawValue == Int {
    /// Converts indexPath.row into a unique number when compared to other sections.
    /// We can't have 2 cells with ID 0, right?
    /// - Parameter section: Section of the collectionView
    /// - Returns: Returns a (hopefully) stable unique number per row/section.
    func id(section: Int) -> Int {
        1000 * section + rawValue // Not likely to have more than 1000 items in any section
    }
}

class SettingsViewController: UIViewController {
    // MARK: Nested Types
    
    private enum Section: Int, CaseIterable, CustomStringConvertible {
        case visibility
        case configuration
        
        var description: String {
            switch self {
            case .visibility: return "Visibility"
            case .configuration: return "Configuration"
            }
        }
    }
    
    private enum VisiblityItem: Int, CellContentProvider, CaseIterable {
        case showBezier
        
        var title: String {
            switch self {
            case .showBezier: return "Show Bezier"
            }
        }
        var icon: UIImage? {
            switch self {
            case .showBezier: return UIImage(systemName: "point.topleft.down.curvedto.point.bottomright.up")
            }
        }
    }
    
    private enum ConfigurationItem: Int, CellContentProvider, CaseIterable {
        case numberOfControlPoints
        
        var title: String {
            switch self {
            case .numberOfControlPoints: return "# of Control Points"
            }
        }
        var icon: UIImage? {
            switch self {
            case .numberOfControlPoints: return UIImage(systemName: "number.circle")
            }
        }
    }
    
    // MARK: Internal vars
    
    var bezierDelegate: SettingsViewControllerBezierDelegate?
    
    // MARK: Private vars
    
    private lazy var collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            let section = NSCollectionLayoutSection.list(
                using: UICollectionLayoutListConfiguration(appearance: .sidebarPlain),
                layoutEnvironment: layoutEnvironment
            )
            return section
        }
    )
    
    private lazy var dataSource: UICollectionViewDiffableDataSource<Section, Int> = {
        UICollectionViewDiffableDataSource<Section, Int>(
            collectionView: collectionView
        ) { collectionView, indexPath, sectionItem in
            guard let section = Section(rawValue: indexPath.section) else {
                preconditionFailure("Invalid Section")
            }
            switch section {
            case .visibility:
                guard let visiblityItem = VisiblityItem(rawValue: indexPath.row) else {
                    preconditionFailure("Invalid VisiblityRow")
                }
                return collectionView.dequeueConfiguredReusableCell(
                    using: self.configuredListCell(),
                    for: indexPath,
                    item: visiblityItem
                )
            case .configuration:
                guard let configurationItem = ConfigurationItem(rawValue: indexPath.row) else {
                    preconditionFailure("Invalid ConfigurationRow")
                }
                return collectionView.dequeueConfiguredReusableCell(
                    using: self.configuredListCell(),
                    for: indexPath,
                    item: configurationItem
                )
            }
        }
    }()
        
    // MARK: Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        updateUI()
    }
    
    // MARK: Private functions
    
    private func setupUI() {
        navigationItem.title = "Settings"
        
        func setupCollectionView() {
            collectionView.indicatorStyle = .white
            collectionView.backgroundColor = .clear
            collectionView.delegate = self
            collectionView.dragInteractionEnabled = true
            view.addSubview(collectionView)
            collectionView.pinEdgesToParent()
            
            ButtonCollectionViewCell.registerProgrammatic(to: collectionView)
            SettingsCollectionViewCell.registerProgrammatic(to: collectionView)
        }
        setupCollectionView()
    }
    
    private func updateUI() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Int>()
        snapshot.appendSections(Section.allCases)
        Section.allCases.forEach { section in
            switch section {
            case .visibility:
                snapshot.appendItems(
                    VisiblityItem.allCases.map { $0.id(section: section.rawValue) },
                    toSection: section
                )
            case .configuration:
                snapshot.appendItems(
                    ConfigurationItem.allCases.map { $0.id(section: section.rawValue) },
                    toSection: section
                )
            }
        }
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
    private func configuredListCell() -> UICollectionView.CellRegistration<UICollectionViewListCell, CellContentProvider> {
        UICollectionView.CellRegistration<UICollectionViewListCell, CellContentProvider> { cell, indexPath, item in
            var content = UIListContentConfiguration.sidebarCell()
            content.text = item.title
            content.image = item.icon
            cell.contentConfiguration = content
            cell.accessories = [UICellAccessory.disclosureIndicator()]
        }
    }

    @objc
    private func refreshAction(sender: UIRefreshControl) {
        updateUI()
        sender.endRefreshing()
    }
}

extension SettingsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let section = Section(rawValue: indexPath.section) else {
            preconditionFailure("Invalid section")
        }
        switch section {
        case .visibility:
            splitViewController?.showsSecondaryOnlyButton = true
        case .configuration:
            break
        }
        collectionView.deselectItem(at: indexPath, animated: false)
        if let bezierViewController = bezierDelegate as? BezierViewController {
            splitViewController?.showDetailViewController(bezierViewController, sender: nil)
        }
    }
}

extension SettingsViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, show vc: UIViewController, sender: Any?) -> Bool {
        false
    }
}
