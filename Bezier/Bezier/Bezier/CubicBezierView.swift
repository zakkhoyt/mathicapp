//
//  CubicBezierView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/21/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class CubicBezierView: MathView {
    // MARK: Private vars
    private let controlPointSize: CGFloat
    private var touchPoint: CGPoint = CGPoint(x: 1, y: 0)
    private var controlItems: [ControlItem] = []
    private var controlPoints: [CGPoint] { controlItems.compactMap { $0.point } }
    private var controlPointViews: [ControlPointView] { controlItems.compactMap { $0.view } }
    private lazy var panGesture: UIPanGestureRecognizer = {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan))

        return panGesture
    }()
    private let theme = Bezier.Theme()
    
    public init(
        controlPointSize: CGFloat
    ) {
        self.controlPointSize = controlPointSize
        super.init(frame: .zero)
    }
    
    required public init?(coder: NSCoder) {
        self.controlPointSize = 44
        super.init(coder: coder)
    }
    
    // MARK: Override functions
    
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        stop()
        isUserInteractionEnabled = true
        setupControlPointViews(theme: theme)
        addGestureRecognizer(panGesture)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        for controlItem in controlItems {
            let x = controlItem.point.x * bounds.width
            let y = (1.0 - controlItem.point.y) * bounds.height
            controlItem.view.center = CGPoint(x: x, y: y)
        }
    }

    open override func draw(_ rect: CGRect) {
        drawTangents(theme: theme)
        drawBezierPath(theme: theme)
        drawTouchTangent(t: touchPoint.x / bounds.width, theme: theme)
    }
    
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for controlPointView in controlPointViews {
            // TODO: Convert to path for better in/out touch detection
            let frame = self.convert(controlPointView.frame, to: self)
            print("\(frame.debugDescription) \(point.debugDescription)")
            if frame.contains(point) {
                return true
            }
        }
        return false
    }
    
    // MARK: Private functions
    
    private func setupControlPointViews(theme: Bezier.Theme) {
        let size = controlPointSize
        let halfSize = size / 2
        let numberOfControlPoints = 4
        for i in 0..<numberOfControlPoints {
            let controlPointView = ControlPointView()
            controlPointView.label.text = "p\(i)"
            controlPointView.label.font = theme.controlPoint.font
            controlPointView.label.textColor = theme.controlPoint.textColor
            controlPointView.frame = CGRect(
                x: halfSize + CGFloat(i) * size,
                y: halfSize + CGFloat(i) * size,
                width: size,
                height: size
            )
            controlPointView.backgroundColor = theme.controlPoint.fillColor
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan))
            controlPointView.addGestureRecognizer(panGesture)
            addSubview(controlPointView)
            
            let controlPoint: CGPoint = {
                switch i {
                case 0: return CGPoint(x: 0, y: 0)
                case 1: return CGPoint(x: 0.1, y: 0.9)
                case 2: return CGPoint(x: 0.9, y: 0.1)
                case 3: return CGPoint(x: 1, y: 1)
                default: return .zero
                }
            }()

            let controlItem = ControlItem(point: controlPoint, view: controlPointView)
            controlItems.append(controlItem)
        }
    }
    
    /// Draws faint tangent lines every `strideValue` points along the bezier curve.
    /// - Parameter strideValue: A value in points.
    private func drawTangents(
        theme: Bezier.Theme
    ) {
        for i in stride(from: 0, to: Int(bounds.width), by: theme.stride) {
            let t = CGFloat(i) / bounds.width
            do {
                let solution = try Bezier.solve(
                    points: controlPoints,
                    t: t
                )
                drawLine(
                    point1: expand(point: solution.tangentPoint1),
                    point2: expand(point: solution.tangentPoint2),
                    strokeColor: theme.strideTangent.color,
                    fillColor: nil,
                    lineWidth: 0.5
                )
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    /// Draws the bezier path itself with a segment every `strideValue` points.
    /// - Parameter strideValue: A value in points.
    private func drawBezierPath(
        theme: Bezier.Theme
    ) {
        UIColor.label.setStroke()
        let path = UIBezierPath()
        path.lineWidth = theme.path.width
        theme.path.color.setStroke()
        
        for i in stride(from: 0, to: Int(bounds.width), by: theme.stride) {
            let t = CGFloat(i) / bounds.width
            do {
                let solution = try Bezier.solve(
                    points: controlPoints,
                    t: t
                )
                let point = expand(point: solution.point)
                if i == 0 {
                    path.move(to: point)
                } else {
                    path.addLine(to: point)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        
        path.stroke()
    }
            
    /// Draws a bold tangent line along the bezier path which intersects at the current touchPoint.
    /// - Parameter t: T value from 0...1
    private func drawTouchTangent(
        t: CGFloat,
        theme: Bezier.Theme
    ) {
        do {
            let solution = try Bezier.solve(
                points: controlPoints,
                t: t
            )
            drawLine(
                point1: expand(point: solution.tangentPoint1),
                point2: expand(point: solution.tangentPoint2),
                strokeColor: .magenta,
                fillColor: nil,
                lineWidth: 1
            )
            
            let point = CGPoint.interpolate(
                percent: t,
                point1: solution.tangentPoint1,
                point2: solution.tangentPoint2
            )
            drawCircle(
                center: expand(point: point),
                radius: theme.tangent.width,
                strokeColor: theme.tangent.color,
                fillColor: theme.tangent.color,
                lineWidth: theme.tangent.width
            )
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc
    private func pan(sender: UIPanGestureRecognizer) {
        switch sender {
        case self.panGesture:
            touchPoint = sender.location(in: self)
            setNeedsDisplay()
        default:
            guard let cpView = sender.view as? ControlPointView else { return }
            switch sender.state {
            case .began:
                cpView.startPoint = cpView.center
                cpView.panStart = sender.location(in: self)
            case .changed, .ended:
                let point = sender.location(in: self)
                let deltaX = point.x - cpView.panStart.x
                let deltaY = point.y - cpView.panStart.y
                let newPoint = CGPoint(
                    x: cpView.startPoint.x + deltaX,
                    y: cpView.startPoint.y + deltaY
                ).clip(to: bounds)
                cpView.center = newPoint
                
                do {
                    let controlPoint = contract(point: newPoint)
                    guard let index = controlPointViews.firstIndex(of: cpView) else { return }
                    controlItems[index].point = controlPoint
                }
                
                setNeedsDisplay()
            default: break
            }
        }
    }
}

extension CubicBezierView: TitledAnimation {
    public static var animationTitle: String { "Quadratic Bezier" }
}

extension CubicBezierView {
    class ControlPointView: UIView {
        let label = UILabel()
        var startPoint: CGPoint = .zero
        var panStart: CGPoint = .zero
        
        init() {
            super.init(frame: .zero)
            commonInit()
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            commonInit()
        }
        
        private func commonInit() {
            label.translatesAutoresizingMaskIntoConstraints = false
            addSubview(label)
            label.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            label.topAnchor.constraint(equalTo: topAnchor).isActive = true
            label.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            label.font = UIFont.preferredFont(forTextStyle: .callout)
            label.textAlignment = .center
            label.textColor = .label
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            layer.masksToBounds = true
            layer.cornerRadius = self.radius
        }
        
//        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//            print("\(frame.debugDescription) \(point.debugDescription)")
//            if frame.contains(point) {
//                return self
//            } else {
//                return nil
//            }
//        }
//        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//            print("\(frame.debugDescription) \(point.debugDescription)")
//        return true
////            return frame.contains(point)
//        }
    }
    
    class ControlItem {
        var point: CGPoint
        var view: ControlPointView
        
        init(point: CGPoint, view: ControlPointView) {
            self.point = point
            self.view = view
        }
    }
}
