//
//  AnimationView.swift
//  Mathic
//
//  Created by Zakk Hoyt on 2/2/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

public protocol AnimationViewFrameRateDelegate: AnyObject {
    func animationViewDidRenderFrame(_ animationView: AnimationView, frameRate: Double)
}

open class AnimationView: UIView, Informable {
    internal class FrameRateCalculator: Animatable {
        private(set) var frameRate: Double = 0
        private(set) var frameCount: Int = 0
        private(set) var startDate: Date = Date()
        private var elapsed: TimeInterval {
            Date().timeIntervalSince(startDate)
        }
        
        init() {}
        
        func increment() {
            frameCount += 1
            frameRate = TimeInterval(frameCount) / elapsed
        }
        
        public func start() {
            startDate = Date()
        }
        
        public func stop() {}
    }

    public enum State: Int, CaseIterable {
        case stopped
        case started
    }
    
    // MARK: Public vars
    weak public var frameRateDelegate: AnimationViewFrameRateDelegate?
    open var startDate = Date()

    /// The time elapsed in seconds since startDate
    public var elapsed: TimeInterval {
        Date().timeIntervalSince(startDate) + previewElapsed
    }

    //// The frequency which cycles, phases, etc.. will run at
    public var frequency: TimeInterval {
        0.1
    }
    
    /// The time it takes for one cycle.
    /// Eg if f = 0.1, cycleDuration = 10
    public var cycleDuration: TimeInterval {
        1 / frequency
    }

    /// Progress though the current cycle 0.0 ... 1.0
    public var cycle: CGFloat {
        CGFloat((elapsed).truncatingRemainder(dividingBy: cycleDuration) / cycleDuration)
    }
    
    /// Progress though the current cycle -1.0 ... 1.0
    public var normalizedCycle: CGFloat {
        2.0 * CGFloat((elapsed).truncatingRemainder(dividingBy: cycleDuration) / cycleDuration) - 1.0
    }

    /// The number of cycles which have elapsed elapsed / cycleDuration
    public var cycleCount: CGFloat {
        CGFloat(elapsed / cycleDuration)
    }

    public var frameRate: Double {
        frameRateCalculator.frameRate
    }

    public var frameCount: Int {
        frameRateCalculator.frameCount
    }

    public var didRenderFrame: (() -> Void)?
    
    /// A static view can "pretend" it is a certain time into the animation
    open var previewElapsed: TimeInterval {
        0
    }

    public private(set) var state: AnimationView.State = .stopped
    
    // MARK: Internal vars
    
    internal var frameRateCalculator = FrameRateCalculator()
    
    // MARK: Open methods
    
    private var link: CADisplayLink?
    
    // MARK: Private methods
    private func installUpdateLoop() {
        if self.link != nil {
            return
        }
        
        let link = CADisplayLink(target: self, selector: #selector(update))
        link.add(to: .current, forMode: .common)
        link.preferredFramesPerSecond = 60
        self.link = link
        state = .started
    }

    private func removeUpdateLoop() {
        link?.remove(from: .current, forMode: .common)
        state = .stopped
    }
    
    public func cycleCountIncremented() {
        // Child can implement to catch event
    }

    @objc
    private func update() {
        setNeedsDisplay()
        
        if ProcessInfo.processInfo.arguments.contains("PRINT_BASE_VARS") {
            // swiftlint:disable line_length
            print("frequency: \(frequency) cycleDuration: \(cycleDuration) elapsed: \(elapsed) cycle: \(cycle) normalizedCycle: \(normalizedCycle) cycleCount: \(cycleCount)")
            // swiftlint:enable line_length
        }
        
        frameRateCalculator.increment()
        frameRateDelegate?.animationViewDidRenderFrame(self, frameRate: frameRate)
        
        didRenderFrame?()
    }
}

extension AnimationView: Animatable {
    open func start() {
        guard state != .started else {
            print("Already started")
            return
        }
        
        installUpdateLoop()
        frameRateCalculator.start()
    }
    
    open func stop() {
        guard state != .stopped else {
            print("Already stopped")
            return
        }
        frameRateCalculator.stop()
        removeUpdateLoop()
    }
}

extension UIView {
    open var radius: CGFloat {
        min(bounds.width, bounds.height) / 2.0
    }
    
    public var centerRect: CGRect {
        let smaller: CGFloat = min(bounds.width, bounds.height)
        return CGRect(x: (bounds.width - smaller) / 2.0,
                      y: (bounds.height - smaller) / 2.0,
                      width: smaller,
                      height: smaller)
    }
    
    open var smallDotRadius: CGFloat {
        max(2, min(bounds.width, bounds.height) / 128)
    }
    
    open var mediumDotRadius: CGFloat {
        max(4, min(bounds.width, bounds.height) / 64)
    }
    
    open var largeDotRadius: CGFloat {
        max(8, min(bounds.width, bounds.height) / 32)
    }
}
