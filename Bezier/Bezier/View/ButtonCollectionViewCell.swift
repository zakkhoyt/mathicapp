//
//  ButtonTableViewCell.swift
//  Bezier
//
//  Created by Zakk Hoyt on 8/25/21.
//

import UIKit

class ButtonCollectionViewCell: UICollectionViewCell {
    let button = UIButton(type: .custom)
    var buttonTapped: () -> Void = {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        button.addTarget(self, action: #selector(buttonTouchUpInside), for: .touchUpInside)
        addSubview(button)
        button.pinEdgesToParent(useLayoutMargins: true)
    }
    
    @objc
    private func buttonTouchUpInside(sender: UIButton) {
        buttonTapped()
    }
}

extension ButtonCollectionViewCell: CollectionViewReusable {
    static var reuseIdentifier: String { "ButtonCollectionViewCell" }
}
