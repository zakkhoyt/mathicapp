//
//  SettingsTableViewCell.swift
//  Bezier
//
//  Created by Zakk Hoyt on 8/25/21.
//

import UIKit

class SettingsCollectionViewCell: UICollectionViewCell {
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        label.font = .preferredFont(forTextStyle: .body)
        contentView.addSubview(label)
        label.pinEdgesToParent()
    }
}

extension SettingsCollectionViewCell: CollectionViewReusable {
    static var reuseIdentifier: String { "SettingsCollectionViewCell" }
}
