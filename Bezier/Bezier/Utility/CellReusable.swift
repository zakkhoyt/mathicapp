//
//  CollectionViewDequeuable.swift
//  Nightlight
//
//  Created by Zakk Hoyt on 3/5/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

// MARK: - TableView

/**
 This is to be implemented by a UITableViewCell subclass, as a convenience to setup reusability by the caller.
 */
public protocol TableViewReusable {
    /// The identifier used for registering and dequeuing.
    static var reuseIdentifier: String { get }

    /// Works to register a nib by identieifer string
    /// - Parameter tableView: The `UITableView` to register against
    static func registerNib(to tableView: UITableView)

    /// Works to register a nib by identieifer string
    /// - Parameter tableView: The `UITableView` to register against
    static func registerProgrammatic(to tableView: UITableView)
}

extension TableViewReusable where Self: UITableViewCell {
    public static func registerNib(to tableView: UITableView) {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: reuseIdentifier, bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    public static func registerProgrammatic(to tableView: UITableView) {
        tableView.register(Self.self, forCellReuseIdentifier: reuseIdentifier)
    }
}

extension TableViewReusable where Self: UITableViewHeaderFooterView {
    public static func registerNib(to tableView: UITableView) {
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
    
    public static func registerProgrammatic(to tableView: UITableView) {
        tableView.register(Self.self, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
}

// MARK: - CollectionView

/**
 This is to be implemented by a UICollectionViewCell subclass, as a convenience to setup reusability by the caller.
 */
public protocol CollectionViewReusable {
    /// The identifier used for registering and dequeuing.
    static var reuseIdentifier: String { get }
    
    /// Works to register a nib by identieifer string
    /// - Parameter collectionView: The `UICollectionView` to register against
    static func registerNib(to collectionView: UICollectionView)
    
    /// Works to register a programmatic class by identieifer string
    /// - Parameter collectionView: The `UICollectionView` to register against
    static func registerProgrammatic(to collectionView: UICollectionView)
}

extension CollectionViewReusable where Self: UICollectionViewCell {
    public static func registerNib(to collectionView: UICollectionView) {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: reuseIdentifier, bundle: bundle)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    public static func registerProgrammatic(to collectionView: UICollectionView) {
        collectionView.register(Self.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
}

public protocol CollectionReusableViewReusable: CollectionViewReusable {
    static func registerProgrammatic(forSupplementaryViewOfKind kind: String, to collectionView: UICollectionView)
}

extension CollectionViewReusable where Self: UICollectionReusableView {
    public static func registerNib(to collectionView: UICollectionView) {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: reuseIdentifier, bundle: bundle)
        collectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: reuseIdentifier)
    }

    public static func registerProgrammatic(to collectionView: UICollectionView) {
        collectionView.register(Self.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    public static func registerProgrammatic(forSupplementaryViewOfKind kind: String, to collectionView: UICollectionView) {
        collectionView.register(Self.self, forSupplementaryViewOfKind: kind, withReuseIdentifier: reuseIdentifier)
    }
}
