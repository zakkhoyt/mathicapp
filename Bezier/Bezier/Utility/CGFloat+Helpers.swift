//
//  CGFloat+Helpers.swift
//  Mathic
//
//  Created by Zakk Hoyt on 5/2/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGFloat {
    static func project(
        value: CGFloat,         // 210
        inMinimum: CGFloat,     // 200
        inMaximum: CGFloat,     // 300
        outMinimum: CGFloat,    // 1000
        outMaximum: CGFloat     // 2000
    ) -> CGFloat {
        let inRange = inMaximum - inMinimum // 300 - 200 = 100
        let inPercentage = (value - inMinimum) / inRange // ((210 - 200) / 100) = 0.1
        let outRange = outMaximum - outMinimum // 3000 - 2000 = 1000
        let outValue = outMinimum + (inPercentage * outRange) // 2000 + 0.1 * 1000 = 2100
        return outValue
    }
    
    static func interpolate<CGFloat: FloatingPoint>(
        percent: CGFloat,
        minimum: CGFloat,
        maximum: CGFloat
    ) -> CGFloat {
        minimum + percent * (maximum - minimum)
    }
}
