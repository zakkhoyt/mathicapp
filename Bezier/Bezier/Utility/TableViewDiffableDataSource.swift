//
//  TableViewDiffableDataSource.swift
//  HatchTools
//
//  Created by Zakk Hoyt on 5/3/21.
//  Copyright © 2021 Zakk Hoyt. All rights reserved.
//

import UIKit

// swiftlint:disable colon
// swiftlint:disable generic_type_name
open class TableViewDiffableDataSource<SectionIdentifierType, ItemIdentifierType>:
    UITableViewDiffableDataSource<SectionIdentifierType, ItemIdentifierType>
    where SectionIdentifierType: Hashable, ItemIdentifierType: Hashable {
    // swiftlint:enable generic_type_name
    // swiftlint:enable colon
    open var canEditRowAt: ((UITableView, IndexPath) -> Bool)?
    open var canMoveRowAt: ((UITableView, IndexPath) -> Bool)?
    open var moveRowAt: ((UITableView, _ source: IndexPath, _ destination: IndexPath) -> Void)?
    
    open override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        canEditRowAt?(tableView, indexPath) ?? super.tableView(tableView, canEditRowAt: indexPath)
    }

    open override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        canMoveRowAt?(tableView, indexPath) ?? super.tableView(tableView, canMoveRowAt: indexPath)
    }

    open override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        moveRowAt?(tableView, sourceIndexPath, destinationIndexPath)
    }
}
