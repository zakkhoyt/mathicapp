//
//  BezierViewModel.swift
//  Bezier
//
//  Created by Zakk Hoyt on 8/26/21.
//

import UIKit

protocol LineTheme {
    var color: UIColor { get }
    var width: CGFloat { get }
}

protocol LineSetTheme: LineTheme {
    var stride: Int { get }
}

extension Bezier {
    struct Theme {
        struct ControlPoint {
            var font: UIFont = .preferredFont(forTextStyle: .caption1)
            var textColor: UIColor = .label
            var textIsHidden: Bool = false
            
            var fillColor: UIColor = .systemGreen
            var strokeColor: UIColor = .label
        }
        
        struct Line: LineTheme {
            var color: UIColor
            var width: CGFloat
        }
        
        struct LineSet: LineSetTheme {
            var color: UIColor
            var width: CGFloat
            var stride: Int
        }
        
        struct Dot: LineTheme {
            var color: UIColor
            var width: CGFloat
        }
        
        // MARK: Render Inputs
        
        var controlPoint = ControlPoint()
        
        var path = Line(color: .label, width: 8)
        
        var t = Dot(color: .label, width: 16)
        
        var tangent = Line(color: .systemPink, width: 0.6)
        
        var stride: Int = 20
        var strideTangent = Line(color: .systemGray.withAlphaComponent(0.25), width: 0.5)
        
        var showStrideTangent = true
        var showPath = true
        var showControlPoints = true
    }
}

struct BezierModel {
    // MARK: - Input
    
    /// A series of points which define the bezier path.
    var controlPoints: [CGPoint]
    
    /// If set, the first and last contorl points at (0,0)) and (1,1) respectively
    var fixedControlPointsEnds = true {
        didSet {
            guard fixedControlPointsEnds == true,
                  !controlPoints.isEmpty
            else {
                return
            }
            controlPoints[0] = .zero
            controlPoints[controlPoints.count - 1] = CGPoint(x: 1, y: 1)
        }
    }
    
    // MARK: - Output
    
    /// The solution to the bezier using `contolPoints` as input
    func solve(t: CGFloat) throws -> Bezier.Solution {
        try Bezier.solve(points: controlPoints, t: t)
    }

    /// Theme for the bezier rendering
    var theme = Bezier.Theme()
}
