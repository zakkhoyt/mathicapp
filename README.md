## Mathic APP

A CoreGraphics showcase.

### Inspiration.
I often see interesting mathematical graphics and optical illusions on places like /r/math. I find it enjoyable/challanging to implement them using CoreGraphics. 
I also dabble with GPU based drawing (GL Shaders and Metal Shaders). GPU tools are much more powerful. Maybe a showcase will be coming down the pipeline (heh). 

### Project layout
The workspace is broken into two Targets:
* Mathic - A dynamic framework where the intersting stuff lives. There are a few useful UIView base classes and categories in here too.
    * The hierarchy of UIIVews which all of the rendered views inherit from:
        * AnimationView (root class) - Provides a frequency var and elapsed (time) computed var. Keeps track of drawing time, framerate, framecount, etc...
        * MathView - Provides a number of convenient drawing functions (lines, circles, etc..) 
        * PhasedCircleView - Supplies a "phase" var which converts "elapsed" into a 0 .. 2Pi range over and over. Allows touch scrubbing around the origin.
        * TrigView, FourierView, CariogramView, etc...
* MathicApp - An iOS app which supplies a menu and ViewControllers to display the views. Also provides a couple of custom UIControls  Not much to see here.        
  
